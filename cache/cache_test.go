//go:build unit || all
// +build unit all

package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/database"
	"testing"
	"time"
)

func TestRedisCache_SetUserState(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	state := 2
	key := fmt.Sprintf("state:%d", chatID)
	mock.ExpectSet(key, state, 15*time.Minute).SetVal("2")
	mock.ExpectGet(key).SetVal("2")

	cache := RedisCache{r: db}
	err := cache.SetUserState(context.Background(), chatID, state)
	assert.NoError(t, err)
	assert.Equal(t, state, cache.GetUserState(context.Background(), chatID))
}

func TestRedisCache_SetUserLogin(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	login := "TestLogin"
	key := fmt.Sprintf("login:%d", chatID)
	mock.ExpectSet(key, login, 15*time.Minute).SetVal(login)
	mock.ExpectGet(key).SetVal(login)

	cache := RedisCache{r: db}
	err := cache.SetUserLogin(context.Background(), chatID, login)
	assert.NoError(t, err)
	assert.Equal(t, login, cache.GetUserLogin(context.Background(), chatID))
}

func TestRedisCache_SetUserPassword(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	password := "TestLogin"
	key := fmt.Sprintf("password:%d", chatID)
	mock.ExpectSet(key, password, 15*time.Minute).SetVal(password)
	mock.ExpectGet(key).SetVal(password)

	cache := RedisCache{r: db}
	err := cache.SetUserPassword(context.Background(), chatID, password)
	assert.NoError(t, err)
	assert.Equal(t, password, cache.GetUserPassword(context.Background(), chatID))
}

func TestRedisCache_SetUserNews(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	news := "TestNews"
	key := fmt.Sprintf("news_text:%d", chatID)
	mock.ExpectSet(key, news, 15*time.Minute).SetVal(news)
	mock.ExpectGet(key).SetVal(news)
	cache := RedisCache{r: db}
	err := cache.SetUserNewsText(context.Background(), chatID, news)
	assert.NoError(t, err)
	actual, err := cache.GetUserNewsText(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, news, actual)
}

func TestRedisCache_SetUserMarks(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	mark := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "tEst",
			Name: "Tesstt",
		},
		Dates:           []string{"test"},
		DisciplineMarks: nil,
	}
	key := fmt.Sprintf("marks:%d", chatID)
	val, err := mark.Value()
	require.NoError(t, err)
	mock.ExpectSet(key, mark, 15*time.Minute).SetVal(val.(string))
	mock.ExpectGet(key).SetVal(val.(string))
	cache := RedisCache{r: db}
	err = cache.SetUserMarks(context.Background(), chatID, mark)
	assert.NoError(t, err)
	actual, err := cache.GetUserMarks(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, mark, actual)
}

func TestRedisCache_SetAdminChosenReport(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	reportID := 1

	key := fmt.Sprintf("chosen_report:%d", chatID)
	mock.ExpectSet(key, reportID, 15*time.Minute).SetVal(fmt.Sprintf("%d", reportID))
	mock.ExpectGet(key).SetVal(fmt.Sprintf("%d", reportID))
	cache := RedisCache{r: db}
	err := cache.SetAdminChosenReport(context.Background(), chatID, reportID)
	assert.NoError(t, err)
	actual, err := cache.GetAdminChosenReport(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, reportID, actual)
}

func TestRedisCache_SetUserHomeworkDate(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	date := time.Now()

	key := fmt.Sprintf("hw_date:%d", chatID)
	mock.ExpectSet(key, date.Format("2006-01-02"), 30*time.Minute).SetVal(date.Format("2006-01-02"))
	mock.ExpectGet(key).SetVal(date.Format("2006-01-02"))
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkDate(context.Background(), chatID, date)
	assert.NoError(t, err)
	actual, err := cache.GetUserHomeworkDate(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, date.Year(), actual.Year())
	assert.Equal(t, date.Month(), actual.Month())
	assert.Equal(t, date.Day(), actual.Day())
}

func TestRedisCache_SetUserHomeworkSubject(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	subject := "Test"

	key := fmt.Sprintf("hw_subject:%d", chatID)
	mock.ExpectSet(key, subject, 30*time.Minute).SetVal(subject)
	mock.ExpectGet(key).SetVal(subject)
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkSubject(context.Background(), chatID, subject)
	assert.NoError(t, err)
	actual, err := cache.GetUserHomeworkSubject(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, subject, actual)
}

func TestRedisCache_SetUserHomeworkText(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	subject := "Test"

	key := fmt.Sprintf("hw_text:%d", chatID)
	mock.ExpectSet(key, subject, 30*time.Minute).SetVal(subject)
	mock.ExpectGet(key).SetVal(subject)
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkText(context.Background(), chatID, subject)
	assert.NoError(t, err)
	actual, err := cache.GetUserHomeworkText(context.Background(), chatID)
	assert.NoError(t, err)
	assert.Equal(t, subject, actual)
}

func TestRedisCache_AddUserHomeworkFiles(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)
	key := fmt.Sprintf("files_doc:%d", chatID)
	file := "TestFileID"

	mock.ExpectRPush(key, file).SetVal(1)
	mock.ExpectRPush(key, file+file).SetVal(1)
	mock.ExpectRPush(fmt.Sprintf("files_img:%d", chatID), file).SetVal(1)
	mock.ExpectRPush(fmt.Sprintf("files_img:%d", chatID), file+file).SetVal(1)
	mock.ExpectLRange(key, 0, -1).SetVal([]string{file, file + file})
	mock.ExpectLRange(fmt.Sprintf("files_img:%d", chatID), 0, -1).SetVal([]string{file, file + file})

	err := cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file, IsDocument: true})
	require.NoError(t, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file + file, IsDocument: true})
	require.NoError(t, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file, IsDocument: false})
	require.NoError(t, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file + file, IsDocument: false})
	require.NoError(t, err)
	files, err := cache.GetUserHomeworkFiles(context.Background(), chatID)
	require.NoError(t, err)
	assert.Equal(t, 4, len(files))
	assert.Equal(t, database.HomeworkFile{
		FileID:     file,
		IsDocument: true,
	}, files[0])
	assert.Equal(t, database.HomeworkFile{
		FileID:     file + file,
		IsDocument: true,
	}, files[1])
	assert.Equal(t, database.HomeworkFile{
		FileID:     file,
		IsDocument: false,
	}, files[2])
	assert.Equal(t, database.HomeworkFile{
		FileID:     file + file,
		IsDocument: false,
	}, files[3])
}

func TestRedisCache_CleanUserHomework(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)

	mock.ExpectDel(fmt.Sprintf("hw_date:%d", chatID), fmt.Sprintf("hw_subject:%d", chatID), fmt.Sprintf("hw_text:%d", chatID), fmt.Sprintf("files_img:%d", chatID), fmt.Sprintf("files_doc:%d", chatID)).SetVal(1)

	err := cache.CleanUserHomework(context.Background(), chatID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_DeleteUser(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)

	mock.ExpectKeys(fmt.Sprintf("*%d*", chatID)).SetVal([]string{fmt.Sprintf("kek:%d", chatID), fmt.Sprintf("lol:%d", chatID)})
	mock.ExpectDel(fmt.Sprintf("kek:%d", chatID), fmt.Sprintf("lol:%d", chatID)).SetVal(1)

	err := cache.DeleteUser(context.Background(), chatID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_SetUserState_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	state := 2
	key := fmt.Sprintf("state:%d", chatID)

	wantedErr := fmt.Errorf("not today")

	mock.ExpectSet(key, state, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)

	cache := RedisCache{r: db}
	err := cache.SetUserState(context.Background(), chatID, state)
	assert.ErrorIs(t, wantedErr, err)
	assert.Equal(t, -1, cache.GetUserState(context.Background(), chatID))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_SetUserLogin_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	login := "TestLogin"
	key := fmt.Sprintf("login:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, login, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)

	cache := RedisCache{r: db}
	err := cache.SetUserLogin(context.Background(), chatID, login)
	assert.Equal(t, "", cache.GetUserLogin(context.Background(), chatID))
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_SetUserPassword_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()

	chatID := int64(123456789)
	password := "TestLogin"
	key := fmt.Sprintf("password:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, password, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)

	cache := RedisCache{r: db}
	err := cache.SetUserPassword(context.Background(), chatID, password)
	assert.ErrorIs(t, wantedErr, err)
	assert.Equal(t, "", cache.GetUserPassword(context.Background(), chatID))
}

func TestRedisCache_SetUserNews_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	news := "TestNews"
	key := fmt.Sprintf("news_text:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, news, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetUserNewsText(context.Background(), chatID, news)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetUserNewsText(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, "", actual)
}

func TestRedisCache_SetUserMarks_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	mark := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "tEst",
			Name: "Tesstt",
		},
		Dates:           []string{"test"},
		DisciplineMarks: nil,
	}
	wantedErr := fmt.Errorf("not today")
	key := fmt.Sprintf("marks:%d", chatID)
	mock.ExpectSet(key, mark, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetUserMarks(context.Background(), chatID, mark)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetUserMarks(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, api.SummaryMarks{}, actual)
}

func TestRedisCache_SetAdminChosenReport_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	reportID := 1

	key := fmt.Sprintf("chosen_report:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, reportID, 15*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetAdminChosenReport(context.Background(), chatID, reportID)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetAdminChosenReport(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, -1, actual)
}

func TestRedisCache_SetUserHomeworkDate_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	date := time.Now()

	key := fmt.Sprintf("hw_date:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, date.Format("2006-01-02"), 30*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkDate(context.Background(), chatID, date)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetUserHomeworkDate(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, time.Time{}, actual)
}

func TestRedisCache_SetUserHomeworkSubject_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	subject := "Test"

	key := fmt.Sprintf("hw_subject:%d", chatID)
	wantedErr := fmt.Errorf("not today")
	mock.ExpectSet(key, subject, 30*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkSubject(context.Background(), chatID, subject)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetUserHomeworkSubject(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, "", actual)
}

func TestRedisCache_SetUserHomeworkText_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	chatID := int64(123456789)
	text := "Test"
	wantedErr := fmt.Errorf("not today")

	key := fmt.Sprintf("hw_text:%d", chatID)
	mock.ExpectSet(key, text, 30*time.Minute).SetErr(wantedErr)
	mock.ExpectGet(key).SetErr(redis.Nil)
	cache := RedisCache{r: db}
	err := cache.SetUserHomeworkText(context.Background(), chatID, text)
	assert.ErrorIs(t, wantedErr, err)
	actual, err := cache.GetUserHomeworkText(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, "", actual)
}

func TestRedisCache_AddUserHomeworkFiles_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)
	key := fmt.Sprintf("files_doc:%d", chatID)
	file := "TestFileID"

	wantedErr := fmt.Errorf("not today")

	mock.ExpectRPush(key, file).SetErr(wantedErr)
	mock.ExpectRPush(key, file+file).SetErr(wantedErr)
	mock.ExpectRPush(fmt.Sprintf("files_img:%d", chatID), file).SetErr(wantedErr)
	mock.ExpectRPush(fmt.Sprintf("files_img:%d", chatID), file+file).SetErr(wantedErr)
	mock.ExpectLRange(key, 0, -1).SetErr(redis.Nil)
	mock.ExpectLRange(fmt.Sprintf("files_img:%d", chatID), 0, -1).SetErr(redis.Nil)

	err := cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file, IsDocument: true})
	assert.ErrorIs(t, wantedErr, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file + file, IsDocument: true})
	assert.ErrorIs(t, wantedErr, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file, IsDocument: false})
	assert.ErrorIs(t, wantedErr, err)
	err = cache.AddUserHomeworkFile(context.Background(), chatID, database.HomeworkFile{FileID: file + file, IsDocument: false})
	assert.ErrorIs(t, wantedErr, err)
	files, err := cache.GetUserHomeworkFiles(context.Background(), chatID)
	assert.ErrorIs(t, redis.Nil, err)
	assert.Equal(t, 0, len(files))
}

func TestRedisCache_CleanUserHomework_Error(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)

	wantedErr := fmt.Errorf("not today")
	mock.ExpectDel(fmt.Sprintf("hw_date:%d", chatID), fmt.Sprintf("hw_subject:%d", chatID), fmt.Sprintf("hw_text:%d", chatID), fmt.Sprintf("files_img:%d", chatID), fmt.Sprintf("files_doc:%d", chatID)).SetErr(wantedErr)

	err := cache.CleanUserHomework(context.Background(), chatID)
	assert.ErrorIs(t, err, wantedErr)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_DeleteUser_ErrorOnDel(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)

	wantedErr := fmt.Errorf("not today")

	mock.ExpectKeys(fmt.Sprintf("*%d*", chatID)).SetVal([]string{fmt.Sprintf("kek:%d", chatID), fmt.Sprintf("lol:%d", chatID)})
	mock.ExpectDel(fmt.Sprintf("kek:%d", chatID), fmt.Sprintf("lol:%d", chatID)).SetErr(wantedErr)

	err := cache.DeleteUser(context.Background(), chatID)
	assert.ErrorIs(t, err, wantedErr)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_DeleteUser_ErrorOnKeys(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}

	chatID := int64(123456789)

	wantedErr := fmt.Errorf("not today")

	mock.ExpectKeys(fmt.Sprintf("*%d*", chatID)).SetErr(wantedErr)

	err := cache.DeleteUser(context.Background(), chatID)
	assert.ErrorIs(t, err, wantedErr)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestRedisCache_Close(t *testing.T) {
	t.Parallel()
	db, mock := redismock.NewClientMock()
	cache := RedisCache{r: db}
	cache.Close(context.Background())
	assert.NoError(t, mock.ExpectationsWereMet())
}
