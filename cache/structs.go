package cache

import (
	"github.com/go-redis/redis/v8"
)

// RedisCache is a redis cache wrapper
type RedisCache struct {
	r *redis.Client
}
