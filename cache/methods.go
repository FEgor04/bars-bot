package cache

import (
  "context"
  "fmt"
  "gitlab.com/bars-bot/bars-bot/database"
  "time"

  "github.com/go-redis/redis/extra/redisotel/v8"
  "github.com/go-redis/redis/v8"
  "gitlab.com/bars-bot/bars-bot/api"
  "go.opentelemetry.io/otel"
  "go.opentelemetry.io/otel/attribute"
  "go.opentelemetry.io/otel/trace"
)

// TracerName is a name for jaeger tracer
const TracerName = "redis"

// GetUserLogin gets user login from cache
func (c *RedisCache) GetUserLogin(ctx context.Context, chatID int64) string {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserLogin")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  val := c.r.Get(ctx, fmt.Sprintf("login:%d", chatID)).Val()
  return val
}

// GetUserPassword gets user password from cache
func (c *RedisCache) GetUserPassword(ctx context.Context, chatID int64) string {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserPassword")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  val := c.r.Get(ctx, fmt.Sprintf("password:%d", chatID)).Val()
  return val
}

// SetUserLogin sets user login in cache
func (c *RedisCache) SetUserLogin(ctx context.Context, chatID int64, login string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserLogin")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("login:%d", chatID), login, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// SetUserPassword sets user login in cache
func (c *RedisCache) SetUserPassword(ctx context.Context, chatID int64, password string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserPassword")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("password:%d", chatID), password, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// Close closes the connection with cache
func (c *RedisCache) Close(ctx context.Context) {
  var span trace.Span
  _, span = otel.Tracer(TracerName).Start(ctx, "Close")
  defer span.End()
  err := c.r.Close()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
}

// Init inits connection and pings it
func (c *RedisCache) Init(ctx context.Context, addr, password string, db int) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "Init")
  defer span.End()

  c.r = redis.NewClient(&redis.Options{
    Addr:     addr,
    Password: password,
    DB:       db,
  })
  c.r.AddHook(redisotel.NewTracingHook()) // Hook for Redis
  err := c.r.Ping(ctx).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserState gets user state from cache
func (c *RedisCache) GetUserState(ctx context.Context, chatID int64) int {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserState")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  val, err := c.r.Get(ctx, fmt.Sprintf("state:%d", chatID)).Int()
  if err == redis.Nil {
    return -1
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return -1
  }
  return val
}

// SetUserState set users state in cache
func (c *RedisCache) SetUserState(ctx context.Context, chatID int64, state int) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserState")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("state:%d", chatID), state, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// SetUserNewsText sets users news text in cache
// It is only needed in /addnews command
func (c *RedisCache) SetUserNewsText(ctx context.Context, chatID int64, text string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserNewsText")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("news_text:%d", chatID), text, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserNewsText gets users news text from cache
// It is only needed in /addnews command
func (c *RedisCache) GetUserNewsText(ctx context.Context, chatID int64) (string, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserNewsText")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  val, err := c.r.Get(ctx, fmt.Sprintf("news_text:%d", chatID)).Result()
  if err == redis.Nil {
    return "", redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return val, err
}

// SetUserMarks set user's marks for /left command
func (c *RedisCache) SetUserMarks(ctx context.Context, chatID int64, marks api.SummaryMarks) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserMarks")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("marks:%d", chatID), marks, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserMarks gets user's marks for /left command
func (c *RedisCache) GetUserMarks(ctx context.Context, chatID int64) (api.SummaryMarks, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserMarks")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  var val api.SummaryMarks
  err := c.r.Get(ctx, fmt.Sprintf("marks:%d", chatID)).Scan(&val)
  if err == redis.Nil {
    return api.SummaryMarks{}, redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  if err == nil {
    val.Sort()
  }
  return val, err
}

// GetAdminChosenReport gets admin chosen report
func (c *RedisCache) GetAdminChosenReport(ctx context.Context, chatID int64) (int, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAdminChosenReport")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  var val int
  err := c.r.Get(ctx, fmt.Sprintf("chosen_report:%d", chatID)).Scan(&val)
  if err == redis.Nil {
    return -1, redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return val, err
}

// SetAdminChosenReport sets admin chosen report
func (c *RedisCache) SetAdminChosenReport(ctx context.Context, chatID int64, id int) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetAdminChosenReport")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("chosen_report:%d", chatID), id, 15*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// SetUserHomeworkDate sets user homework date
func (c *RedisCache) SetUserHomeworkDate(ctx context.Context, chatID int64, date time.Time) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserHomeworkDate")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()

  if date.Year() == 0 {
    date = date.AddDate(time.Now().Year(), 0, 0)
  }

  err := c.r.Set(ctx, fmt.Sprintf("hw_date:%d", chatID), date.Format("2006-01-02"), 30*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserHomeworkDate gets user homework date
func (c *RedisCache) GetUserHomeworkDate(ctx context.Context, chatID int64) (time.Time, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAdminChosenReport")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  var valStr string
  var val time.Time
  err := c.r.Get(ctx, fmt.Sprintf("hw_date:%d", chatID)).Scan(&valStr)
  if err == redis.Nil {
    return val, redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  val, err = time.Parse("2006-01-02", valStr)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return val, err
}

// SetUserHomeworkSubject sets user homework subject
func (c *RedisCache) SetUserHomeworkSubject(ctx context.Context, chatID int64, subject string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserHomeworkDate")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("hw_subject:%d", chatID), subject, 30*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserHomeworkSubject gets user homework subject
func (c *RedisCache) GetUserHomeworkSubject(ctx context.Context, chatID int64) (string, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAdminChosenReport")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  var val string
  err := c.r.Get(ctx, fmt.Sprintf("hw_subject:%d", chatID)).Scan(&val)
  if err == redis.Nil {
    return val, redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return val, err
}

// SetUserHomeworkText sets user homework text
func (c *RedisCache) SetUserHomeworkText(ctx context.Context, chatID int64, text string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "SetUserHomeworkDate")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  err := c.r.Set(ctx, fmt.Sprintf("hw_text:%d", chatID), text, 30*time.Minute).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserHomeworkText gets user homework text
func (c *RedisCache) GetUserHomeworkText(ctx context.Context, chatID int64) (string, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAdminChosenReport")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  var val string
  err := c.r.Get(ctx, fmt.Sprintf("hw_text:%d", chatID)).Scan(&val)
  if err == redis.Nil {
    return val, redis.Nil
  }
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return val, err
}

// AddUserHomeworkFile adds user homework file
func (c *RedisCache) AddUserHomeworkFile(ctx context.Context, chatID int64, file database.HomeworkFile) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "AddUserHomeworkFile")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()
  pref := "doc"
  if !file.IsDocument {
    pref = "img"
  }
  err := c.r.RPush(ctx, fmt.Sprintf("files_%s:%d", pref, chatID), file.FileID).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserHomeworkFiles gets user homework files
func (c *RedisCache) GetUserHomeworkFiles(ctx context.Context, chatID int64) (database.HomeworkFiles, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "AddUserHomeworkFile")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()

  var docs []database.HomeworkFile
  res1, err := c.r.LRange(ctx, fmt.Sprintf("files_doc:%d", chatID), 0, -1).Result()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  span.AddEvent("got docs", trace.WithAttributes(attribute.StringSlice("res", res1)))

  for _, doc := range res1 {
    docs = append(docs, database.HomeworkFile{
      FileID:     doc,
      IsDocument: true,
    })
  }

  res2, err := c.r.LRange(ctx, fmt.Sprintf("files_img:%d", chatID), 0, -1).Result()
  span.AddEvent("got images", trace.WithAttributes(attribute.StringSlice("res", res2)))

  for _, doc := range res2 {
    docs = append(docs, database.HomeworkFile{
      FileID:     doc,
      IsDocument: false,
    })
  }

  return docs, err
}

// CleanUserHomework cleans all the homework keys
func (c *RedisCache) CleanUserHomework(ctx context.Context, chatID int64) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "CleanUserHomework")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()

  err := c.r.Del(ctx, fmt.Sprintf("hw_date:%d", chatID), fmt.Sprintf("hw_subject:%d", chatID), fmt.Sprintf("hw_text:%d", chatID), fmt.Sprintf("files_img:%d", chatID), fmt.Sprintf("files_doc:%d", chatID)).Err()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// DeleteUser deletes everything connected with user
func (c RedisCache) DeleteUser(ctx context.Context, chatID int64) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "DeleteUser")
  span.SetAttributes(attribute.Int64("user.id", chatID))

  keys, err := c.r.Keys(ctx, fmt.Sprintf("*%d*", chatID)).Result()
  if err != nil {
    span.RecordError(err)
    return err
  }

  err = c.r.Del(ctx, keys...).Err()
  if err != nil {
    span.RecordError(err)
  }
  return err
}
