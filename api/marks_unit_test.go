//go:build all || unit
// +build all unit

package api

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gock.v1"
	"net/http"
	"testing"
	"time"
)
import "gopkg.in/h2non/gentleman-mock.v2"

func TestGetMarks_GoodPassword(t *testing.T) {
	defer mock.Disable()

	var wantedSummaryMarks = SummaryMarks{
		Subperiod: MarksSubperiod{"Test", "Test"},
		Dates:     []string{"Test"},
		DisciplineMarks: []DisciplineMark{{
			Discipline: "Test",
			Marks: []Mark{{
				Date:        "Test",
				Description: "Test",
				Mark:        "5",
			}},
			AverageMark: "5",
		}},
	}

	mock.New("https://test.com").Get("/api/MarkService/GetSummaryMarks").ReplyFunc(func(r *gock.Response) {
		r.Status(http.StatusOK)
		body := wantedSummaryMarks
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			r.Status(http.StatusInternalServerError)
			r.SetError(err)
			return
		}
		r.BodyBuffer = bodyBytes
	})

	cli := gentleman.New()
	cli = cli.Use(mock.Plugin)
	cli = cli.URL("https://test.com")
	//cli = cli.AddCookie(&http.Cookie{Name: "sessionid", Value: wantedSessionId})

	api := API{client: cli, sessionID: wantedSessionID}
	marks, err := RetryGetSummaryMarks(context.Background(), api, time.Now(), 3, time.Millisecond, nil)

	assert.NoError(t, err)
	assert.Equal(t, wantedSummaryMarks, marks)
}

func TestGetMarks_BadPassword(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/MarkService/GetSummaryMarks").ReplyFunc(func(r *gock.Response) {
		r.Status(http.StatusUnauthorized)
		bodyString := "{\n    \"faultcode\": \"Server.UserNotAuthenticated\",\n    \"faultstring\": \"Вы не авторизованы. </br>Обновите страницу для повторной аутентификации.\"\n}"
		r.BodyBuffer = []byte(bodyString)
	})

	cli := gentleman.New()
	cli = cli.Use(mock.Plugin)
	cli = cli.URL("https://test.com")
	//cli = cli.AddCookie(&http.Cookie{Name: "sessionid", Value: wantedSessionId})

	api := API{client: cli, sessionID: wantedSessionID}
	marks, err := RetryGetSummaryMarks(context.Background(), api, time.Now(), 3, time.Millisecond, nil)

	assert.ErrorIs(t, ErrUnauthorized, err)
	assert.Equal(t, SummaryMarks{}, marks)
}

func TestGetMarks_BadGateway(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/MarkService/GetSummaryMarks").Reply(http.StatusBadGateway)

	cli := gentleman.New()
	cli = cli.Use(mock.Plugin)
	cli = cli.URL("https://test.com")
	//cli = cli.AddCookie(&http.Cookie{Name: "sessionid", Value: wantedSessionId})

	api := API{client: cli, sessionID: wantedSessionID}
	marks, err := api.GetPersonMarks(context.Background(), time.Now())

	assert.Error(t, err)
	assert.Equal(t, SummaryMarks{}, marks)
}

func TestValueAndScan(t *testing.T) {
	t.Parallel()
	marks := SummaryMarks{
		Subperiod: MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates: []string{"Test"},
		DisciplineMarks: []DisciplineMark{{
			Discipline: "Test",
			Marks: []Mark{{
				Date:        "2021-11-24",
				Description: "Test",
				Mark:        "5",
			}},
			AverageMark: "5.0",
		}},
	}
	marksValue, err := marks.Value()
	require.NoError(t, err)
	assert.NotEqual(t, "", marksValue)

	marksScanned := SummaryMarks{}
	err = marksScanned.Scan(marksValue)
	require.NoError(t, err)
	assert.Equal(t, marks, marksScanned)
}

func TestMarksCount(t *testing.T) {
	t.Parallel()
	marks := SummaryMarks{
		Subperiod: MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates: []string{"Test"},
		DisciplineMarks: []DisciplineMark{{
			Discipline: "Test",
			Marks: []Mark{
				{
					Date:        "2021-11-24",
					Description: "Test",
					Mark:        "5",
				},
				{
					Date:        "2021-11-24",
					Description: "Test",
					Mark:        "5",
				},
				{
					Date:        "2021-11-24",
					Description: "Test",
					Mark:        "5",
				},
				{
					Date:        "2021-11-24",
					Description: "Test",
					Mark:        "5",
				},
			},
			AverageMark: "5.0",
		}},
	}
	assert.Equal(t, 4, marks.MarksCount())
}
