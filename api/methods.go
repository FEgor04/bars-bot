package api

import (
  "database/sql/driver"
  "encoding/json"
  "errors"
  "fmt"
  "gitlab.com/bars-bot/bars-bot/emoji"
  "math"
  "sort"
  "strconv"
  "time"

  "github.com/docker/docker/errdefs"
)

// GetSessionID gets this api sessionID
func (a API) GetSessionID() string {
  return a.sessionID
}

func (a MarkWithDiscipline) Less(b MarkWithDiscipline) bool {
  if a.Discipline == b.Discipline {
    return a.GetDate().Before(b.GetDate())
  }
  return a.Discipline < b.Discipline
}

// Value is a method to work with SQL
func (s SummaryMarks) Value() (driver.Value, error) {
  kek, err := json.Marshal(s)
  if err != nil {
    return "", err
  }
  return string(kek), nil
}

// Scan is a method to work with SQL
func (s *SummaryMarks) Scan(kek interface{}) error {
  switch kek := kek.(type) {
  case string:
    return json.Unmarshal([]byte(kek), s)
  case []uint8:
    return json.Unmarshal(kek, s)
  case nil:
    return nil
  }
  return errors.New("wtf lol")
}

// Value is a method to work with SQL
func (m Mark) Value() (driver.Value, error) {
  kek, err := json.Marshal(m)
  if err != nil {
    return "", err
  }
  return string(kek), nil
}

// Scan is a method to work with SQL
func (m *Mark) Scan(kek interface{}) error {
  switch kek := kek.(type) {
  case string:
    return json.Unmarshal([]byte(kek), m)
  case []uint8:
    return json.Unmarshal(kek, m)
  case nil:
    return nil
  }
  return errors.New("wtf lol")
}

// Round gets rounded average mark
func (d DisciplineMark) Round() int {
  markFloat, _ := strconv.ParseFloat(d.AverageMark, 64)
  return int(math.Round(markFloat - 0.1))
}

// GetDisciplineNames returns array of all the discipline names
func (s SummaryMarks) GetDisciplineNames() []string {
  var answ []string
  for _, subj := range s.DisciplineMarks {
    answ = append(answ, subj.ShortenDisciplineName())
  }
  return answ
}

// MarshalBinary returns the JSON representation of this marks
func (s SummaryMarks) MarshalBinary() (data []byte, err error) {
  kek, err := json.Marshal(s)
  return kek, err
}

// UnmarshalBinary creates summary marks from JSON
func (s *SummaryMarks) UnmarshalBinary(data []byte) error {
  return json.Unmarshal(data, s)
}

// Int converts mark to an integer
func (m Mark) Int() int {
  kek, _ := strconv.Atoi(m.Mark[0:1])
  return kek
}

// CalculateLeft calculates how many wantedMark you can get if you want to get markToGet
func (d DisciplineMark) CalculateLeft(wantedMark int, markToGet int) int {
  marksCnt := float64(d.Count())
  marksSum := float64(d.CalculateSum())

  for i := 0; i < 500; i++ {
    if int(math.Round((marksSum+float64(markToGet*(i+1)))/(marksCnt+float64(i+1))-0.1)) < wantedMark {
      return i
    }
  }
  return 0
}

// CalculateNeeded calculates how many wantedMark you can get if you want to get markToGet
func (d DisciplineMark) CalculateNeeded(wantedMark int, markToGet int) int {
  marksCnt := float64(d.Count())
  marksSum := float64(d.CalculateSum())

  for i := 0; i < 500; i++ {
    if int(math.Round((marksSum+float64(markToGet*(i)))/(marksCnt+float64(i))-0.1)) >= wantedMark {
      return i
    }
  }
  return 0
}

// MarksCount gets number of marks
func (s SummaryMarks) MarksCount() int {
  res := 0
  for _, subj := range s.DisciplineMarks {
    res += subj.Count()
  }
  return res
}

// CalculateSum returns sum of all marks
func (d DisciplineMark) CalculateSum() int {
  sum := 0
  for _, m := range d.Marks {
    if kek := m.Int(); 1 <= kek && kek <= 5 {
      sum += kek
    }
  }
  return sum
}

// Count gets number of marks
func (d DisciplineMark) Count() int {
  cnt := 0
  for _, m := range d.Marks {
    if kek := m.Int(); 1 <= kek && kek <= 5 {
      cnt++
    }
  }
  return cnt
}

// CalculateAverage calculates mean marks
func (d DisciplineMark) CalculateAverage() float64 {
  if d.Count() > 0 {
    return float64(d.CalculateSum()) / float64(d.Count())
  } else {
    return 0
  }
}

func (d DisciplineMark) BARSAverage() float64 {
  ans, _ := strconv.ParseFloat(d.AverageMark, 32)
  return ans
}

// GetDay gets HomeworkDay with desired date
func (s ScheduleWithHomework) GetDay(date time.Time) HomeworkDay {
  for _, d := range s.Days {
    if d.Date == date.Format("2006-01-02") {
      return d
    }
  }
  return HomeworkDay{}
}

// GetSubjects gets subject in desired day
func (s HomeworkDay) GetSubjects(shorten bool) []string {
  subjects := make(map[string]bool)
  for _, subj := range s.Homeworks {
    if shorten {
      subjects[subj.ShortenDisciplineName()] = true
    } else {
      subjects[subj.Discipline] = true
    }
  }

  var subjectsStr []string
  for subj := range subjects {
    subjectsStr = append(subjectsStr, subj)
  }
  sort.Strings(subjectsStr[:])
  return subjectsStr
}

func FindNewAndChangedMarks(all []MarkWithDiscipline) SummaryMarks {
  var newOnly []MarkWithDiscipline
  for i := range all {
    if !all[i].FromOld {
      newOnly = append(newOnly, all[i])
    }
  }

  return SummaryMarks{
    DisciplineMarks: GroupMarksWithDiscipline(newOnly),
  }
}

func FindNewMarks(old, new SummaryMarks) SummaryMarks {
  newAndChanged := FindNewAndChangedMarks(FindDifference(old, new))
  changedOnly := FindChangedMarks(old, new)
  var res []MarkWithDiscipline
  for i := range newAndChanged.DisciplineMarks {
    changedDisciplineId := -1
    for j := range changedOnly {
      if newAndChanged.DisciplineMarks[i].Discipline == changedOnly[j].Discipline {
        changedDisciplineId = j
      }
    }
    if changedDisciplineId == -1 {
      for m := range newAndChanged.DisciplineMarks[i].Marks {
        res = append(res,
          MarkWithDiscipline{
            Mark:       newAndChanged.DisciplineMarks[i].Marks[m],
            Discipline: newAndChanged.DisciplineMarks[i].Discipline,
            FromOld:    false,
          })
      }
      continue
    }
    for m := range newAndChanged.DisciplineMarks[i].Marks {
      foundInChanged := false
      for cm := range changedOnly[changedDisciplineId].Marks {
        foundInChanged = foundInChanged || newAndChanged.DisciplineMarks[i].Marks[m] == changedOnly[changedDisciplineId].Marks[cm].Mark
      }

      if !foundInChanged {
        res = append(res,
          MarkWithDiscipline{
            Mark:       newAndChanged.DisciplineMarks[i].Marks[m],
            Discipline: newAndChanged.DisciplineMarks[i].Discipline,
            FromOld:    false,
          })
      }
    }
  }

  return SummaryMarks{
    DisciplineMarks: GroupMarksWithDiscipline(res),
  }
}

func FindDeletedMarks(old, new SummaryMarks) SummaryMarks {
  oldAndChanged := FindDeletedAndChangedMarks(FindDifference(old, new))
  changedOnly := FindChangedMarks(old, new)
  var res []MarkWithDiscipline
  for i := range oldAndChanged.DisciplineMarks {
    changedDisciplineId := -1
    for j := range changedOnly {
      if oldAndChanged.DisciplineMarks[i].Discipline == changedOnly[j].Discipline {
        changedDisciplineId = j
      }
    }
    if changedDisciplineId == -1 {
      for m := range oldAndChanged.DisciplineMarks[i].Marks {
        res = append(res,
          MarkWithDiscipline{
            Mark:       oldAndChanged.DisciplineMarks[i].Marks[m],
            Discipline: oldAndChanged.DisciplineMarks[i].Discipline,
            FromOld:    false,
          })
      }
      continue
    }

    for m := range oldAndChanged.DisciplineMarks[i].Marks {
      foundInChanged := false
      for cm := range changedOnly[changedDisciplineId].Marks {
        foundInChanged = foundInChanged || oldAndChanged.DisciplineMarks[i].Marks[m].Mark == changedOnly[changedDisciplineId].Marks[cm].OldMark
      }

      if !foundInChanged {
        res = append(res,
          MarkWithDiscipline{
            Mark:       oldAndChanged.DisciplineMarks[i].Marks[m],
            Discipline: oldAndChanged.DisciplineMarks[i].Discipline,
            FromOld:    false,
          })
      }
    }
  }

  return SummaryMarks{
    DisciplineMarks: GroupMarksWithDiscipline(res),
  }
}

func FindDeletedAndChangedMarks(all []MarkWithDiscipline) SummaryMarks {
  var newOnly []MarkWithDiscipline
  for i := range all {
    if all[i].FromOld {
      newOnly = append(newOnly, all[i])
    }
  }

  return SummaryMarks{
    DisciplineMarks: GroupMarksWithDiscipline(newOnly),
  }
}

func GroupMarksWithDiscipline(m []MarkWithDiscipline) []DisciplineMark {
  var answ []DisciplineMark
  var cur DisciplineMark
  for i := 0; i < len(m); i++ {
    if i == 0 {
      cur = DisciplineMark{
        Discipline:  m[i].Discipline,
        Marks:       []Mark{m[i].Mark},
        AverageMark: m[i].Mark.Mark,
      }
    } else {
      if m[i].Discipline == m[i-1].Discipline {
        cur.Marks = append(cur.Marks, m[i].Mark)
      } else {
        cur.UpdateAverageMark()
        answ = append(answ, cur)
        cur = DisciplineMark{
          Discipline:  m[i].Discipline,
          Marks:       []Mark{m[i].Mark},
          AverageMark: "",
        }
      }
    }
  }
  if len(cur.Marks) > 0 {
    cur.UpdateAverageMark()
    answ = append(answ, cur)
  }
  return answ
}

// FindDifference find difference between old and new
// old must be a subset of new
func FindDifference(old, new SummaryMarks) []MarkWithDiscipline {
  var oldMarks []MarkWithDiscipline
  var newMarks []MarkWithDiscipline

  for _, d := range old.DisciplineMarks {
    for _, m := range d.Marks {
      oldMarks = append(oldMarks, MarkWithDiscipline{
        Mark:       m,
        Discipline: d.Discipline,
        FromOld:    true,
      })
    }
  }

  for _, d := range new.DisciplineMarks {
    for _, m := range d.Marks {
      newMarks = append(newMarks, MarkWithDiscipline{
        Mark:       m,
        Discipline: d.Discipline,
        FromOld:    false,
      })
    }
  }

  sort.Slice(oldMarks[:], func(i, j int) bool {
    return oldMarks[i].Less(oldMarks[j])
  })

  sort.Slice(newMarks[:], func(i, j int) bool {
    return newMarks[i].Less(newMarks[j])
  })

  i := 0
  j := 0
  n1 := len(oldMarks)
  n2 := len(newMarks)

  var dif []MarkWithDiscipline

  //for i < n1 && j < n2 {
  //	if oldMarks[i].Less(newMarks[j]) && oldMarks[i].Mark != newMarks[j].Mark {
  //		dif = append(dif, oldMarks[i])
  //		i++
  //	} else if oldMarks[i].Mark == newMarks[j].Mark {
  //		i++
  //		j++
  //	} else {
  //		dif = append(dif, newMarks[j])
  //		j++
  //	}
  //}
  //
  //for i < n1 {
  //	dif = append(dif, oldMarks[i])
  //	i++
  //}
  //
  //for j < n2 {
  //	dif = append(dif, newMarks[j])
  //	j++
  //}

  for i = 0; i < n1; i++ {
    exists := false
    for j = 0; j < n2; j++ {
      exists = exists || (oldMarks[i].Discipline == newMarks[j].Discipline && oldMarks[i].Mark == newMarks[j].Mark)
      if exists {
        break
      }
    }
    if !exists {
      dif = append(dif, oldMarks[i])
    }
  }

  for i = 0; i < n2; i++ {
    exists := false
    for j = 0; j < n1; j++ {
      exists = exists || (newMarks[i].Discipline == oldMarks[j].Discipline && newMarks[i].Mark == oldMarks[j].Mark)
      if exists {
        break
      }
    }
    if !exists {
      dif = append(dif, newMarks[i])
    }
  }

  sort.Slice(dif[:], func(i, j int) bool {
    return dif[i].Less(dif[j])
  })

  return dif
}

// GetDate gets mark date
func (m Mark) GetDate() time.Time {
  date, _ := time.Parse("2006-01-02", m.Date)
  return date
}

func (d *DisciplineMark) UpdateAverageMark() {
  averageMark := d.CalculateAverage()
  d.AverageMark = fmt.Sprintf("%.2f", averageMark)
}

func (d *SummaryMarks) Push(discipline string, m Mark) {
  disciplineNumber := -1
  for i, disc := range d.DisciplineMarks {
    if disc.Discipline == discipline {
      disciplineNumber = i
      break
    }
  }
  if disciplineNumber == -1 {
    newDiscipline := DisciplineMark{
      Discipline:  discipline,
      Marks:       []Mark{m},
      AverageMark: m.Mark,
    }
    d.DisciplineMarks = append(d.DisciplineMarks, newDiscipline)
  } else {
    d.DisciplineMarks[disciplineNumber].Marks = append(d.DisciplineMarks[disciplineNumber].Marks, m)
    d.DisciplineMarks[disciplineNumber].UpdateAverageMark()
  }
}

func (d *SummaryMarks) PushManyMarks(marks DisciplineMark) {
  disciplineNumber := -1
  for i, disc := range d.DisciplineMarks {
    if disc.Discipline == marks.Discipline {
      disciplineNumber = i
      break
    }
  }
  if disciplineNumber == -1 {
    d.DisciplineMarks = append(d.DisciplineMarks, marks)
  } else {
    oldDisc := d.DisciplineMarks[disciplineNumber]
    newDisc := marks
    oldMarks := oldDisc.Marks
    newMarks := newDisc.Marks
    var mergedMarks []Mark

    sort.Slice(oldMarks, func(i, j int) bool {
      return oldMarks[i].GetDate().Before(oldMarks[j].GetDate())
    })

    sort.Slice(newMarks, func(i, j int) bool {
      return newMarks[i].GetDate().Before(oldMarks[j].GetDate())
    })

    i := 0
    j := 0
    n1 := len(oldMarks)
    n2 := len(newMarks)

    for i < n1 && j < n2 {
      if oldMarks[i].GetDate().Before(newMarks[j].GetDate()) {
        mergedMarks = append(mergedMarks, oldMarks[i])
        i++
      } else if oldMarks[i] == newMarks[j] {
        mergedMarks = append(mergedMarks, oldMarks[i])
        i++
      } else {
        mergedMarks = append(mergedMarks, newMarks[j])
        j++
      }
    }

    for i < n1 {
      mergedMarks = append(mergedMarks, oldMarks[i])
      i++
    }

    for j < n2 {
      mergedMarks = append(mergedMarks, newMarks[j])
      j++
    }

    d.DisciplineMarks[disciplineNumber].Marks = mergedMarks
    d.DisciplineMarks[disciplineNumber].UpdateAverageMark()
  }
}

func (s SummaryMarks) GetDisciplineByName(name string) (DisciplineMark, error) {
  for i := range s.DisciplineMarks {
    if s.DisciplineMarks[i].Discipline == name {
      return s.DisciplineMarks[i], nil
    }
  }
  return DisciplineMark{}, errdefs.NotFound(errors.New("discipline not found"))
}

func (m Mark) GetOutputDate() string {
  date, err := time.Parse("2006-01-02", m.Date)
  if err != nil {
    return m.Date
  }
  return date.Format("02-01-2006")
}

func (m Mark) Emoji(useEmoji bool) string {
  return emoji.GetEmoji(m.Mark, useEmoji)
}

func (m *SummaryMarks) Sort() {
  sort.Slice(m.DisciplineMarks, func(i, j int) bool {
    return m.DisciplineMarks[i].Discipline < m.DisciplineMarks[j].Discipline
  })
}
