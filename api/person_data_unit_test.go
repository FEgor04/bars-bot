//go:build all || unit
// +build all unit

package api

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	mock "gopkg.in/h2non/gentleman-mock.v2"
	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gock.v1"
	"net/http"
	"testing"
	"time"
)

func TestGetPersonData_GoodPassword(t *testing.T) {
	defer mock.Disable()

	var wantedPersonData = PersonData{
		FullName:    "Test Testov",
		Description: "Test information",
		ID:          42,
		AvaURL:      "supergoodava.png",
		HasAva:      true,
		ClassYear:   "12",
		School:      "MBOU \"KEK LOL\"",
	}

	mock.New("https://test.com").Get("/api/ProfileService/GetPersonData").ReplyFunc(func(r *gock.Response) {
		body := wantedPersonData
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			r.Status(http.StatusInternalServerError)
			r.SetError(err)
			return
		}
		r.Status(http.StatusOK)
		r.BodyBuffer = bodyBytes
	})

	cli := gentleman.New()
	cli.Use(mock.Plugin)
	cli.URL("https://test.com")
	//cli.Use(cookies.Set("sessionid", wantedSessionId))
	//cli.AddCookie(&http.Cookie{Name: "sessionid", Value: wantedSessionId})
	//cli.

	api := API{client: cli, sessionID: wantedSessionID}
	marks, err := RetryGetPersonData(context.Background(), api, 3, time.Millisecond, nil)

	assert.NoError(t, err)
	assert.Equal(t, wantedPersonData, marks)
}

func TestGetPersonData_WrongPassword(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/ProfileService/GetPersonData").ReplyFunc(func(r *gock.Response) {
		r.Status(http.StatusUnauthorized)
		bodyString := "{\n    \"faultcode\": \"Server.UserNotAuthenticated\",\n    \"faultstring\": \"Вы не авторизованы. </br>Обновите страницу для повторной аутентификации.\"\n}"
		r.BodyBuffer = []byte(bodyString)
	})

	cli := gentleman.New()
	cli.URL("https://test.com")
	cli.Use(mock.Plugin)
	//cli.AddCookie(&http.Cookie{Name: "sessionid", Value: "badSessionId"})
	//cli.Use(cookies.Set("sessionid", "badSe"))

	api := API{client: cli}
	marks, err := RetryGetPersonData(context.Background(), api, 3, time.Millisecond, nil)

	assert.ErrorIs(t, ErrUnauthorized, err)
	assert.Equal(t, PersonData{}, marks)
}

func TestGetPersonData_BadGateway(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/ProfileService/GetPersonData").Reply(http.StatusBadGateway)

	cli := gentleman.New()
	cli.URL("https://test.com")
	cli.Use(mock.Plugin)
	//cli.AddCookie(&http.Cookie{Name: "sessionid", Value: "badSessionId"})
	//cli.Use(cookies.Set("sessionid", "badSe"))

	api := API{client: cli}
	marks, err := api.GetPersonData(context.Background())

	assert.Error(t, err)
	assert.Equal(t, PersonData{}, marks)
}
