//go:build unit || all
// +build unit all

package api

import (
	"context"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

var wantedSessionID = "keklolkek"
var wantedLogin = "login"
var wantedPassword = "password"

var GetSessionIDResponder = func(r *http.Request) (*http.Response, error) {
	login := r.FormValue("login_login")
	password := r.FormValue("login_password")
	if login == wantedLogin && password == wantedPassword {
		encoded := []byte(`{"success": true, "redirect": "/"}`)
		response := httpmock.NewBytesResponse(http.StatusOK, encoded)
		response.Header.Set("Set-Cookie", "sessionid="+wantedSessionID)
		return response, nil
	} else if login == "badGateway" && password == "badGateway" {
		return httpmock.NewStringResponse(http.StatusBadGateway, "bad gateway lol"), nil
	} else {
		encoded := []byte(`{"success": false, "redirect": "", "message": "Неверный логин или пароль."}`)
		response := httpmock.NewBytesResponse(http.StatusUnauthorized, encoded)
		return response, nil
	}
}

func TestGetSessionID_GoodPassword(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("POST", baseURL+"/auth/login", GetSessionIDResponder)
	sessionid, err := GetSessionID(context.Background(), wantedLogin, wantedPassword, baseURL)
	assert.Equal(t, 1, httpmock.GetTotalCallCount())
	assert.Nil(t, err)
	assert.Equal(t, wantedSessionID, sessionid)
}

func TestGetSessionID_WrongPassword(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("POST", baseURL+"/auth/login", GetSessionIDResponder)
	sessionid, err := GetSessionID(context.Background(), "kek", "lol", baseURL)
	assert.Equal(t, 1, httpmock.GetTotalCallCount())
	assert.Equal(t, ErrWrongPassword, err)
	assert.Equal(t, "", sessionid)
}

func TestGetSessionID_BadGateway(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("POST", baseURL+"/auth/login", GetSessionIDResponder)
	sessionid, err := GetSessionID(context.Background(), "badGateway", "badGateway", baseURL)
	assert.Equal(t, 1, httpmock.GetTotalCallCount())
	assert.Error(t, err)
	assert.Equal(t, "", sessionid)
}

func TestGetSessionID_PostError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://kek.test.com"
	httpmock.RegisterResponder("POST", "https://test.com"+"/auth/login", GetSessionIDResponder)
	sessionid, err := GetSessionID(context.Background(), "badGateway", "badGateway", baseURL)
	assert.Equal(t, 0, httpmock.GetTotalCallCount())
	assert.Contains(t, err.Error(), "no responder found")
	assert.NotEqual(t, ErrWrongPassword, err)
	assert.NotEqual(t, ErrInternal, err)
	assert.NotEqual(t, nil, err)
	assert.Equal(t, "", sessionid)
}

func TestRegisterSessionID_GoodSessionId(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("GET", baseURL+"/personal-area", func(r *http.Request) (*http.Response, error) {
		sessionCookie, err := r.Cookie("sessionid")
		require.NoError(t, err)
		assert.True(t, sessionCookie.Value == wantedSessionID)
		return httpmock.NewStringResponse(http.StatusOK, "Ok"), nil
	})

	err := RegisterSessionID(context.Background(), wantedSessionID, baseURL)
	assert.NoError(t, err)
	assert.Equal(t, 1, httpmock.GetTotalCallCount())
}

func TestRegisterSessionID_PostError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("GET", baseURL+"/personal-area", func(r *http.Request) (*http.Response, error) {
		sessionCookie, err := r.Cookie("sessionid")
		require.NoError(t, err)
		assert.True(t, sessionCookie.Value == "" || sessionCookie.Value == wantedSessionID)
		return httpmock.NewStringResponse(http.StatusOK, "Ok"), nil
	})

	err := RegisterSessionID(context.Background(), wantedSessionID, "https://kek.test.com")

	assert.Equal(t, 0, httpmock.GetTotalCallCount())
	assert.Contains(t, err.Error(), "no responder found")
	assert.Error(t, err)
	assert.NotEqual(t, ErrWrongPassword, err)
	assert.NotEqual(t, ErrInternal, err)
	assert.NotEqual(t, nil, err)
}

func TestRegisterSessionID_BadGateway(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("GET", baseURL+"/personal-area", func(r *http.Request) (*http.Response, error) {
		sessionCookie, err := r.Cookie("sessionid")
		require.NoError(t, err)
		assert.True(t, sessionCookie.Value == "" || sessionCookie.Value == wantedSessionID)
		return httpmock.NewStringResponse(http.StatusBadGateway, "Ok"), nil
	})

	err := RegisterSessionID(context.Background(), wantedSessionID, baseURL)

	assert.Equal(t, 1, httpmock.GetTotalCallCount())
	assert.Equal(t, ErrInternal, err)
}

func TestCreateFromCredentials_GoodPassword(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("POST", baseURL+"/auth/login", GetSessionIDResponder)
	httpmock.RegisterResponder("GET", baseURL+"/personal-area", func(r *http.Request) (*http.Response, error) {
		sessionCookie, err := r.Cookie("sessionid")
		require.NoError(t, err)
		assert.True(t, sessionCookie.Value == wantedSessionID)
		return httpmock.NewStringResponse(http.StatusOK, "Ok"), nil
	})

	api, err := CreateFromCredentials(context.Background(), wantedLogin, wantedPassword, baseURL)
	assert.NoError(t, err)
	require.NotNil(t, api.client)
	require.NotNil(t, api.sessionID)

	assert.Equal(t, api.sessionID, wantedSessionID)
	assert.Equal(t, api.sessionID, api.GetSessionID())

	assert.Equal(t, 2, httpmock.GetTotalCallCount())
}

func TestCreateFromCredentials_WrongPassword(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	baseURL := "https://test.com"
	httpmock.RegisterResponder("POST", baseURL+"/auth/login", GetSessionIDResponder)
	httpmock.RegisterResponder("GET", baseURL+"/personal-area", func(r *http.Request) (*http.Response, error) {
		sessionCookie, err := r.Cookie("sessionid")
		require.NoError(t, err)
		assert.True(t, sessionCookie.Value == wantedSessionID)
		return httpmock.NewStringResponse(http.StatusOK, "Ok"), nil
	})

	api, err := CreateFromCredentials(context.Background(), wantedLogin+"kek", wantedPassword, baseURL)

	assert.Equal(t, api.sessionID, "")
	assert.Equal(t, err, ErrWrongPassword)

	assert.Equal(t, 1, httpmock.GetTotalCallCount())
}
