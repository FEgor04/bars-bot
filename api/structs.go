package api

import "gopkg.in/h2non/gentleman.v2"

// API is a class to interact with BARS
type API struct {
	client     *gentleman.Client
	authed     bool
	RetryTimes int
	sessionID  string
}

// LoginResponse is a class to store /auth/login response
type LoginResponse struct {
	Redirect string `json:"redirect"`
	Success  bool   `json:"success"`
	Message  string `json:"message"`
}

// SummaryMarks is a class to store summary marks
type SummaryMarks struct {
	Subperiod       MarksSubperiod   `json:"subperiod"`
	Dates           []string         `json:"dates"`
	DisciplineMarks []DisciplineMark `json:"discipline_marks"`
}

// MarksSubperiod is a class to store marks subperiod
type MarksSubperiod struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

// DisciplineMark is a class to store marks of the discipline
type DisciplineMark struct {
	Discipline  string `json:"discipline"`
	Marks       []Mark `json:"marks"`
	AverageMark string `json:"average_mark"`
}

// Mark is a class to store mark data
type Mark struct {
	Date        string `json:"date"`
	Description string `json:"description"`
	Mark        string `json:"mark"`
}

type MarkWithDiscipline struct {
	Mark       `db:"mark"`
	Discipline string `db:"discipline"`
	FromOld    bool   `db:"from_old"`
}

type ChangedMark struct {
	Mark
	OldMark    string
	Discipline string
}

type ChangedMarkDiscipline struct {
	Discipline string
	Marks      []ChangedMark
}

// WeekSchedule is a class to store week schedule
type WeekSchedule struct {
	Days []ScheduleDay `json:"days"`
}

// ScheduleDay is a class to store a day in schedule
type ScheduleDay struct {
	Lessons   []ScheduleLesson `json:"lessons"`
	Date      string           `json:"date"`
	IsWeekend bool             `json:"is_weekend"`
}

// ScheduleLesson is a class to store a lesson
type ScheduleLesson struct {
	Discipline     string `json:"discipline"`
	Index          int    `json:"index"`
	Office         string `json:"office"`
	TimeBegin      string `json:"time_begin"`
	TimeEnd        string `json:"time_end"`
	StudyTimeShift int    `json:"study_time_shift"`
	Date           string `json:"date"`
	Teacher        string `json:"teacher"`
	StudyTimeName  string `json:"study_time_name"`
}

// ScheduleWithHomework is a class to store schedule with homework
type ScheduleWithHomework struct {
	Days []HomeworkDay
}

// HomeworkDay is a class to store a day with homework info
type HomeworkDay struct {
	Date      string     `json:"date"`
	Name      string     `json:"name"`
	Homeworks []Homework `json:"homeworks"`
}

// Homework is a class to store homework data
type Homework struct {
	Discipline              string               `json:"discipline"`
	NextHomework            string               `json:"nextHomework"`
	NextMaterials           []HomeworkMaterial   `json:"nextMaterials"`
	ScheduleLessonType      string               `json:"schedulelessontype"`
	IndividualHomeworks     []IndividualHomework `json:"individualHomeworks"`
	NextIndividualHomeworks []IndividualHomework `json:"nextIndividualHomeworks"`
	Theme                   string               `json:"theme"`
	Materials               []HomeworkMaterial   `json:"materials"`
	Date                    string               `json:"date"`
	Teacher                 string               `json:"teacher"`
	Homework                string               `json:"homework"`
}

// HomeworkMaterial is a class to store attached materials
type HomeworkMaterial struct {
	URL  string `json:"url"`
	Name string `json:"name"`
}

// HomeworkDocument is a class to store homework document
// IDK what fields it has
type HomeworkDocument struct {
}

// IndividualHomework is a class to store individual homework
type IndividualHomework struct {
	Document    HomeworkDocument `json:"document"`
	Description string           `json:"description"`
	LessonID    int              `json:"lesson_id"`
}

// ErrorResponse is a class to store error response
type ErrorResponse struct {
	FaultCode   string `json:"faultcode"`
	FaultString string `json:"faultstring"`
}

// FullSchedule is a class to store full schedule (office + schedule)
type FullSchedule struct {
	Days []FullScheduleDay `json:"days"`
}

// FullScheduleDay is a class to store a day of full schedule
type FullScheduleDay struct {
	Date    string               `json:"date"`
	Name    string               `json:"name"`
	Lessons []FullScheduleLesson `json:"lessons"`
}

// FullScheduleLesson is a class to store a lesson of full schedule
type FullScheduleLesson struct {
	Homework       `json:"homework"`
	ScheduleLesson `json:"schedule_lesson"`
}

// PersonData is a class to store person data
type PersonData struct {
	FullName    string `json:"user_fullname"`
	Description string `json:"user_desc"`
	ID          int    `json:"auth_user_profile_id"`
	AvaURL      string `json:"user_ava_url"`
	HasAva      bool   `json:"user_has_ava"`
	ClassYear   string `json:"selected_pupil_classyear"`
	School      string `json:"selected_pupil_school"`
}
