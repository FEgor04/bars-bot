//go:build all || unit
// +build all unit

package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	mock "gopkg.in/h2non/gentleman-mock.v2"
	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gock.v1"
	"net/http"
	"testing"
	"time"
)

func TestApi_GetScheduleWithHomework(t *testing.T) {
	defer mock.Disable()

	var hwDays []HomeworkDay
	for i := 0; i < 7; i++ {
		dayNow := HomeworkDay{
			Date: fmt.Sprintf("2021-%02d-11", i+1),
			Name: "Test",
			Homeworks: []Homework{
				{
					Discipline:              "Test",
					NextHomework:            "Test",
					NextMaterials:           nil,
					ScheduleLessonType:      "Test",
					IndividualHomeworks:     nil,
					NextIndividualHomeworks: nil,
					Theme:                   "Test",
					Materials:               nil,
					Date:                    fmt.Sprintf("2021-%02d-11", i+1),
					Teacher:                 "Test",
					Homework:                "Test",
				},
			},
		}
		hwDays = append(hwDays, dayNow)
	}

	mock.New("https://test.com").Get("/api/HomeworkService/GetHomeworkFromRange").ReplyFunc(func(r *gock.Response) {
		body := hwDays
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			r.Status(http.StatusInternalServerError)
			r.SetError(err)
			return
		}
		r.Status(http.StatusOK)
		r.BodyBuffer = bodyBytes
	})

	cli := gentleman.New()
	cli.Use(mock.Plugin)
	cli.URL("https://test.com")

	api := API{client: cli}
	schedule, err := RetryGetScheduleWithHomework(context.Background(), api, time.Now(), 3, time.Millisecond, nil)

	require.NoError(t, err)
	assert.Equal(t, hwDays, schedule.Days)
}

func TestGetSchedule_WrongPassword(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/HomeworkService/GetHomeworkFromRange").ReplyFunc(func(r *gock.Response) {
		r.Status(http.StatusUnauthorized)
		bodyString := "{\n    \"faultcode\": \"Server.UserNotAuthenticated\",\n    \"faultstring\": \"Вы не авторизованы. </br>Обновите страницу для повторной аутентификации.\"\n}"
		r.BodyBuffer = []byte(bodyString)
	})

	cli := gentleman.New()
	cli.URL("https://test.com")
	cli.Use(mock.Plugin)

	api := API{client: cli}
	schedule, err := RetryGetScheduleWithHomework(context.Background(), api, time.Now(), 3, time.Millisecond, nil)

	assert.ErrorIs(t, ErrUnauthorized, err)
	assert.Equal(t, ScheduleWithHomework{}, schedule)
}

func TestGetSchedule_BadGateway(t *testing.T) {
	defer mock.Disable()
	mock.New("https://test.com").Get("/api/HomeworkService/GetHomeworkFromRange").Reply(http.StatusBadGateway)

	cli := gentleman.New()
	cli.URL("https://test.com")
	cli.Use(mock.Plugin)

	api := API{client: cli}
	schedule, err := api.GetScheduleWithHomework(context.Background(), time.Now())

	assert.Error(t, err)
	assert.Equal(t, ScheduleWithHomework{}, schedule)
}

func TestGetDay(t *testing.T) {
	var hwDays []HomeworkDay
	for i := 0; i < 7; i++ {
		dayNow := HomeworkDay{
			Date: fmt.Sprintf("2021-11-%02d", i+1),
			Name: "Test",
			Homeworks: []Homework{
				{
					Discipline:              "Test",
					NextHomework:            "Test",
					NextMaterials:           nil,
					ScheduleLessonType:      "Test",
					IndividualHomeworks:     nil,
					NextIndividualHomeworks: nil,
					Theme:                   "Test",
					Materials:               nil,
					Date:                    fmt.Sprintf("2021-11-%02d", i+1),
					Teacher:                 "Test",
					Homework:                "Test",
				},
			},
		}
		hwDays = append(hwDays, dayNow)
	}
	schedule := ScheduleWithHomework{Days: hwDays}
	wantedDay := time.Date(2021, time.November, 1, 0, 0, 0, 0, time.Now().Location())
	t.Logf("wanted day: %s", wantedDay.Format("2006-01-02"))
	day := schedule.GetDay(wantedDay)
	require.NotEqual(t, HomeworkDay{}, day)
}
