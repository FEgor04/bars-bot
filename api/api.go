package api

import (
	"context"
	"encoding/json"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"go.opentelemetry.io/otel/attribute"
	"gopkg.in/eapache/go-resiliency.v1/retrier"
	retry "gopkg.in/h2non/gentleman-retry.v2"
	"gopkg.in/h2non/gentleman.v2/plugins/timeout"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gentleman.v2/plugins/cookies"
	"gopkg.in/h2non/gentleman.v2/plugins/transport"
)

// BarsErrorsCounter is a counter of BARS errors
var BarsErrorsCounter = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: "bars_api_errors",
	Help: "Total ammount of BARS API errors",
}, []string{"error", "endpoint"})

// RequestError Enum for Request error
const RequestError = "request_error"

// ClientDoError Enum for error that may occur on client.do
const ClientDoError = "client_do_error"

// BodyReadError Enum for error that may occur on body read
const BodyReadError = "body_read_error"

// BadGatewayError Enum for Bad Gateway error
const BadGatewayError = "bad_gateway_error"

// PromUnauthorizedError Enum for unauthorized error
const PromUnauthorizedError = "unauthorized_error"

// ForbiddenError Enum for forbidden error
const ForbiddenError = "forbidden_error"

// JSONUnmarshalError Enum for error that may occur on json.unmarshal
const JSONUnmarshalError = "unmarshal_error"

const httpTimeout = 30 * time.Second

// GetSessionID gets new session id for BARS API
func GetSessionID(ctx context.Context, login, password, baseURL string) (string, error) {
	var span trace.Span
	var endpoint = "GetSessionID"
	ctx, span = otel.Tracer("api").Start(ctx, endpoint)
	defer span.End()

	data := url.Values{
		"login_login":    {login},
		"login_password": {password},
	}

	client := http.Client{Timeout: 5 * time.Second}
	client.Transport = otelhttp.NewTransport(http.DefaultTransport)

	req, err := http.NewRequestWithContext(ctx, "POST", baseURL+"/auth/login", strings.NewReader(data.Encode()))
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		BarsErrorsCounter.WithLabelValues(RequestError, endpoint).Inc()
		return "", err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(BodyReadError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return "", err
	}
	span.SetAttributes(attribute.String("html.text", string(body)))
	span.SetAttributes(attribute.Int("html.code", resp.StatusCode))

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusBadGateway {
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return "", ErrInternal
		}
		if resp.StatusCode == http.StatusUnauthorized || resp.StatusCode == http.StatusForbidden {
			BarsErrorsCounter.WithLabelValues(PromUnauthorizedError, endpoint).Inc()
			return "", ErrWrongPassword
		}
		BarsErrorsCounter.WithLabelValues("unknown_error", endpoint).Inc()
		return "", ErrUnknownLogin
	}

	loginResp := LoginResponse{}
	err = json.Unmarshal(body, &loginResp)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(JSONUnmarshalError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return "", err
	}

	cookiesNow := resp.Cookies()

	var sessionID string

	for i := range cookiesNow {
		if cookiesNow[i].Name == "sessionid" {
			sessionID = cookiesNow[i].Value
		}
	}
	return sessionID, nil
}

// RegisterSessionID registers provided session id
func RegisterSessionID(ctx context.Context, sessionID, baseURL string) error {
	var endpoint = "RegisterSessionID"
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, endpoint)
	defer span.End()

	client := http.Client{Timeout: httpTimeout}
	client.Transport = otelhttp.NewTransport(http.DefaultTransport)

	req, err := http.NewRequestWithContext(ctx, "GET", baseURL+"/personal-area#diary", nil)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(RequestError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}
	req.AddCookie(&http.Cookie{Name: "sessionid", Value: sessionID})
	resp, err := client.Do(req)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}

	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(BodyReadError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}

	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusBadGateway {
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return ErrInternal
		}
		BarsErrorsCounter.WithLabelValues("unknown_error", endpoint).Inc()
		return ErrUnknownLogin
	}
	return nil
}

// CreateFromCredentials creates object of API class from credentials
func CreateFromCredentials(ctx context.Context, login, password, baseURL string) (API, error) {
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, "CreateFromCredentials")
	defer span.End()

	sessionID, err := GetSessionID(ctx, login, password, baseURL)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return API{}, err
	}
	err = RegisterSessionID(ctx, sessionID, baseURL)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return API{}, err
	}
	return CreateFromSessionID(sessionID, baseURL), nil
}

// CreateFromSessionID create object of API class from sessionID
func CreateFromSessionID(sessionID, url string) API {
	client := gentleman.New()
	client.BaseURL(url)
	client.Use(cookies.Set("sessionid", sessionID))
	return API{
		client:     client,
		authed:     true,
		RetryTimes: 0,
		sessionID:  sessionID,
	}
}

// GetPersonData gets person data
func (a API) GetPersonData(ctx context.Context) (PersonData, error) {
	const endpoint = "GetPersonData"
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, endpoint)
	defer span.End()

	myClient := a.client
	myClient.Use(timeout.Request(httpTimeout))
	myClient.Use(retry.New(retrier.New(retrier.ExponentialBackoff(3, time.Second), nil)))
	myClient.UseContext(ctx)
	myClient.Use(transport.Set(otelhttp.NewTransport(http.DefaultTransport)))

	req := myClient.Request()
	resp, err := req.Path("/api/ProfileService/GetPersonData").Method("GET").Use(cookies.Set("sessionid", a.sessionID)).Send()
	if err != nil {
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		return PersonData{}, err
	}
	if !resp.Ok {
		switch resp.StatusCode {
		case http.StatusBadGateway:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return PersonData{}, ErrInternal
		case http.StatusInternalServerError:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return PersonData{}, ErrInternal
		case http.StatusUnauthorized:
			BarsErrorsCounter.WithLabelValues(PromUnauthorizedError, endpoint).Inc()
			return PersonData{}, ErrUnauthorized
		case http.StatusForbidden:
			BarsErrorsCounter.WithLabelValues(ForbiddenError, endpoint).Inc()
			return PersonData{}, ErrUnauthorized
		}
	}
	var personData PersonData
	err = resp.JSON(&personData)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(JSONUnmarshalError, endpoint).Inc()
		return PersonData{}, err
	}
	return personData, err
}

// GetPersonMarks gets summary marks
func (a API) GetPersonMarks(ctx context.Context, date time.Time) (SummaryMarks, error) {
	const endpoint = "GetSummaryMarks"
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, endpoint)
	defer span.End()

	myClient := a.client
	myClient.Use(retry.New(retrier.New(retrier.ExponentialBackoff(3, time.Second), nil)))
	myClient.Use(timeout.Request(httpTimeout))
	myClient.UseContext(ctx)
	myClient.Use(transport.Set(otelhttp.NewTransport(http.DefaultTransport)))

	req := myClient.Request()
	req.Method("GET")
	req.Path("/api/MarkService/GetSummaryMarks")
	req.SetQuery("date", date.Format("2006-01-02"))
	resp, err := req.Do()
	if err != nil {
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		return SummaryMarks{}, err
	}
	if !resp.Ok {
		switch resp.StatusCode {
		case http.StatusBadGateway:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return SummaryMarks{}, ErrInternal
		case http.StatusInternalServerError:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return SummaryMarks{}, ErrInternal
		case http.StatusUnauthorized:
			BarsErrorsCounter.WithLabelValues(PromUnauthorizedError, endpoint).Inc()
			return SummaryMarks{}, ErrUnauthorized
		case http.StatusForbidden:
			BarsErrorsCounter.WithLabelValues(ForbiddenError, endpoint).Inc()
			return SummaryMarks{}, ErrUnauthorized
		}
	}
	var val SummaryMarks
	err = resp.JSON(&val)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(JSONUnmarshalError, endpoint).Inc()
		return SummaryMarks{}, err
	}

	val.Sort()
	return val, nil
}

// GetScheduleWithHomework gets schedule with homework
func (a API) GetScheduleWithHomework(ctx context.Context, date time.Time) (ScheduleWithHomework, error) {
	const endpoint = "GetScheduleWithHomework"
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, endpoint)
	defer span.End()

	myClient := a.client
	myClient.Use(retry.New(retrier.New(retrier.ExponentialBackoff(3, time.Second), nil)))
	myClient.Use(timeout.Request(httpTimeout))
	myClient.UseContext(ctx)
	myClient.Use(transport.Set(otelhttp.NewTransport(http.DefaultTransport)))

	req := myClient.Request()
	req.Method("GET")
	req.Path("/api/HomeworkService/GetHomeworkFromRange")
	req.SetQuery("date", date.Format("2006-01-02"))
	resp, err := req.Do()
	if err != nil {
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		return ScheduleWithHomework{}, err
	}
	if !resp.Ok {
		switch resp.StatusCode {
		case http.StatusBadGateway:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return ScheduleWithHomework{}, ErrInternal
		case http.StatusInternalServerError:
			BarsErrorsCounter.WithLabelValues(BadGatewayError, endpoint).Inc()
			return ScheduleWithHomework{}, ErrInternal
		case http.StatusUnauthorized:
			BarsErrorsCounter.WithLabelValues(PromUnauthorizedError, endpoint).Inc()
			return ScheduleWithHomework{}, ErrUnauthorized
		case http.StatusForbidden:
			BarsErrorsCounter.WithLabelValues(ForbiddenError, endpoint).Inc()
			return ScheduleWithHomework{}, ErrUnauthorized
		}
	}
	var val ScheduleWithHomework
	err = resp.JSON(&val.Days)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(JSONUnmarshalError, endpoint).Inc()
		return ScheduleWithHomework{}, err
	}
	return val, nil
}

// GetFile gets file
func GetFile(ctx context.Context, baseURL, fileURL string) ([]byte, error) {
	const endpoint = "GetFile"

	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, "GetFile")
	defer span.End()

	client := http.Client{Timeout: httpTimeout}
	client.Transport = otelhttp.NewTransport(http.DefaultTransport)

	req, err := http.NewRequestWithContext(ctx, "GET", baseURL+fileURL, nil)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(RequestError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}
	response, err := client.Do(req)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(ClientDoError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		BarsErrorsCounter.WithLabelValues(BodyReadError, endpoint).Inc()
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	return body, err
}
