//go:build integration || all
// +build integration all

package api

import (
	"context"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/suite"
	"gitlab.com/bars-bot/bars-bot/config"
	"testing"
	"time"
)

var dateToGet = time.Date(2021, time.December, 1, 0, 0, 0, 0, time.Local)

type GoodPasswordTestSuite struct {
	suite.Suite
	api API
}

func (g *GoodPasswordTestSuite) SetupTest() {
	var err error
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath("..")
	_ = viper.ReadInConfig()
	config.LoadConfigs()
	viper.SetDefault("BARS_URL", "https://school.karelia.ru")
	login := viper.GetString("BARS_LOGIN")
	password := viper.GetString("BARS_PASSWORD")
	url := viper.GetString("BARS_URL")
	g.api, err = RetryCreateFromCredentials(context.Background(), login, password, url, 3, time.Second, nil)
	g.Require().NoError(err)
	g.Require().NotEqual("", g.api.sessionID)
}

func (g GoodPasswordTestSuite) TestGetPersonData() {
	pData, err := RetryGetPersonData(context.Background(), g.api, 5, time.Second*2, nil)
	g.Assert().NoError(err)
	expectedPersonData := PersonData{ // TODO Store expected result in config
		FullName:    "Федоров Егор Владимирович",
		Description: "Ученик 11 ИМП класса",
		ID:          200229,
		AvaURL:      "/static/../../../../static/profiles/img/noavatar_big.png",
		HasAva:      false,
		ClassYear:   "11 ИМП",
		School:      "МОУ «Державинский лицей»",
	}
	g.Assert().Equal(expectedPersonData, pData)
}

func (g GoodPasswordTestSuite) TestAuth() {
	g.Assert().NotEqual("", g.api.GetSessionID())
}

func (g GoodPasswordTestSuite) TestGetMarks() {
	marks, err := g.api.GetPersonMarks(context.Background(), dateToGet)
	g.Assert().NoError(err)
	g.Assert().NotEqual(0, len(marks.DisciplineMarks))
}

func (g GoodPasswordTestSuite) TestGetSchedule() {
	schedule, err := g.api.GetScheduleWithHomework(context.Background(), dateToGet)
	g.Assert().NoError(err)
	g.Assert().NotEqual(0, len(schedule.Days))
	g.Assert().NotEqual(nil, schedule.Days)
}

func TestGoodApiTestSuite(t *testing.T) {
	suite.Run(t, new(GoodPasswordTestSuite))
}
