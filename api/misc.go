package api

import (
	"context"
	"fmt"
	"gitlab.com/bars-bot/bars-bot/emoji"
	"regexp"
	"time"

	strip "github.com/grokify/html-strip-tags-go"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// ShortenDisciplineName shortens discipline name
func (d DisciplineMark) ShortenDisciplineName() string {
	return ShortenDisciplineName(d.Discipline)
}

// RetryGetSummaryMarks gets summary marks. it makes desired number of retries with the desired delay between them
func RetryGetSummaryMarks(ctx context.Context, api API, date time.Time, retries int, delay time.Duration, logger *zap.Logger) (SummaryMarks, error) {
	return api.GetPersonMarks(ctx, date)
}

// RetryGetPersonData gets person data. it makes desired number of retries with the desired delay between them
func RetryGetPersonData(ctx context.Context, api API, retries int, delay time.Duration, logger *zap.Logger) (PersonData, error) {
	return api.GetPersonData(ctx)
}

// RetryGetScheduleWithHomework gets schedule with homework. it makes desired number of retries with the desired delay between them
func RetryGetScheduleWithHomework(ctx context.Context, api API, date time.Time, retries int, delay time.Duration, logger *zap.Logger) (ScheduleWithHomework, error) {
	return api.GetScheduleWithHomework(ctx, date)
}

// RetryCreateFromCredentials creates api from credentials. it makes desired number of retries with the desired delay between them
func RetryCreateFromCredentials(ctx context.Context, login, password, baseurl string, retries int, delay time.Duration, logger *zap.Logger) (API, error) {
	var span trace.Span
	ctx, span = otel.Tracer("api").Start(ctx, "RetryCreateFromCredentials")
	defer span.End()

	for r := 0; ; r++ {
		span.SetAttributes(attribute.Int("attempt", r))
		span.SetAttributes(attribute.Int("max_retries", retries))
		span.SetAttributes(attribute.String("delay", delay.String()))
		api, err := CreateFromCredentials(ctx, login, password, baseurl)
		if err == nil {
			if logger != nil {
				logger.Info("Success", zap.Int("attempt", r+1))
			}
			return api, err
		}
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		if logger != nil {
			logger.Warn("Failed", zap.Error(err))
		}
		if err == ErrUnauthorized || err == ErrWrongPassword {
			if logger != nil {
				logger.Info("Leaving because of bad password")
			}
			return api, err
		}
		if r >= retries {
			if logger != nil {
				logger.Warn("Goodbye cruel world")
			}
			return api, err
		}
		time.Sleep(delay)
	}
}

// ShortenDisciplineName shortens discipline name
func ShortenDisciplineName(s string) string {
	switch s {
	case "Математика: алгебра и начала математического анализа, геометрия":
		return "Математика"
	case "Курсы по выбору. Наглядные методы решения алгебраических задач повышенной сложности.":
		return "КПВ Математика"
	case "Курсы по выбору. Практикум по информатике.":
		return "КПВ Информатика"
	case "Курс по выбору. Практикум по русскому языку":
		return "КПВ Русский язык"
	case "Курсы по выбору. Решение задач по физике.":
		return "КПВ Физика"
	default:
		return s
	}
}

// ShortenDisciplineName shortens discipline name
func (h Homework) ShortenDisciplineName() string {
	return ShortenDisciplineName(h.Discipline)
}

// GetHomework gets homework and cleans it from html tags
func (h Homework) GetHomework() string {
	if h.Homework != "" {
		var re = regexp.MustCompile("<br/>")
		newHomework := re.ReplaceAllString(h.Homework, "; ")
		return strip.StripTags(newHomework)
	}
	return "нет ДЗ"
}

// GetTheme gets theme
func (h Homework) GetTheme() string {
	if h.Theme != "" {
		return h.Theme
	}
	return "не указана"
}

func FindChangedMarks(old, new SummaryMarks) []ChangedMarkDiscipline {
	difference := FindDifference(old, new)

	deleted := FindDeletedAndChangedMarks(difference)
	added := FindNewAndChangedMarks(difference)

	var changed []ChangedMark

	for i := range deleted.DisciplineMarks {
		for j := range added.DisciplineMarks {
			if deleted.DisciplineMarks[i].Discipline == added.DisciplineMarks[j].Discipline {
				for x := range deleted.DisciplineMarks[i].Marks {
					for y := range added.DisciplineMarks[j].Marks {
						if deleted.DisciplineMarks[i].Marks[x].Description == added.DisciplineMarks[j].Marks[y].Description &&
							deleted.DisciplineMarks[i].Marks[x].Date == added.DisciplineMarks[j].Marks[y].Date {
							changed = append(changed, ChangedMark{
								Mark:       added.DisciplineMarks[j].Marks[y],
								OldMark:    deleted.DisciplineMarks[i].Marks[x].Mark,
								Discipline: deleted.DisciplineMarks[i].Discipline,
							})
						}
					}
				}
			}
		}
	}

	var current ChangedMarkDiscipline
	var res []ChangedMarkDiscipline
	for i := range changed {
		if i == 0 {
			current = ChangedMarkDiscipline{
				Discipline: changed[i].Discipline,
				Marks:      []ChangedMark{changed[i]},
			}
		} else {
			if changed[i].Discipline == changed[i-1].Discipline {
				current.Marks = append(current.Marks, changed[i])
			} else {
				res = append(res, current)
				current = ChangedMarkDiscipline{
					Discipline: changed[i].Discipline,
					Marks:      []ChangedMark{changed[i]},
				}
			}
		}
	}

	if current.Discipline != "" {
		res = append(res, current)
	}

	return res
}

func GetDifferenceMessage(old, new SummaryMarks, useEmoji, showDate, showDescription bool, logger *zap.Logger) (string, int) {
	newMarks := FindNewMarks(old, new)
	deletedMarks := FindDeletedMarks(old, new)
	changedMarks := FindChangedMarks(old, new)
	disciplinesInReport := make(map[string]bool)
	text := ""

	totalMarksCount := 0
	if newMarks.MarksCount() > 0 {
		text += "Полученные оценки"
		for _, discipline := range newMarks.DisciplineMarks {
			text += "\n"
			disciplinesInReport[discipline.Discipline] = true
			text += discipline.ShortenDisciplineName()

			for _, mark := range discipline.Marks {
				text += "\n"
				text += mark.Emoji(useEmoji)
				totalMarksCount++

				if showDate {
					text += " (" + mark.GetOutputDate() + ")"
				}
				if showDescription {
					text += " - " + mark.Description
				}
			}

		}
	}

	if deletedMarks.MarksCount() >= 1 {
		text += "\n\nУдаленные оценки"
		for _, discipline := range deletedMarks.DisciplineMarks {
			text += "\n"
			disciplinesInReport[discipline.Discipline] = true
			text += discipline.ShortenDisciplineName()

			for _, mark := range discipline.Marks {
				text += "\n"
				text += mark.Emoji(useEmoji)
				totalMarksCount++

				if showDate {
					text += " (" + mark.GetOutputDate() + ")"
				}
				if showDescription {
					text += " - " + mark.Description
				}
			}
		}
	}

	if len(changedMarks) >= 1 {
		text += "\n\n"
		text += "Исправленные оценки"

		for _, subj := range changedMarks {
			disciplinesInReport[subj.Discipline] = true
			text += "\n"
			text += ShortenDisciplineName(subj.Discipline) + "\n"
			for _, m := range subj.Marks {
				text += "\n"
				totalMarksCount++
				text += emoji.GetEmoji(m.OldMark, useEmoji) + " -> " + m.Emoji(useEmoji)

				if showDate {
					text += " (" + m.GetOutputDate() + ")"
				}
				if showDescription {
					text += " - " + m.Description
				}
			}
		}
	}

	deltaText := ""
	for discipline := range disciplinesInReport {
		was, err1 := old.GetDisciplineByName(discipline)
		became, err2 := new.GetDisciplineByName(discipline)
		if err1 != nil || err2 != nil {
			logger.Warn("could not found discipline", zap.Error(err1), zap.Error(err2), zap.String("discipline", discipline))
			continue
		}

		wasAverage := was.BARSAverage()
		becameAverage := became.BARSAverage()
		if wasAverage-becameAverage == 0 {
			deltaText += fmt.Sprintf("\n%s: %.2f", was.ShortenDisciplineName(), becameAverage)
		} else {
			delta := fmt.Sprintf("%.2f", becameAverage-wasAverage)
			if becameAverage-wasAverage > 0 {
				delta = "+" + delta
			}

			deltaText += fmt.Sprintf("\n%s: %.2f -> %.2f (%s)", was.ShortenDisciplineName(), wasAverage, becameAverage, delta)
		}
	}

	if len(deltaText) > 0 {
		text += "\n\n"
		text += deltaText
	}

	return text, len(disciplinesInReport)
}
