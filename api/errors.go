package api

import "errors"

var (
	// ErrInternal is an internal server error
	ErrInternal = errors.New("internal server error")
	// ErrWrongPassword is a wrong password error
	ErrWrongPassword = errors.New("wrong password")
	// ErrUnknownLogin is an unknown login error
	ErrUnknownLogin = errors.New("unknown login error")
	// ErrUnauthorized is an unauthorized error
	ErrUnauthorized = errors.New("user unauthorized")
)
