VERSION := $(shell git rev-parse --short HEAD)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
ifeq (${BRANCH}, main)
	TAG := stable
else
	TAG := unstable
endif

build: build-handler build-updater
build-handler: ./handler ./api ./database/ ./cache/ ./config/ ./emoji/
	CGO_ENABLED=0 go build -ldflags "-s -w" -o bot-handler ./handler
build-updater: ./updater/ ./api/ ./database/ ./config/ ./emoji/
	CGO_ENABLED=0 go build -ldflags "-s -w" -o bot-updater ./updater
test:
	gotestsum --format testname -- -tags=all -count=1 -race ./...
unit-test:
	gotestsum --format testname -- -tags=unit -race ./...
integration-test:
	gotestsum --format testname -- -tags=integration -count=1 -race ./...
build-stripped: build
	upx bot-handler
	upx bot-updater
lint:
	golangci-lint run --config=.golangci.yml ./...
generate-keys:
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx/key.pem -out nginx/cert.pem

deploy-old: build
	cp bot-handler ./ansible/files/
	cp bot-updater ./ansible/files/
	cp -r database/migrations ./ansible/files/
	ansible-playbook ansible/deploy-old.yml --extra-vars '{version: $(VERSION)}'

deploy-staging-old: build
	cp bot-handler ./ansible/files/
	cp bot-updater ./ansible/files/
	cp -r database/migrations ./ansible/files/
	ansible-playbook ansible/deploy-old.yml --extra-vars '{version: staging}'

build-docker:
	docker-compose build

push-docker: build-docker
	docker-compose push

deploy: push-docker
	ansible-playbook ansible/deploy.yml -i ~/.ansible/hosts.ini --extra-vars '{version: $(VERSION)}'

deploy-staging: push-docker
	ansible-playbook ansible/deploy.yml -i ~/.ansible/hosts.ini --extra-vars '{version: staging}'

buildx-handler:
	docker buildx build --platform linux/amd64,linux/arm64 -f Dockerfile.handler --build-arg VERSION_BUILD=${VERSION} -t fegor04/handler:${TAG} --push .

buildx-updater:
	docker buildx build --platform linux/amd64,linux/arm64 -f Dockerfile.updater --build-arg VERSION_BUILD=${VERSION} -t fegor04/updater:${TAG} --push .

buildx: 
	make buildx-handler &
	make buildx-updater
