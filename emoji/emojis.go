package emoji

const (
	// ExclamationMarkEmoji ❗
	ExclamationMarkEmoji = "❗"
	// VictorySign ✌
	VictorySign = "✌"
	// CheckMark ✅
	CheckMark = "✅"
	// ThumbsUpEmoji 👍
	ThumbsUpEmoji = "👍"
	// DigitOne 1️⃣
	DigitOne = "1️⃣"
	// DigitTwo 2️⃣
	DigitTwo = "2️⃣"
	// DigitThree 3️⃣
	DigitThree = "3️⃣"
	// DigitFour 4️⃣
	DigitFour = "4️⃣"
	// DigitFive 5️⃣
	DigitFive = "5️⃣"
	// Minus ➖
	Minus = "➖"
	// Plus ➕
	Plus = "➕"
	// LoudlyCryingFace 😭
	LoudlyCryingFace = "😭"
	// CryingFace 😢
	CryingFace = "😢"
	// ShockedFace 🤯
	ShockedFace = "🤯"
	// CrossMark ❌
	CrossMark = "❌"
	// DotMark ⏺
	DotMark = "⏺"
)

func GetEmoji(m string, useEmoji bool) string {
	if !useEmoji {
		return m
	}
	answ := ""
	if m[0:1] == "5" {
		answ += DigitFive
	}
	if m[0:1] == "4" {
		answ += DigitFour
	}
	if m[0:1] == "3" {
		answ += DigitThree
	}
	if m[0:1] == "2" {
		answ += DigitTwo
	}
	if m[0:1] == "1" {
		answ += DigitOne
	}
	if m[0:1] == "." {
		answ += DotMark
	}
	if len(m) >= 2 {
		if m[2:] == "+" {
			answ += Plus
		} else if m[2:] == "-" {
			answ += Minus
		}
	}
	return answ
}

// React gets reaction for mark or returns nothing
func React(m int, useEmoji bool) string {
	if !useEmoji {
		return ""
	}
	switch m {
	case 5:
		return ShockedFace
	case 4:
		return ThumbsUpEmoji
	case 3:
		return ExclamationMarkEmoji
	case 2:
		return CryingFace
	case 1:
		return LoudlyCryingFace
	}
	return ""
}
