package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// DatabaseConfig is a class to store database config
type DatabaseConfig struct {
	Host             string
	Port             int
	Database         string
	User             string
	Password         string
	SslMode          string
	MigrationsFolder string
}

// ConnString gets postgres connection string
func (c DatabaseConfig) ConnString() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", c.User, c.Password, c.Host, c.Port, c.Database, c.SslMode)
}

// NewDatabaseConfig creates new database config
func NewDatabaseConfig() DatabaseConfig {
	return DatabaseConfig{
		Host:             viper.GetString("POSTGRES_HOST"),
		Port:             viper.GetInt("POSTGRES_PORT"),
		Database:         viper.GetString("POSTGRES_NAME"),
		User:             viper.GetString("POSTGRES_USER"),
		Password:         viper.GetString("POSTGRES_PASSWORD"),
		SslMode:          viper.GetString("POSTGRES_SSL_MODE"),
		MigrationsFolder: viper.GetString("MIGRATIONS_FOLDER"),
	}
}

// BotConfig is a class to store bot config
type BotConfig struct {
	Token            string
	WebhookURL       string
	Version          string
	UpdaterURL       string
	SetIntervalToken string // Token to set Interval
	JaegerEndpoint   string
	Environment      string
	LogFolder        string
}

// NewBotConfig creates new bot config
func NewBotConfig() BotConfig {
	return BotConfig{
		Token:            viper.GetString("TOKEN"),
		WebhookURL:       viper.GetString("WEBHOOK_URL"),
		Version:          viper.GetString("VERSION"),
		UpdaterURL:       viper.GetString("UPDATER_URL"),
		SetIntervalToken: viper.GetString("SET_INTERVAL_TOKEN"),
		JaegerEndpoint:   viper.GetString("JAEGER_ENDPOINT"),
		Environment:      viper.GetString("ENVIRONMENT"),
		LogFolder:        viper.GetString("LOGFOLDER"),
	}
}

// RedisConfig is a class to store Redis config
type RedisConfig struct {
	Host     string
	Port     int
	Password string
	Database int
}

// NewRedisConfig creates new redis config
func NewRedisConfig() RedisConfig {
	return RedisConfig{
		Host:     viper.GetString("REDIS_HOST"),
		Port:     viper.GetInt("REDIS_PORT"),
		Password: viper.GetString("REDIS_PASS"),
		Database: viper.GetInt("REDIS_DB"),
	}
}

// LoadConfigs loads all the configs
func LoadConfigs() {
	viper.AutomaticEnv()
	viper.SetDefault("REDIS_HOST", "redis")
	viper.SetDefault("REDIS_PORT", 6379)
	viper.SetDefault("REDIS_DB", 0)
	viper.SetDefault("POSTGRES_SSL_MODE", "disable")
	viper.SetDefault("INTERNAL_PORT", 5000)
	viper.SetDefault("MIGRATIONS_FOLDER", "/database/migrations")
	viper.SetDefault("ENVIRONMENT", "dev")
	viper.AutomaticEnv()
}
