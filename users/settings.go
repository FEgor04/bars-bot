package users

const (
	// SettingsUseEmoji - should bot use emoji
	SettingsUseEmoji = iota
	// SettingsScheduleShowTeacher - should bot show teacher in schedule
	SettingsScheduleShowTeacher
	// SettingsScheduleShowDescription - should bot show theme in schedule
	SettingsScheduleShowDescription
	// SettingsUpdatesShowMarkDate - should bot show mark date on update
	SettingsUpdatesShowMarkDate
	// SettingsUpdatesShowMarkDescription should bot show mark theme on update
	SettingsUpdatesShowMarkDescription
	// SettingsUpdatesShowAverage should bot show discipline average on update
	SettingsUpdatesShowAverage
	// SettingsMarksShowGoodAverageMark should bot show average mark (i.e. arithmetic mean of all marks)
	SettingsMarksShowGoodAverageMark
)

// DefaultSettings is a default user settings
const DefaultSettings = 57

// SettingsNames are names of all settings
var SettingsNames = []string{
	"Использовать эмодзи",
	"Показывать учителя (расписание)",
	"Показывать тему (расписание)",
	"Показывать дату (уведомления)",
	"Показывать тему (уведомление)",
	"Показывать средний балл (уведомление)",
	//"Показывать кабинет (расписание)",
	"Показывать средний арифметический балл",
}
