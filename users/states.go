package users

const (
	// StateEmptyUser - empty user
	StateEmptyUser = iota
	// StateLoginWanted - login input in /start
	StateLoginWanted
	// StatePasswordWanted - password input in /start
	StatePasswordWanted
	// StateUserLoggedIn - default user state
	StateUserLoggedIn
	// StateNewsTextWanted - news input in /addnews
	StateNewsTextWanted
	// StateNewsConfirmWanted - news confirmation in /addnews
	StateNewsConfirmWanted
	// StateReportTextWanted - report input in /report
	StateReportTextWanted
	// StateAdminReportAnswerWanted - answer report in /admin -> answer report
	StateAdminReportAnswerWanted
	// StateHomeworkDateWanted - date input in /addhw
	StateHomeworkDateWanted
	// StateHomeworkTextWanted - text input in /addhw
	StateHomeworkTextWanted
	// StateHomeworkSubjectWanted - subject input in /addhw
	StateHomeworkSubjectWanted
	// StateHomeworkFileWanted - file input in /addhw
	StateHomeworkFileWanted
)
