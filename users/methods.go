package users

import (
	"context"
	"time"

	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/database"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// GetMaskBit gets i-th mask bit from the right
// e.g. mask: 1101. i = 1, bit is True
func GetMaskBit(mask, i int) bool {
	return ((mask >> i) & 1) == 1
}

// GetUserByChatID gets user from database by chat ID
func GetUserByChatID(ctx context.Context, userRepo *database.UserRepository, chatID int64) (User, error) {
	var span trace.Span
	ctx, span = otel.Tracer(packageName).Start(ctx, "GetUserByChatID", trace.WithAttributes(attribute.Int64("user.id", chatID)))
	defer span.End()
	repositoryUser, err := userRepo.GetUserByChatID(ctx, chatID)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return User{}, err
	}
	return NewUserFromRepositoryUser(repositoryUser), nil
}

// GetUsersThatNeedMarksUpdate gets users that need marks update
// i.e. now() - update_time() > interval
func GetUsersThatNeedMarksUpdate(ctx context.Context, userRepo *database.UserRepository, interval int) ([]User, error) {
	var span trace.Span
	ctx, span = otel.Tracer(packageName).Start(ctx, "GetUsersThatNeedMarksUpdate")
	defer span.End()
	usersRaw, err := userRepo.GetUsersThanNeedMarksUpdate(ctx, interval)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}

	var answer []User
	for _, u := range usersRaw {
		answer = append(answer, NewUserFromRepositoryUser(u))
	}
	return answer, nil
}

// GetUsersThatHaveNoPersonData gets users that have no person data
func GetUsersThatHaveNoPersonData(ctx context.Context, userRepo *database.UserRepository) ([]User, error) {
	var span trace.Span
	ctx, span = otel.Tracer(packageName).Start(ctx, "GetUsersThatHaveNoPersonData")
	defer span.End()
	usersRaw, err := userRepo.GetUsersThatHaveNoPersonData(ctx)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}

	var answer []User
	for _, u := range usersRaw {
		answer = append(answer, NewUserFromRepositoryUser(u))
	}
	return answer, nil
}

// GetDefaultState gets user's default state
func (u User) GetDefaultState() int {
	if u.Login != "" && u.Password != "" {
		return StateUserLoggedIn
	}
	return StateEmptyUser
}

// GetUserAPI gets user api with database SessionID or by creating a new one
func (u User) GetUserAPI(ctx context.Context, userRepo *database.UserRepository, baseURL string, retries int, delay time.Duration, logger *zap.Logger, forceCredentials bool) (api.API, error) {
	var span trace.Span
	ctx, span = otel.Tracer("users").Start(ctx, "RetryGetUserApi")
	defer span.End()
	span.SetAttributes(attribute.Int64("user.id", u.ChatID))
	span.SetAttributes(
		attribute.String("user.login", u.Login),
		attribute.String("user.sessionid.update_time", u.SessionIDUpdateTime.String()),
	)

	logger.Info("Got user from db")
	var userAPI api.API
	if u.SessionIDUpdateTime.Add(4*time.Hour).After(time.Now()) && u.SessionID != "" && !forceCredentials { // SessionID was created less than 8 hours ago
		logger.Info("Creating user from session id")
		userAPI = api.CreateFromSessionID(u.SessionID, baseURL)
		return userAPI, nil
	}
	logger.Info("Creating user from credentials")
	for r := 0; ; r++ {
		userAPI, err := api.CreateFromCredentials(ctx, u.Login, u.Password, baseURL)
		if err == nil {
			logger.Info("Success", zap.Int("attempt", r+1))
			err := userRepo.UpdateSessionID(ctx, u.ChatID, userAPI.GetSessionID())
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				logger.Error("could not update session id")
			}
			return userAPI, nil
		}
		logger.Warn("Failed", zap.Int("attempt", r+1), zap.Error(err))
		if r >= retries {
			logger.Info("goodbye cruel world", zap.Int("attempt", r+1))
			return userAPI, err
		}
		time.Sleep(delay)
	}
}

// GetMarks gets user marks from database or BARS API
func (u *User) GetMarks(ctx context.Context, repo *database.UserRepository, baseURL string, retries int, delay time.Duration, logger *zap.Logger) (api.SummaryMarks, error) {
	var span trace.Span
	ctx, span = otel.Tracer("users").Start(ctx, "GetUserMarks")
	defer span.End()
	if len(u.Marks.DisciplineMarks) == 0 {
		API, err := u.GetUserAPI(ctx, repo, baseURL, retries, delay, logger, false)
		if err != nil {
			return api.SummaryMarks{}, err
		}
		marks, err := api.RetryGetSummaryMarks(ctx, API, time.Now(), retries, delay, logger)
		if err != nil {
			return api.SummaryMarks{}, err
		}
		u.Marks = marks
		_ = repo.UpdateUserMarks(ctx, u.ChatID, u.Marks)
		return u.Marks, nil
	}
	return u.Marks, nil
}

// GetUserSetting gets user i-th setting
// You should use it with users.Settings consts
func (u User) GetUserSetting(id int) bool {
	return GetMaskBit(u.Settings, id)
}

func (u User) GetTraceAttributes() []attribute.KeyValue {
	var ans []attribute.KeyValue
	ans = append(ans, attribute.String("fullname", u.FullName))
	ans = append(ans, attribute.String("school", u.School))
	ans = append(ans, attribute.String("class", u.Class))
	ans = append(ans, attribute.Int64("chat.id", u.ChatID))
	return ans
}

func (u User) GetLoggerFields() []zap.Field {
	var ans []zap.Field
	ans = append(ans, zap.String("fullname", u.FullName))
	ans = append(ans, zap.String("school", u.School))
	ans = append(ans, zap.String("class", u.Class))
	ans = append(ans, zap.Int64("chat.id", u.ChatID))
	return ans
}
