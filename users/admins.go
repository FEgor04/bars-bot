package users

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/bars-bot/bars-bot/database"
)

// AdminKeyboard is a keyboard in /admin command
var AdminKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Ответить на репорт", CallbackAdminAnswerReport),
		tgbotapi.NewInlineKeyboardButtonData("Настроить интервал", CallbackAdminSetInterval),
	),
)

// ReturnToAdminKeyboard is a keyboard with the only return button
var ReturnToAdminKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Назад", CallbackAdminReturnToAdminKeyboard),
	),
)

// SetIntervalKeyboard is a keyboard in /admin -> Set interval
var SetIntervalKeyboard = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("1 минута", CallbackAdminChoseInterval+"1"),
		tgbotapi.NewInlineKeyboardButtonData("5 минут", CallbackAdminChoseInterval+"5"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("10 минут", CallbackAdminChoseInterval+"10"),
		tgbotapi.NewInlineKeyboardButtonData("15 минут", CallbackAdminChoseInterval+"15"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Назад", CallbackAdminReturnToAdminKeyboard),
	),
)

// GetReportsKeyboard gets a keyboard with all reports
func GetReportsKeyboard(reports []database.Report) tgbotapi.InlineKeyboardMarkup {
	rowLen := 4
	var buttons []tgbotapi.InlineKeyboardButton
	for i := range reports {
		buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData(fmt.Sprintf("%d", reports[i].ID), fmt.Sprintf("%s%d", CallbackAdminAnswerReportPrefix, reports[i].ID)))
	}

	rows := GenerateInlineRows(buttons, rowLen)

	rows = append(rows, tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("Назад", CallbackAdminReturnToAdminKeyboard)))

	keyboard := tgbotapi.InlineKeyboardMarkup{
		InlineKeyboard: rows,
	}
	return keyboard
}
