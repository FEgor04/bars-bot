package users

import (
  tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
  "gitlab.com/bars-bot/bars-bot/api"
  "math"
  "strconv"
)

const (
  // CallbackUserSettingsPrefix is a prefix for /settings
  CallbackUserSettingsPrefix = ","
  // CallbackUserDisciplinesPrefix is a prefix for /left
  CallbackUserDisciplinesPrefix = ";"
  // CallbackAdminAnswerReportPrefix is a prefix in /admin -> answer report
  CallbackAdminAnswerReportPrefix = ":"
  // CallbackAdminChoseInterval is a prefix for set interval
  CallbackAdminChoseInterval = "&"
  // CallbackHomeworkSetDate data for HomeworkSetDate
  CallbackHomeworkSetDate = "homeworkSetDate"
  // CallbackHomeworkSetSubject data for HomeworkSetSubject
  CallbackHomeworkSetSubject = "homeworkSetSubject"
  // CallbackHomeworkSetText data for HomeworkS
  CallbackHomeworkSetText = "homeworkSetText"
  // CallbackHomeworkAddFile data for HomeworkAddFile
  CallbackHomeworkAddFile = "homeworkAddPhoto"
  // CallbackHomeworkConfirm data for HomeworkConfirm
  CallbackHomeworkConfirm = "homeworkConfirm"
  // CallbackHomeworkReturnToMenu data for HomeworkReturnToMenu
  CallbackHomeworkReturnToMenu = "homeworkReturnToMenu"
  // CallbackAdminAnswerReport data for /admin -> answer reports
  CallbackAdminAnswerReport = "getReports"
  // CallbackAdminSetInterval data for /admin -> set interval
  CallbackAdminSetInterval = "setInterval"
  // CallbackAdminReturnToAdminKeyboard data for /admin -> ... -> return to menu
  CallbackAdminReturnToAdminKeyboard = "returnToAdminKeyboard"
  // CallbackAllMarksPrefix prefix for /allmarks
  CallbackAllMarksPrefix = "*"
)

// GenerateInlineRows generate rows for inline keyboard
func GenerateInlineRows(buttons []tgbotapi.InlineKeyboardButton, rowLen int) [][]tgbotapi.InlineKeyboardButton {
  n := len(buttons)
  var rows [][]tgbotapi.InlineKeyboardButton
  lastI := 0
  for i := 0; i < n/rowLen; i++ {
    var rowNow []tgbotapi.InlineKeyboardButton
    for j := 0; j < rowLen; j++ {
      rowNow = append(rowNow, buttons[rowLen*i+j])
    }
    rows = append(rows, rowNow)
    lastI = rowLen*i + rowLen
  }
  var kek []tgbotapi.InlineKeyboardButton
  for i := lastI; i < n; i++ {
    kek = append(kek, buttons[i])
  }
  if len(kek) > 0 {
    rows = append(rows, kek)
  }
  return rows
}

// GenerateSettingsKeyboard generate /settings keyboard
func GenerateSettingsKeyboard() tgbotapi.InlineKeyboardMarkup {
  var buttons []tgbotapi.InlineKeyboardButton

  for i := range SettingsNames {
    buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData(SettingsNames[i], CallbackUserSettingsPrefix+strconv.Itoa(i)))
  }
  return tgbotapi.InlineKeyboardMarkup{InlineKeyboard: GenerateInlineRows(buttons, int(math.Floor(math.Sqrt(float64(len(buttons))))))}
}

// GenerateDisciplinesKeyboard generate keyboard for /left command
func GenerateDisciplinesKeyboard(disciplines api.SummaryMarks, selectedDisciplineID int, prefix string) tgbotapi.InlineKeyboardMarkup {
  disciplines.Sort()

  var buttons []tgbotapi.InlineKeyboardButton
  for i := range disciplines.DisciplineMarks {
    if i != selectedDisciplineID {
      buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData(disciplines.DisciplineMarks[i].ShortenDisciplineName(), prefix+strconv.Itoa(i)))
    } else {
      buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("▶"+disciplines.DisciplineMarks[i].ShortenDisciplineName()+"◀", prefix+strconv.Itoa(i)))
    }
  }

  return tgbotapi.InlineKeyboardMarkup{InlineKeyboard: GenerateInlineRows(buttons, 2)}
}

// GenerateAddHomeworkKeyboard generate keyboard for /addhw
func GenerateAddHomeworkKeyboard(addConfirmButton bool) tgbotapi.InlineKeyboardMarkup {
  var buttons []tgbotapi.InlineKeyboardButton
  buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("Указать дату", CallbackHomeworkSetDate))
  buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("Указать предмет", CallbackHomeworkSetSubject))
  buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("Указать текст", CallbackHomeworkSetText))
  buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("Добавить фото / документ", CallbackHomeworkAddFile))
  if addConfirmButton {
    buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonData("Подтвердить", CallbackHomeworkConfirm))
  }
  return tgbotapi.InlineKeyboardMarkup{InlineKeyboard: GenerateInlineRows(buttons, 2)}
}
