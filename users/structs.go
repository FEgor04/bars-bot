package users

import (
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/database"
	"time"
)

const packageName = "users"

// User is a class to store user data
type User struct {
	ChatID              int64
	Login               string
	Password            string
	SessionID           string
	Settings            int
	SessionIDUpdateTime time.Time
	Marks               api.SummaryMarks
	MarksUpdateTime     time.Time
	School              string
	Class               string
	FullName            string
}

// NewUserFromRepositoryUser creates user from database.RepositoryUser
func NewUserFromRepositoryUser(repositoryUser database.RepositoryUser) User {
	return User{
		ChatID:              repositoryUser.ChatID,
		Login:               repositoryUser.Login,
		Password:            repositoryUser.Password,
		SessionID:           repositoryUser.SessionID,
		Settings:            repositoryUser.Settings,
		SessionIDUpdateTime: repositoryUser.SessionIDUpdateTime,
		Marks:               repositoryUser.Marks,
		MarksUpdateTime:     repositoryUser.MarksUpdateTime,
		School:              repositoryUser.School,
		Class:               repositoryUser.Class,
		FullName:            repositoryUser.FullName,
	}
}

// RepositoryUser creates database.RepositoryUser
func (u User) RepositoryUser() database.RepositoryUser {
	return database.RepositoryUser{
		ChatID:              u.ChatID,
		Login:               u.Login,
		Password:            u.Password,
		SessionID:           u.SessionID,
		Settings:            u.Settings,
		SessionIDUpdateTime: u.SessionIDUpdateTime,
		Marks:               u.Marks,
		MarksUpdateTime:     u.MarksUpdateTime,
		School:              u.School,
		Class:               u.Class,
		FullName:            u.FullName,
	}
}
