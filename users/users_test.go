//go:build unit || all
// +build unit all

package users

import (
	"context"
	"fmt"
	"github.com/jarcoal/httpmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	sqlmock "github.com/zhashkevych/go-sqlxmock"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/database"
	"go.uber.org/zap"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

var ctx = context.Background()

func randStr(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	answ := make([]rune, n)
	for i := 0; i < n; i++ {
		answ[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(answ)
}

func generateUser() User {
	return User{
		ChatID:              rand.Int63(),
		Login:               randStr(10),
		Password:            randStr(10),
		SessionID:           randStr(10),
		Settings:            rand.Int(),
		SessionIDUpdateTime: time.UnixMicro(rand.Int63()),
		Marks:               api.SummaryMarks{},
		MarksUpdateTime:     time.Time{},
		School:              randStr(15),
		Class:               randStr(15),
		FullName:            randStr(15),
	}
}

func getDatabase() (*database.UserRepository, *sqlx.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	if err != nil {
		panic(err)
	}
	return database.New(db).NewUserRepository(), db, mock
}

func TestGetUserByChatID_NoError(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()
	user := generateUser()
	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time", "school", "class", "fullname"}
	mock.ExpectQuery("SELECT * FROM users WHERE chatid = $1").
		WithArgs(user.ChatID).
		WillReturnRows(
			sqlmock.NewRows(columns).
				AddRow(
					user.ChatID,
					user.Login,
					user.Password,
					user.SessionID,
					user.Settings,
					user.SessionIDUpdateTime,
					user.Marks,
					user.MarksUpdateTime,
					user.School,
					user.Class,
					user.FullName,
				),
		)

	actual, err := GetUserByChatID(ctx, repo, user.ChatID)
	assert.NoError(t, err)
	assert.Equal(t, user, actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}
func TestGetUserByChatID_Error(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()
	user := User{
		ChatID:              1234,
		Login:               "Test Login",
		Password:            "Test Password",
		SessionID:           "Test session Id",
		Settings:            57,
		SessionIDUpdateTime: time.Now(),
		Marks:               api.SummaryMarks{},
		MarksUpdateTime:     time.Now(),
		School:              "Good school",
		Class:               "Bad class",
		FullName:            "Ugly Name",
	}
	returnedError := fmt.Errorf("hi there user %d", user.ChatID)
	mock.ExpectQuery("SELECT * FROM users WHERE chatid = $1").
		WithArgs(user.ChatID).
		WillReturnError(returnedError)

	actual, err := GetUserByChatID(ctx, repo, user.ChatID)
	assert.ErrorIs(t, returnedError, err)
	assert.NotEqual(t, user, actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestGetUsersThatNeedMarksUpdate_NoError(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()

	interval := 5
	usersCnt := 5

	users := make([]User, usersCnt)
	for i := 0; i < usersCnt; i++ {
		users[i] = generateUser()
	}

	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time", "school", "class", "fullname"}
	rows := sqlmock.NewRows(columns)

	for i := 0; i < usersCnt; i++ {
		rows.AddRow(
			users[i].ChatID,
			users[i].Login,
			users[i].Password,
			users[i].SessionID,
			users[i].Settings,
			users[i].SessionIDUpdateTime,
			users[i].Marks,
			users[i].MarksUpdateTime,
			users[i].School,
			users[i].Class,
			users[i].FullName,
		)
	}
	mock.ExpectQuery("SELECT * FROM users where marks_update_time < now() - make_interval(mins => $1) + make_interval(secs => 10) or marks_update_time is null or marks is null;").WithArgs(interval).WillReturnRows(rows)

	actual, err := GetUsersThatNeedMarksUpdate(ctx, repo, interval)
	assert.NoError(t, err)
	assert.Equal(t, users, actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}
func TestGetUsersThatNeedMarksUpdate_Error(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()

	interval := 5

	returnedError := fmt.Errorf("Test")
	mock.ExpectQuery("SELECT * FROM users where marks_update_time < now() - make_interval(mins => $1) + make_interval(secs => 10) or marks_update_time is null or marks is null;").WithArgs(interval).WillReturnError(returnedError)

	actual, err := GetUsersThatNeedMarksUpdate(ctx, repo, interval)
	assert.ErrorIs(t, returnedError, err)
	assert.Equal(t, []User(nil), actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestGetUsersThatHaveNoPersonData_NoError(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()

	usersCnt := 5

	users := make([]User, usersCnt)
	for i := 0; i < usersCnt; i++ {
		users[i] = generateUser()
	}

	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time", "school", "class", "fullname"}
	rows := sqlmock.NewRows(columns)

	for i := 0; i < usersCnt; i++ {
		rows.AddRow(
			users[i].ChatID,
			users[i].Login,
			users[i].Password,
			users[i].SessionID,
			users[i].Settings,
			users[i].SessionIDUpdateTime,
			users[i].Marks,
			users[i].MarksUpdateTime,
			users[i].School,
			users[i].Class,
			users[i].FullName,
		)
	}
	mock.ExpectQuery("SELECT * FROM users WHERE (school = '') IS NOT FALSE OR (class = '') IS NOT FALSE OR (fullname = '') IS NOT FALSE").WillReturnRows(rows)

	actual, err := GetUsersThatHaveNoPersonData(ctx, repo)
	assert.NoError(t, err)
	assert.Equal(t, users, actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}
func TestGetUsersThatHaveNoPersonData_Error(t *testing.T) {
	repo, db, mock := getDatabase()
	defer db.Close()

	returnedError := fmt.Errorf("Test")
	mock.ExpectQuery("SELECT * FROM users WHERE (school = '') IS NOT FALSE OR (class = '') IS NOT FALSE OR (fullname = '') IS NOT FALSE").WillReturnError(returnedError)

	actual, err := GetUsersThatHaveNoPersonData(ctx, repo)
	assert.ErrorIs(t, returnedError, err)
	assert.Equal(t, []User(nil), actual)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestGetUserAPI_NoUpdate(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	repo, db, _ := getDatabase()
	defer db.Close()

	baseUrl := "https://test.com"
	user := generateUser()
	user.SessionIDUpdateTime = time.Now()

	httpmock.RegisterResponder("POST", baseUrl+"/auth/login", func(request *http.Request) (*http.Response, error) {
		encoded := []byte(`{"success": true, "redirect": "/"}`)
		response := httpmock.NewBytesResponse(http.StatusOK, encoded)
		response.Header.Set("Set-Cookie", "sessionid="+user.SessionID)
		return response, nil
	})
	httpmock.RegisterResponder("GET", baseUrl+"/personal-area#diary", httpmock.NewStringResponder(http.StatusOK, "good"))

	API, err := user.GetUserAPI(ctx, repo, baseUrl, 3, time.Second, zap.NewNop(), true)
	assert.NoError(t, err)
	assert.Equal(t, user.SessionID, API.GetSessionID())
}

func TestGetUserAPI_Update(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	repo, db, mock := getDatabase()
	defer db.Close()

	baseUrl := "https://test.com"
	user := generateUser()
	user.SessionIDUpdateTime = time.Date(time.Now().Year()-1, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.Local)
	httpmock.RegisterResponder("POST", baseUrl+"/auth/login", func(request *http.Request) (*http.Response, error) {
		encoded := []byte(`{"success": true, "redirect": "/"}`)
		response := httpmock.NewBytesResponse(http.StatusOK, encoded)
		response.Header.Set("Set-Cookie", "sessionid="+user.SessionID)
		return response, nil
	})
	httpmock.RegisterResponder("GET", baseUrl+"/personal-area#diary", httpmock.NewStringResponder(http.StatusOK, "good"))
	mock.ExpectExec("UPDATE users SET sessionid = $1, sessionid_update_time = now() WHERE chatid = $2").
		WithArgs(user.SessionID, user.ChatID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	API, err := user.GetUserAPI(ctx, repo, baseUrl, 3, time.Nanosecond, zap.NewNop(), true)
	assert.NoError(t, err)
	assert.Equal(t, user.SessionID, API.GetSessionID())
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestGetUserAPI_UpdateError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	repo, db, mock := getDatabase()
	defer db.Close()

	baseUrl := "https://test.com"
	user := generateUser()
	user.SessionIDUpdateTime = time.Date(time.Now().Year()-1, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.Local)
	httpmock.RegisterResponder("POST", baseUrl+"/auth/login", httpmock.NewStringResponder(http.StatusUnauthorized, "Sorry =("))
	httpmock.RegisterResponder("GET", baseUrl+"/personal-area#diary", httpmock.NewStringResponder(http.StatusOK, "good"))

	API, err := user.GetUserAPI(ctx, repo, baseUrl, 3, time.Nanosecond, zap.NewNop(), true)
	assert.ErrorIs(t, api.ErrWrongPassword, err)
	assert.Equal(t, "", API.GetSessionID())
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestUser_GetMarks_MarksFromUser(t *testing.T) {
	user := generateUser()
	user.Marks = api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates: []string{"test test"},
		DisciplineMarks: []api.DisciplineMark{{
			Discipline: "Test Discipline",
			Marks: []api.Mark{{
				Date:        "Test",
				Description: "test",
				Mark:        "5",
			}},
			AverageMark: "5.00",
		}},
	}

	actual, err := user.GetMarks(ctx, &database.UserRepository{}, "test.com", 3, time.Second, zap.NewNop())
	assert.NoError(t, err)
	assert.Equal(t, user.Marks, actual)
}

func TestUser_GetMarks_MarksFromAPI(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	repo, db, mock := getDatabase()
	defer db.Close()

	user := generateUser()
	wantedMarks := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates: []string{"test test"},
		DisciplineMarks: []api.DisciplineMark{{
			Discipline: "Test Discipline",
			Marks: []api.Mark{{
				Date:        "Test",
				Description: "test",
				Mark:        "5",
			}},
			AverageMark: "5.00",
		}},
	}

	baseURL := "https://test.com"
	httpmock.RegisterResponder("GET", baseURL+"/api/MarkService/GetSummaryMarks", httpmock.NewJsonResponderOrPanic(http.StatusOK, wantedMarks))
	mock.ExpectExec("UPDATE users SET marks = $1, marks_update_time = now() WHERE chatid = $2").
		WithArgs(wantedMarks, user.ChatID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	actual, err := user.GetMarks(ctx, repo, baseURL, 3, time.Second, zap.NewNop())
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, wantedMarks, actual)
	assert.Equal(t, wantedMarks, user.Marks)
}
