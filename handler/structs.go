package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/bars-bot/bars-bot/cache"
	"gitlab.com/bars-bot/bars-bot/config"
	"gitlab.com/bars-bot/bars-bot/database"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Bot is a handler bot class
type Bot struct {
	bot            *tgbotapi.BotAPI
	botConfig      config.BotConfig
	databaseConfig config.DatabaseConfig
	redisConfig    config.RedisConfig
	loggerCore     zapcore.Core
	cache          cache.RedisCache
	db             *database.Database
}

// ZapBotLogger is a wrapper for zap logger
type ZapBotLogger struct {
	logger *zap.SugaredLogger
}

// NewBotLogger creates new ZapBotLogger
func NewBotLogger(logger *zap.SugaredLogger) ZapBotLogger {
	return ZapBotLogger{logger: logger}
}

// Printf is a printf method for logger
func (l ZapBotLogger) Printf(format string, v ...interface{}) {
	l.logger.Infof(format, v)
}

// Println is a println method for logger
func (l ZapBotLogger) Println(v ...interface{}) {
	l.logger.Info(v)
}
