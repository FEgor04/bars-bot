package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/users"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

const baseURL = "https://school.karelia.ru"
const eventCommand = "command"
const eventCallback = "callback"
const eventText = "text"
const eventStrange = "strange"
const tracerName = "handler"

// SendMessageToEveryone sends message to every user
func (b *Bot) SendMessageToEveryone(ctx context.Context, msg tgbotapi.MessageConfig, logger *zap.Logger) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "SendMessageToEveryone")
	defer span.End()

	chatIds, err := (b.db.NewUserRepository()).GetAllUsersChatID(ctx)
	if err != nil {
		logger.Error("Could not get all users", zap.Error(err))
	}
	for _, u := range chatIds {
		msg.ChatID = u
		b.SendMessage(ctx, msg, logger)
	}
}

// UpdateHistogram updates timeToHandle histogram
func UpdateHistogram(event string, start time.Time) {
	timeToHandleUserHistogram.WithLabelValues(event).Observe(float64(time.Since(start).Milliseconds()))
}

// HandleUpdate handles incoming update
func (b *Bot) HandleUpdate(update tgbotapi.Update) {
	timeStart := time.Now()
	ctx := context.Background()
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "HandleUpdate")
	defer span.End()

	var userState int

	logger := zap.New(b.loggerCore)

	var messageAuthor int64
	var messageChat *tgbotapi.User
	if update.Message != nil {
		messageAuthor = update.Message.Chat.ID
		messageChat = update.Message.From
	} else if update.CallbackQuery != nil {
		messageAuthor = update.CallbackQuery.From.ID
		messageChat = update.CallbackQuery.From
	} else {
		logger.Info("Strange message! Message is nullptr")
		UpdateHistogram(eventStrange, timeStart)
		return
	}

	user, err := users.GetUserByChatID(ctx, b.db.NewUserRepository(), messageAuthor)
	if err != nil {
		logger.Error("could not get user by chatid", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		msg := tgbotapi.NewMessage(messageAuthor, "Не удалось получить данные из базы даных. Попробуйте позже")
		b.SendMessage(ctx, msg, logger)
		return
	}

	if user.ChatID != 0 {
		logger = logger.With(user.GetLoggerFields()...)
		span.SetAttributes(user.GetTraceAttributes()...)
	} else {
		logger = logger.With(zap.Int64("chat.id", messageChat.ID))
	}

	logger = logger.With(
		zap.String("chat.username", messageChat.UserName),
		zap.String("chat.firstname", messageChat.FirstName),
		zap.String("chat.lastname", messageChat.LastName),
	)

	userState = b.cache.GetUserState(ctx, messageAuthor)
	if userState == -1 {
		logger.Info("User has no state. Getting the default")
		userState = user.GetDefaultState()

		err = b.cache.SetUserState(ctx, messageAuthor, userState)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not set user state", zap.Error(err))
		}
		logger.Info("set user default state", zap.Int("state", userState))
	}

	span.SetAttributes(attribute.Int("user.state", userState))

	if update.Message != nil && userState != users.StatePasswordWanted {
		span.SetAttributes(attribute.String("msg.text", update.Message.Text))
	}

	if update.CallbackQuery != nil {
		logger = logger.With(GetLoggerFields(update.CallbackQuery.Message.Chat)...)
		b.HandleCallback(ctx, update, user, logger)
		UpdateHistogram(eventCallback, timeStart)
		return
	}
	if update.Message != nil {
		logger = logger.With(GetLoggerFields(update.Message.Chat)...)
		if update.Message.IsCommand() {
			if userState >= users.StateUserLoggedIn || update.Message.Command() == "start" || update.Message.Command() == "cancel" || update.Message.Command() == "help" {
				b.HandleCommand(ctx, update.Message, user, logger.With(zap.String("event", update.Message.Command())))
			} else if userState == users.StateEmptyUser {
				m := tgbotapi.NewMessage(update.Message.Chat.ID, "Для начала используйте команду /start")
				m.ReplyToMessageID = update.Message.MessageID
				b.SendMessage(ctx, m, logger)
			} else if userState == users.StateLoginWanted {
				m := tgbotapi.NewMessage(update.Message.Chat.ID, "Для начала работы введите ваш логин")
				m.ReplyToMessageID = update.Message.MessageID
				b.SendMessage(ctx, m, logger)
			} else if userState == users.StatePasswordWanted {
				m := tgbotapi.NewMessage(update.Message.Chat.ID, "Для начала работы введите ваш пароль")
				m.ReplyToMessageID = update.Message.MessageID
				b.SendMessage(ctx, m, logger)
			} else {
				b.HandleCommand(ctx, update.Message, user, logger.With(zap.String("event", update.Message.Command())))
			}
			UpdateHistogram(eventCommand, timeStart)
		} else {
			b.HandleText(ctx, update.Message, userState, logger)
			UpdateHistogram(eventText, timeStart)
		}
	}
}

// GetUserMarks gets user marks from database if they exist or from BARS
func (b *Bot) GetUserMarks(ctx context.Context, chatID int64, logger *zap.Logger) (api.SummaryMarks, error) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "GetUserMarks")
	defer span.End()

	logger.Debug("Gettings marks from database")

	user, err := users.GetUserByChatID(ctx, b.db.NewUserRepository(), chatID)
	if err != nil {
		logger.Error("could not get data from database", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return api.SummaryMarks{}, err
	}
	span.SetAttributes(
		attribute.String("user.name", user.FullName),
		attribute.String("user.school", user.School),
		attribute.String("user.class", user.Class),
	)

	marks := user.Marks
	if marks.DisciplineMarks == nil || err != nil {
		userAPI, err := user.GetUserAPI(ctx, (b.db).NewUserRepository(), baseURL, 5, time.Second, logger, false)
		logger.Debug("Got user api")
		if err != nil {
			return api.SummaryMarks{}, err
		}
		logger.Debug("Getting user marks")
		marks, err = userAPI.GetPersonMarks(ctx, time.Now())
		if err != nil {
			logger.Error("could not get marks from BARS", zap.Error(err))
			return api.SummaryMarks{}, err
		}

		logger.Info("updating database record")
		err = (b.db.NewUserRepository()).UpdateUserMarks(ctx, chatID, marks) // TODO Заменить на метод класса User
		if err != nil {
			logger.Error("could not update database record", zap.Error(err))
		}
		logger.Debug("Got user marks", zap.Int("marks_len", len(marks.DisciplineMarks)), zap.String("source", "bars"))
	} else {
		logger.Debug("Got user marks", zap.Int("marks_len", len(marks.DisciplineMarks)), zap.String("source", "database"))
	}
	return marks, nil
}

// SendScheduleMessage sends schedule message
func (b Bot) SendScheduleMessage(ctx context.Context, chatID int64, messageID int, targetUser users.User, schedule api.ScheduleWithHomework, date time.Time, logger *zap.Logger) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "SendScheduleMessage", trace.WithAttributes(targetUser.GetTraceAttributes()...))
	defer span.End()

	msg := tgbotapi.NewMessage(chatID, "Расписание на "+date.Format("02-01")+"\n")
	msg.ReplyToMessageID = messageID
	var wantedDay = schedule.GetDay(date)

	if wantedDay.Date == "" {
		logger.Error("lol wat")
		msg := UseReportCommand(chatID)
		msg.ReplyToMessageID = messageID
		b.SendMessage(ctx, msg, logger)
		return
	}

	for i, subj := range wantedDay.Homeworks {
		msg.Text += fmt.Sprintf("%d. %s - %s", i+1, subj.ShortenDisciplineName(), subj.GetHomework())
		if targetUser.GetUserSetting(users.SettingsScheduleShowDescription) && targetUser.GetUserSetting(users.SettingsScheduleShowTeacher) {
			msg.Text += " (тема: " + subj.GetTheme() + ", учитель: " + subj.Teacher + ")"
		} else if targetUser.GetUserSetting(users.SettingsScheduleShowDescription) {
			msg.Text += " (тема: " + subj.GetTheme() + ")"
		} else if targetUser.GetUserSetting(users.SettingsScheduleShowTeacher) {
			msg.Text += " (учитель: " + subj.Teacher + ")"
		}
		msg.Text += "\n"
	}

	var buttons []tgbotapi.InlineKeyboardButton
	for _, subj := range wantedDay.Homeworks {
		for _, file := range subj.Materials {
			fileURL := baseURL + file.URL
			buttons = append(buttons, tgbotapi.NewInlineKeyboardButtonURL(subj.ShortenDisciplineName()+" - "+file.Name, fileURL))
		}
	}
	if len(buttons) > 0 {
		msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(users.GenerateInlineRows(buttons, int(math.Sqrt(float64(len(buttons)))))...)
	}
	b.SendMessage(ctx, msg, logger)

	school := targetUser.School
	class := targetUser.Class

	b.SendScheduleUserHomeworks(ctx, chatID, date, school, class, nil)
}

// Request is a wrapper for tgbotapi.Request method
func (b *Bot) Request(ctx context.Context, c tgbotapi.Chattable) (*tgbotapi.APIResponse, error) {
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "Request")
	defer span.End()

	switch c := c.(type) {
	case tgbotapi.EditMessageTextConfig:
		span.SetName("RequestEditMessageText")
		span.SetAttributes(
			attribute.String("text", c.Text),
			attribute.Int64("chatid", c.ChatID),
		)
	case tgbotapi.WebhookConfig:
		span.SetName("RequestSetWebhook")
	case tgbotapi.DeleteWebhookConfig:
		span.SetName("RequestDeleteWebhook")
	case tgbotapi.SetMyCommandsConfig:
		span.SetName("RequestSetCommands")
	}

	response, err := b.bot.Request(c)
	return response, err
}

func (b *Bot) SendMediaGroup(ctx context.Context, config tgbotapi.MediaGroupConfig, logger *zap.Logger) {
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(
		ctx,
		"SendMediaGroup",
		trace.WithAttributes(attribute.Int("media.len", len(config.Media))),
	)
	defer span.End()
	_, err := b.bot.SendMediaGroup(config)
	if err != nil {
		logger.Error("could not send media group", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
}

// SendMessage is a wrapper for a tgbotapi.Send method
func (b *Bot) SendMessage(ctx context.Context, c tgbotapi.Chattable, logger *zap.Logger) {
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "SendMessage")
	defer span.End()
	switch c := c.(type) {
	case tgbotapi.MessageConfig:
		span.SetAttributes(
			attribute.Int64("chat.id", c.ChatID),
			attribute.String("msg.text", c.Text),
		)
	case tgbotapi.DocumentConfig:
		span.SetName("SendDocument")
		span.SetAttributes(
			attribute.Int64("chat.id", c.ChatID),
			attribute.String("msg.caption", c.Caption),
		)
	case tgbotapi.PhotoConfig:
		span.SetName("SendPhoto")
		span.SetAttributes(
			attribute.Int64("chat.id", c.ChatID),
			attribute.String("msg.caption", c.Caption),
		)
	}

	if _, err := b.bot.Send(c); err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		if logger != nil {
			logger.Error("could not send message", zap.Error(err))
		}
	}
}

// SetWebhook sets bot webhook
func (b *Bot) SetWebhook(ctx context.Context, config tgbotapi.WebhookConfig, errIn error) (bool, error) {
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "SetWebhook")
	defer span.End()

	if errIn != nil {
		span.RecordError(errIn)
		return false, errIn
	}
	allowedUpdates := ""
	for i := range config.AllowedUpdates {
		allowedUpdates += config.AllowedUpdates[i]
		if i != len(config.AllowedUpdates)-1 {
			allowedUpdates += ","
		}
	}
	//dropConfigUpdates := "false"
	//if config.DropPendingUpdates {
	// dropConfigUpdates = "true"
	//}
	response, err := b.bot.MakeRequest("setWebhook", map[string]string{
		"url": config.URL.String(),
		//"ip_address": config.IPAddress,
		//"certificate": config.Certificate.SendData(),
		//"max_connections": strconv.Itoa(config.MaxConnections),
		//"allowed_updates": allowedUpdates,
		//"drop_pending_updates": dropConfigUpdates,
	})
	if err != nil {
		span.RecordError(errIn)
		return false, errIn
	}

	var success bool
	err = json.Unmarshal(response.Result, &success)
	if err != nil {
		span.RecordError(errIn)
	}
	return success, err
}

// RemoveWebhook removes bot webhook
func (b *Bot) RemoveWebhook(ctx context.Context) (bool, error) {
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "RemoveWebhook")
	defer span.End()
	response, err := b.bot.MakeRequest("deleteWebhook", nil)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return false, err
	}
	var success bool
	err = json.Unmarshal(response.Result, &success)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return false, err
	}
	return true, nil
}

// SendMessageToAdmins sends message to admins only
func (b Bot) SendMessageToAdmins(ctx context.Context, msg tgbotapi.MessageConfig, logger *zap.Logger) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "SendMessageToAdmins")
	defer span.End()

	admins, err := (b.db.NewUserRepository()).GetAdmins(ctx)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not get admins fro repo", zap.Error(err))
		return
	}
	for _, adm := range admins {
		msg.ChatID = adm
		b.SendMessage(ctx, msg, logger)
	}
}

// GetBiggestPhoto returns biggest photo from given array
func GetBiggestPhoto(photos []tgbotapi.PhotoSize) tgbotapi.PhotoSize {
	sort.Slice(photos, func(i, j int) bool {
		return photos[i].Width*photos[i].Height > photos[j].Width*photos[j].Height
	})

	return photos[0]
}

func GetLoggerFields(m *tgbotapi.Chat) []zap.Field {
	return []zap.Field{
		zap.String("firstname", m.FirstName),
		zap.String("lastname", m.LastName),
		zap.String("username", m.UserName),
	}
}
