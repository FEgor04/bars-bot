package main

import (
  "context"
  "errors"
  "fmt"
  "go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
  "net/http"
  "sort"
  "strconv"
  "strings"
  "time"

  tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
  "gitlab.com/bars-bot/bars-bot/api"
  "gitlab.com/bars-bot/bars-bot/database"
  "gitlab.com/bars-bot/bars-bot/emoji"
  "gitlab.com/bars-bot/bars-bot/users"
  "go.opentelemetry.io/otel"
  "go.opentelemetry.io/otel/attribute"
  "go.opentelemetry.io/otel/trace"
  "go.uber.org/zap"
)

// HandleCommand handles incoming user command
func (b *Bot) HandleCommand(ctx context.Context, msg *tgbotapi.Message, user users.User, logger *zap.Logger) {
  switch msg.Command() {
  case "start":
    b.HandleStart(ctx, msg, user.GetDefaultState(), logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/start")))
  case "marks":
    b.HandleMarks(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/marks")))
  case "help":
    b.HandleHelp(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/help")))
  case "cancel":
    b.HandleCancel(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/cancel")))
  case "left":
    b.HandleLeft(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/left")))
  case "settings":
    b.HandleSettings(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/settings")))
  case "report":
    b.HandleReport(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/report")))
  case "logout":
    b.HandleLogout(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/logout")))
  case "schedule":
    b.HandleSchedule(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/schedule")))
  case "admin":
    b.HandleAdmin(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/admin")))
  case "addhw":
    b.HandleAddHomework(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/addhw")))
  case "addnews":
    b.HandleAddNews(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/addnews")))
  case "allmarks":
    b.HandleAllMarks(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/allmarks")))
  case "wreport":
    b.HandleWeekReport(ctx, msg, user, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/allmarks")))
  default:
    b.HandleUnknownCommand(ctx, msg, logger.With(zap.String("user_msg", msg.Text), zap.String("event", "/"+msg.Command())))
  }
}

// HandleStart handles /start command
func (b *Bot) HandleStart(ctx context.Context, msg *tgbotapi.Message, userState int, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleStart")
  defer span.End()

  switch userState {
  case users.StateEmptyUser:
    m := tgbotapi.NewMessage(msg.Chat.ID,
      "Для начала работы введите ваш логин и пароль *разными сообщениями*\n"+
        "К сожалению, БАРС не имеет иных способов аутентификации.\n"+
        "Все логины и пароли хранятся в базе данных в *открытом виде*\n"+
        "Авторизация через госуслуги *невозможна*",
    )
    m.ParseMode = "markdown"
    b.SendMessage(ctx, m, logger)
    err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateLoginWanted)
    if err != nil {
      logger.Error("could not set user state", zap.Error(err))
    }
  case users.StateUserLoggedIn:
    m := tgbotapi.NewMessage(msg.Chat.ID, "Вы уже вошли. Используйте /help для получения информации о командах.")
    b.SendMessage(ctx, m, logger)
  }
}

// SendMarksMessage sends marks message. chatID - whom to send; messageID - message to reply
func (b *Bot) SendMarksMessage(ctx context.Context, chatID int64, messageID int, marks api.SummaryMarks, logger *zap.Logger) {
  var span trace.Span
  ctx, span = otel.Tracer(tracerName).Start(ctx, "SendMarksMessage")
  defer span.End()

  msgText := "Ваши оценки:\n"
  userSettings, err := (b.db.NewUserRepository()).GetUserSettings(ctx, chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    msgText += "Не удалось получить настройки из БД. Будут использованы стандартные.\n"
    userSettings = users.DefaultSettings
  }

  user := users.User{ChatID: chatID, Settings: userSettings}

  sort.Slice(marks.DisciplineMarks, func(i, j int) bool {
    return marks.DisciplineMarks[i].CalculateAverage() > marks.DisciplineMarks[j].CalculateAverage()
  })

  for _, subj := range marks.DisciplineMarks {
    msgText += emoji.React(subj.Round(), user.GetUserSetting(users.SettingsUseEmoji))
    //msgText += subj.ShortenDisciplineName() + " - " + subj.CalculateAverage() + "\n"
    msgText += fmt.Sprintf("%s - %s", subj.ShortenDisciplineName(), subj.AverageMark)
    if user.GetUserSetting(users.SettingsMarksShowGoodAverageMark) {
      msgText += fmt.Sprintf("\nСредний *арифметический* балл: %.2f\n", subj.CalculateAverage())
    }
    msgText += "\n"
  }
  m := tgbotapi.NewMessage(chatID, msgText)
  if messageID != -1 {
    m.ReplyToMessageID = messageID
  }
  m.ParseMode = tgbotapi.ModeMarkdown
  b.SendMessage(ctx, m, logger)
}

// HandleMarks handles /marks command
func (b *Bot) HandleMarks(ctx context.Context, msg *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleMarks")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  logger.Debug("getting user marks")

  marks, err := user.GetMarks(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger)
  if errors.Is(api.ErrInternal, err) {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get marks", zap.Error(err))
    msg := tgbotapi.NewMessage(msg.Chat.ID, "Не удалось получить оценки из-за внутренней ошибки БАРС")
    b.SendMessage(ctx, msg, logger)
    return
  } else if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get marks", zap.Error(err))
    msg := UseReportCommand(msg.Chat.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }
  logger.Debug("got user marks")
  b.SendMarksMessage(ctx, msg.Chat.ID, msg.MessageID, marks, logger)
  logger.Info("Handled user")
}

// HandleHelp handles /help command
func (b Bot) HandleHelp(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleHelp")
  defer span.End()

  logger.Info("Handling user")
  msg := tgbotapi.NewMessage(m.Chat.ID, "")
  msg.Text += "/start - Начало работы" + "\n"
  msg.Text += "/marks - Получить сводку об оценказ" + "\n"
  msg.Text += "/settings - Настройки" + "\n"
  msg.Text += "/report - Отправить сообщение" + "\n"
  msg.Text += "/allmarks- Посмотреть все оценки по выбранному предмету" + "\n"
  msg.Text += "/schedule [DD-MM] - Получить расписание на число DD-MM" + "\n"
  msg.Text += "/left - Сколько оценок можно/нужно получить для достижения определенного среднего *арифметического* балла. " +
    "В некоторых случаях БАРС считает свой балл иначе, будьте осторожны." + "\n"
  msg.Text += "/cancel - Отмена операции" + "\n" // HandleCancel
  msg.Text += "/addhw - Добавить домашнее задание" + "\n"
  msg.Text += emoji.CrossMark + "[НЕ РЕАЛИЗОВАНО] /logout - Выйти из системы" + "\n"
  msg.ParseMode = tgbotapi.ModeMarkdown
  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleLogout handles /logout command
func (b Bot) HandleLogout(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleLogout")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")

  err := b.db.NewUserRepository().DeleteUser(ctx, user.ChatID)
  if err != nil {
    span.RecordError(err)
    b.HandleError(ctx, user.ChatID, err, logger)
    return
  }
  err = b.cache.DeleteUser(ctx, user.ChatID)
  if err != nil {
    span.RecordError(err)
    b.HandleError(ctx, user.ChatID, err, logger)
    return
  }
  msg := tgbotapi.NewMessage(m.Chat.ID, "Вы успешно вышли"+emoji.CryingFace+emoji.CryingFace+emoji.CryingFace)
  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleLeft handles /left command
func (b Bot) HandleLeft(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleLeft")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  marks, err := user.GetMarks(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger)
  if errors.Is(api.ErrInternal, err) {
    msg := tgbotapi.NewMessage(m.Chat.ID, "Не удалось получить оценки из-за внутренней ошибки БАРС")
    b.SendMessage(ctx, msg, logger)
    return
  } else if err != nil {
    msg := UseReportCommand(m.Chat.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  err = b.cache.SetUserMarks(ctx, m.Chat.ID, marks)
  if err != nil {
    logger.Error("could not set marks", zap.Error(err))
    msg := UseReportCommand(m.Chat.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  msg := tgbotapi.NewMessage(m.Chat.ID, "Выберите предмет")
  msg.ReplyToMessageID = m.MessageID
  msg.ReplyMarkup = users.GenerateDisciplinesKeyboard(marks, -1, users.CallbackUserDisciplinesPrefix)
  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleSettings handles /settings command
func (b Bot) HandleSettings(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleSettings")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  user, err := users.GetUserByChatID(ctx, b.db.NewUserRepository(), m.Chat.ID)
  if err != nil {
    logger.Error("could not get user settings")
  }
  msg := tgbotapi.NewMessage(m.Chat.ID, "Настройки")
  if err == nil {
    msg.Text += "\n\n"
    if user.GetUserSetting(users.SettingsUseEmoji) {
      for i := range users.SettingsNames {
        if user.GetUserSetting(i) {
          msg.Text += emoji.CheckMark
        } else {
          msg.Text += emoji.CrossMark
        }
        msg.Text += users.SettingsNames[i]
        msg.Text += "\n"
      }
    } else {
      for i := range users.SettingsNames {
        msg.Text += users.SettingsNames[i] + " - "
        if user.GetUserSetting(i) {
          msg.Text += " ВКЛЮЧЕНО"
        } else {
          msg.Text += "ВЫКЛЮЧЕНО"
        }
      }
      msg.Text += "\n"
    }
  }
  msg.ParseMode = "markdown"
  msg.ReplyMarkup = users.GenerateSettingsKeyboard()

  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleReport handles /report command
func (b Bot) HandleReport(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleReport")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  msg := tgbotapi.NewMessage(m.Chat.ID, "Введите текст репорта.\nИспользуйте /cancel для отмены.")
  err := b.cache.SetUserState(ctx, m.Chat.ID, users.StateReportTextWanted)
  if err != nil {
    logger.Error("could not save state to cahce", zap.Error(err))
    msg.Text = "Не удалось сохранить состояние в кэш.\nНапишите мне в личку @fegor04"
  }

  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleAddHomework handles /addhw command
func (b Bot) HandleAddHomework(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleAddHomework")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  msg := tgbotapi.NewMessage(m.Chat.ID, "Укажите дату, предмет и само домашнее задание.\nВы можете добавить несколько фотографий / документов")
  msg.ReplyToMessageID = m.MessageID
  msg.ReplyMarkup = users.GenerateAddHomeworkKeyboard(false)

  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleUnknownCommand handles unknown or forbidden command
func (b Bot) HandleUnknownCommand(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleUnknownCommand")
  defer span.End()

  logger.Info("User sent unknown or forbidden command")
  msg := tgbotapi.NewMessage(m.Chat.ID, "Нет такой команды!")
  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

// HandleAddNews handles /addnews command
func (b Bot) HandleAddNews(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleAddNews")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  if (b.db.NewUserRepository()).IsUserAdmin(ctx, m.Chat.ID) {
    msg := tgbotapi.NewMessage(m.Chat.ID, "Введите текст новости")
    b.SendMessage(ctx, msg, logger)
    err := b.cache.SetUserState(ctx, m.Chat.ID, users.StateNewsTextWanted)
    if err != nil {
      logger.Error("could not set user state", zap.Error(err))
    }
    logger.Info("Handled user")

  } else {
    b.HandleUnknownCommand(ctx, m, logger)
  }
}

// HandleSchedule handles /schedule command
func (b Bot) HandleSchedule(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleSchedule")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  isDisabled, _ := b.db.NewConfigRepository().CheckIfScheduleDisabled(ctx)

  if !b.db.NewUserRepository().IsUserAdmin(ctx, user.ChatID) && isDisabled {
    logger.Info("command is currently disabled")
    msg := tgbotapi.NewMessage(user.ChatID, "Данная команда временно отключена")
    b.SendMessage(ctx, msg, logger)
    return
  }

  dateString := m.CommandArguments()
  date := time.Now()
  zone, err := time.LoadLocation("Europe/Moscow")
  if err == nil {
    date = date.In(zone)
  } else {
    logger.Error("could not load zone", zap.Error(err))
  }
  if date.Hour() >= 15 {
    date = time.Now().Add(24 * time.Hour)
  }
  if dateString != "" {
    var err error

    date, err = time.Parse("02-01", dateString)
    if err != nil {
      date = time.Now()
      logger.Error("could not parse time", zap.Error(err))
      if date.Hour() >= 15 {
        date = time.Now().Add(24 * time.Hour)
      }
      msg := tgbotapi.NewMessage(m.Chat.ID, "Неправильный формат даты. Будет использована дата "+date.Format("2006-02-01"))
      b.SendMessage(ctx, msg, logger)
    } else {
      date = date.AddDate(time.Now().Year(), 0, 0)
    }
  }
  if date.Weekday() == time.Sunday {
    date = date.Add(24 * time.Hour)
  }

  userAPI, err := user.GetUserAPI(ctx, b.db.NewUserRepository(), baseURL, 5, time.Second, logger, false)
  if err != nil {
    logger.Error("could not get api", zap.Error(err))
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    b.HandleError(ctx, m.Chat.ID, err, logger)
    return
  }
  logger.Debug("getting schedule", zap.Time("date", date))
  schedule, err := api.RetryGetScheduleWithHomework(ctx, userAPI, date, 5, time.Second*5, logger)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get schedule", zap.Error(err))
    b.HandleError(ctx, m.Chat.ID, err, logger)
    return
  }
  b.SendScheduleMessage(ctx, m.Chat.ID, m.MessageID, user, schedule, date, logger)
  logger.Info("Handled user")
}

// HandleError handles error
func (b Bot) HandleError(ctx context.Context, chatID int64, err error, logger *zap.Logger) {
  if errors.Is(api.ErrInternal, err) {
    msg := tgbotapi.NewMessage(chatID, "Операция не удалась из-за внутренней ошибки БАРС")
    b.SendMessage(ctx, msg, logger)
    return
  } else if err != nil {
    msg := UseReportCommand(chatID)
    b.SendMessage(ctx, msg, logger)
    return
  }
}

// HandleAdmin handles /admin command
func (b Bot) HandleAdmin(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleAdmin")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  if (b.db.NewUserRepository()).IsUserAdmin(ctx, m.Chat.ID) {
    msg := tgbotapi.NewMessage(m.Chat.ID, fmt.Sprintf(adminMenuText, b.botConfig.Version, b.botConfig.Environment))
    msg.ParseMode = tgbotapi.ModeMarkdownV2
    msg.ReplyMarkup = users.AdminKeyboard
    b.SendMessage(ctx, msg, logger)
  } else {
    b.HandleUnknownCommand(ctx, m, logger)
  }
}

// HandleCancel handles /cancel command
func (b Bot) HandleCancel(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleCancel")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()
  logger.Info("Handling user")

  user, err := users.GetUserByChatID(ctx, b.db.NewUserRepository(), m.Chat.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get user from database", zap.Error(err))
    msg := tgbotapi.NewMessage(m.Chat.ID, "Произошла ошибка! Не удалось получить информацию из базы данных.")
    msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
    b.SendMessage(ctx, msg, logger)
    return
  }

  err = b.cache.SetUserState(ctx, m.Chat.ID, user.GetDefaultState())
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not set user state", zap.Error(err))
    msg := tgbotapi.NewMessage(m.Chat.ID, "Произошла ошибка! Не удалось изменить состояние.")
    msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
    b.SendMessage(ctx, msg, logger)
    return
  }
  msg := tgbotapi.NewMessage(m.Chat.ID, "Операция отменена")
  msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
  b.SendMessage(ctx, msg, logger)
}

// SendScheduleUserHomeworks send user's homework
func (b Bot) SendScheduleUserHomeworks(ctx context.Context, chatID int64, date time.Time, school, class string, logger *zap.Logger) {
  var span trace.Span
  ctx, span = otel.Tracer(tracerName).Start(ctx, "SendScheduleUserHomeworks")
  defer span.End()

  homeworks, err := b.db.NewHomeworksRepository().GetHomeworks(ctx, date, school, class)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    if logger != nil {
      logger.Error("could not get users schedule with text only", zap.Error(err))
    }
    return
  }
  span.AddEvent("got homeworks", trace.WithAttributes(attribute.Int("len", len(homeworks))))

  for _, hw := range homeworks {

    var docs []database.HomeworkFile
    var images []database.HomeworkFile

    author, err := b.bot.GetChat(tgbotapi.ChatInfoConfig{ChatConfig: tgbotapi.ChatConfig{ChatID: hw.Author}})
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      continue
    }

    var authorOutName = ""
    if author.UserName != "" {
      authorOutName = "@" + author.UserName
    } else {
      if author.FirstName != "" {
        authorOutName = author.FirstName
      }
      if author.LastName != "" {
        authorOutName += " " + author.LastName
      }
    }

    for i := 0; i < len(hw.Files); i++ {
      file := hw.Files[i]
      if file.IsDocument {
        docs = append(docs, file)
      } else {
        images = append(images, file)
      }
    }

    span.AddEvent("handling homework",
      trace.WithAttributes(
        attribute.Int("id", hw.ID),
        attribute.Int64("author.id", hw.Author),
        attribute.String("subject", hw.Subject),
        attribute.String("text", hw.Text),
        attribute.String("date", hw.Date.Format("2006-02-01")),
        attribute.Int("img.cnt", len(images)),
        attribute.Int("docs.cnt", len(docs)),
        attribute.Int("files.cnt", len(hw.Files)),
        attribute.String("author.outname", authorOutName),
      ),
    )

    if images == nil && docs == nil {
      span.AddEvent("sending text only")
      msg := tgbotapi.NewMessage(chatID, fmt.Sprintf("%s - %s\nДобавил(а): %s", hw.Subject, hw.Text, authorOutName))
      b.SendMessage(ctx, msg, nil)
      continue
    }

    if len(images) >= 2 && len(images) <= 10 {
      var imagesFiles []interface{}
      for _, img := range images {
        file := tgbotapi.NewInputMediaPhoto(tgbotapi.FileID(img.FileID))
        file.Caption = fmt.Sprintf("%s - %s\nДобавил(а): %s", hw.Subject, hw.Text, authorOutName)
        imagesFiles = append(imagesFiles, file)
      }
      mediaGroup := tgbotapi.MediaGroupConfig{
        ChatID: chatID,
        Media:  imagesFiles,
      }
      b.SendMediaGroup(ctx, mediaGroup, logger)
    } else if len(images) == 1 || len(images) > 10 {
      for i, img := range images {
        span.AddEvent("handling image", trace.WithAttributes(attribute.Int("img.id", i), attribute.String("file.id", img.FileID)))
        msg := tgbotapi.NewPhoto(chatID, tgbotapi.FileID(img.FileID))
        if i == 0 {
          msg.Caption = fmt.Sprintf("%s - %s\n", hw.Subject, hw.Text)
          msg.Caption += fmt.Sprintf("Добавил(а): %s\n", authorOutName)
        }
        msg.Caption += fmt.Sprintf("Фото %d из %d", i+1, len(images))
        b.SendMessage(ctx, msg, nil)
      }
    }

    for i, doc := range docs {
      span.AddEvent("handling doc", trace.WithAttributes(attribute.Int("doc.id", i), attribute.String("file.id", doc.FileID)))
      msg := tgbotapi.NewDocument(chatID, tgbotapi.FileID(doc.FileID))
      if i == 0 && images == nil {
        msg.Caption = fmt.Sprintf("%s - %s\n", hw.Subject, hw.Text)
        msg.Caption += fmt.Sprintf("Добавил(а): %s\n", authorOutName)
      }
      msg.Caption += fmt.Sprintf("Документ %d из %d", i+1, len(docs))
      b.SendMessage(ctx, msg, nil)
    }
  }
}

// HandleAllMarks handles /allmarks command
func (b Bot) HandleAllMarks(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleAllMarks")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  logger.Info("Handling user")
  marks, err := user.GetMarks(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger)
  if err != nil {
    logger.Error("could not get marks", zap.Error(err))
    span.RecordError(err)
    msg := tgbotapi.NewMessage(m.Chat.ID, "Не удалось получить оценки. Попробуйте позже.")
    b.SendMessage(ctx, msg, logger)
    return
  }
  msg := tgbotapi.NewMessage(m.Chat.ID, "Выберите предмет")
  msg.ReplyMarkup = users.GenerateDisciplinesKeyboard(marks, -1, users.CallbackAllMarksPrefix)
  b.SendMessage(ctx, msg, logger)
  logger.Info("Handled user")
}

func (b Bot) HandleWeekReport(ctx context.Context, m *tgbotapi.Message, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("HandleWeekReport")
  defer span.End()

  logger.Info("Handling user")
  updaterURL := b.botConfig.UpdaterURL
  path := "/sendWeekReport"

  client := http.Client{}
  client.Transport = otelhttp.NewTransport(http.DefaultTransport)
  req, err := http.NewRequestWithContext(ctx, "POST", updaterURL+path, strings.NewReader(strconv.FormatInt(user.ChatID, 10)))
  req.Header.Set("Auth", b.botConfig.SetIntervalToken)
  if err != nil {
    logger.Error("could not create send week report request", zap.Error(err))
    span.RecordError(err)
    return
  }

  resp, err := client.Do(req)
  if resp.StatusCode == 200 && err == nil {
    logger.Info("everything is fine")
  } else if err == nil && resp.StatusCode != 200 {
    logger.Info("something went wrong, no error though")
    b.SendMessage(ctx, UseReportCommand(m.Chat.ID), logger)
  } else if err != nil {
    logger.Error("something went wrong", zap.Error(err))
    b.SendMessage(ctx, UseReportCommand(m.Chat.ID), logger)
  }
  logger.Info("Handled user")
}
