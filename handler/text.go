package main

import (
	"context"
	"fmt"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/database"
	"gitlab.com/bars-bot/bars-bot/users"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// HandleText handles incoming text
func (b *Bot) HandleText(ctx context.Context, m *tgbotapi.Message, userState int, logger *zap.Logger) {
	switch userState {
	case users.StateEmptyUser:
		msg := tgbotapi.NewMessage(m.Chat.ID, "Используйте команду /start")
		b.SendMessage(ctx, msg, logger)
	case users.StateLoginWanted:
		b.HandleLoginInput(ctx, m, logger.With(zap.String("user_msg", m.Text), zap.String("event", "login_input")))
	case users.StatePasswordWanted:
		b.HandlePasswordInput(ctx, m, logger.With(zap.String("event", "password_input")))
	case users.StateNewsTextWanted:
		b.HandleNewsTextInput(ctx, m, logger.With(zap.String("event", "news_input"), zap.String("user_msg", m.Text)))
	case users.StateNewsConfirmWanted:
		b.HandleNewsConfirm(ctx, m, logger.With(zap.String("event", "news_confirm"), zap.String("user_msg", m.Text)))
	case users.StateReportTextWanted:
		b.HandleReportTextInput(ctx, m, logger.With(zap.String("event", "report_input"), zap.String("user_msg", m.Text)))
	case users.StateAdminReportAnswerWanted:
		b.HandleReportAnswerInput(ctx, m, logger.With(zap.String("event", "report_answer_input"), zap.String("user_msg", m.Text)))
	case users.StateHomeworkDateWanted:
		b.HandleHomeworkDateInput(ctx, m, logger.With(zap.String("event", "homework_date_input"), zap.String("user_msg", m.Text)))
	case users.StateHomeworkTextWanted:
		b.HandleHomeworkTextInput(ctx, m, logger.With(zap.String("event", "homework_text_input"), zap.String("user_msg", m.Text)))
	case users.StateHomeworkSubjectWanted:
		b.HandleHomeworkSubjectInput(ctx, m, logger.With(zap.String("event", "homework_subject_input"), zap.String("user_msg", m.Text)))
	case users.StateHomeworkFileWanted:
		b.HandleHomeworkAddFile(ctx, m, logger.With(zap.String("event", "homework_add_file"), zap.String("user_msg", m.Text)))
	}
}

// HandleHomeworkDateInput handles date input in /add
func (b *Bot) HandleHomeworkDateInput(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleHomeworkTextInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
		attribute.String("msg.caption", m.Caption),
		attribute.Int("msg.id", m.MessageID),
		attribute.Int("msg.photos.len", len(m.Photo)),
		attribute.String("msg.time", m.Time().String()),
	)

	text := m.Text

	date, err := time.Parse("02-01", text)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not parse date", zap.Error(err))
		msg := tgbotapi.NewMessage(m.Chat.ID, "Дата введена в неверном формате. Вв")
		b.SendMessage(ctx, msg, logger)
		return
	}

	err = b.cache.SetUserHomeworkDate(ctx, m.Chat.ID, date)
	if err != nil {
		msg := UseReportCommand(m.Chat.ID)
		b.SendMessage(ctx, msg, logger)
		return
	}

	err = b.cache.SetUserState(ctx, m.Chat.ID, users.StateUserLoggedIn)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user state", zap.Error(err))
		msg := UseReportCommand(m.Chat.ID)
		b.SendMessage(ctx, msg, logger)
		return
	}

	msg := tgbotapi.NewMessage(m.Chat.ID, "Дата сохранена.")
	b.SendMessage(ctx, msg, logger)
}

// HandleHomeworkSubjectInput handles subject input for /addhw
func (b *Bot) HandleHomeworkSubjectInput(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleHomeworkSubjectInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
	)

	msg := tgbotapi.NewMessage(m.Chat.ID, "Предмет сохранен.")
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)

	err := b.cache.SetUserState(ctx, m.Chat.ID, users.StateUserLoggedIn)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user state", zap.Error(err))
		msg.Text = UseReportCommand(msg.ChatID).Text
		b.SendMessage(ctx, msg, logger)
		return
	}

	err = b.cache.SetUserHomeworkSubject(ctx, msg.ChatID, m.Text)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user subject", zap.Error(err))
		msg.Text = UseReportCommand(msg.ChatID).Text
		b.SendMessage(ctx, msg, logger)
		return
	}

	b.SendMessage(ctx, msg, logger)
	logger.Info("handled user")
}

// HandleHomeworkAddFile handles file input for /addhw
func (b *Bot) HandleHomeworkAddFile(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleHomeworkAddFile")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
		attribute.String("msg.caption", m.Caption),
		attribute.Int("msg.id", m.MessageID),
		attribute.Int("msg.photos.len", len(m.Photo)),
		attribute.String("msg.time", m.Time().String()),
	)

	if m.Photo != nil {
		photo := GetBiggestPhoto(m.Photo)
		file := database.HomeworkFile{
			FileID:     photo.FileID,
			IsDocument: false,
		}
		err := b.cache.AddUserHomeworkFile(ctx, m.From.ID, file)
		if err != nil {
			logger.Error("could not add file", zap.Error(err))
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))

			msg := UseReportCommand(m.Chat.ID)
			msg.ReplyToMessageID = m.MessageID
			b.SendMessage(ctx, msg, logger)
			return
		}
	} else if m.Document != nil {
		file := database.HomeworkFile{
			FileID:     m.Document.FileID,
			IsDocument: true,
		}

		err := b.cache.AddUserHomeworkFile(ctx, m.From.ID, file)
		if err != nil {
			logger.Error("could not add file", zap.Error(err))
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))

			msg := UseReportCommand(m.Chat.ID)
			msg.ReplyToMessageID = m.MessageID
			b.SendMessage(ctx, msg, logger)
			return
		}
	} else {
		msg := tgbotapi.NewMessage(m.Chat.ID, "Вы не отправили файл.")
		msg.ReplyToMessageID = m.MessageID
		b.SendMessage(ctx, msg, logger)
		return
	}

	msg := tgbotapi.NewMessage(m.Chat.ID, "Файл добавлен")
	msg.ReplyToMessageID = m.MessageID
	b.SendMessage(ctx, msg, logger)
}

// HandleHomeworkTextInput handles homework text input for /addhw
func (b *Bot) HandleHomeworkTextInput(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleHomeworkTextInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
		attribute.String("msg.caption", m.Caption),
		attribute.Int("msg.id", m.MessageID),
		attribute.Int("msg.photos.len", len(m.Photo)),
		attribute.String("msg.time", m.Time().String()),
	)

	text := m.Text
	err := b.cache.SetUserHomeworkText(ctx, m.Chat.ID, text)
	if err != nil {
		msg := UseReportCommand(m.Chat.ID)
		b.SendMessage(ctx, msg, logger)
		return
	}

	err = b.cache.SetUserState(ctx, m.Chat.ID, users.StateUserLoggedIn)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user state", zap.Error(err))
		msg := UseReportCommand(m.Chat.ID)
		b.SendMessage(ctx, msg, logger)
		return
	}

	msg := tgbotapi.NewMessage(m.Chat.ID, "Текст сохранен.\n\n---------\n"+text)
	b.SendMessage(ctx, msg, logger)
}

// HandleLoginInput handles login input for /start
func (b *Bot) HandleLoginInput(ctx context.Context, msg *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleLoginInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", msg.From.String()),
		attribute.Int64("author.id", msg.From.ID),
		attribute.String("msg.text", msg.Text),
	)

	m := tgbotapi.NewMessage(msg.Chat.ID, "Отлично! Теперь введите Ваш пароль")
	logger.Debug("setting user login", zap.String("login", msg.Text))
	err := b.cache.SetUserLogin(ctx, msg.Chat.ID, msg.Text)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user login", zap.Error(err))
	}
	err = b.cache.SetUserState(ctx, msg.Chat.ID, users.StatePasswordWanted)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user state", zap.Error(err))
	}

	b.SendMessage(ctx, m, logger)
}

// HandlePasswordInput handles password input for /start
func (b *Bot) HandlePasswordInput(ctx context.Context, msg *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandlePasswordInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", msg.From.String()),
		attribute.Int64("author.id", msg.From.ID),
	)

	login := b.cache.GetUserLogin(ctx, msg.Chat.ID)
	password := msg.Text

	logger.Debug("trying to auth user", zap.String("login", login), zap.String("password", password))
	userAPI, err := api.RetryCreateFromCredentials(ctx, login, password, "https://school.karelia.ru", 5, time.Second, logger)
	if err == api.ErrWrongPassword {
		m := tgbotapi.NewMessage(msg.Chat.ID, "Неверный пароль!\nЧтобы попробовать еще раз введите /start.")
		logger.Error("could not auth user", zap.Error(err))
		err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateEmptyUser)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not set user state", zap.Error(err))
		}
		b.SendMessage(ctx, m, logger)
	} else if err != nil {
		m := tgbotapi.NewMessage(msg.Chat.ID, "Операция не удалась из-за неизвестной ошибки.\nЧтобы попробовать еще раз введите /start.")
		logger.Error("could not auth user", zap.Error(err))
		err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateEmptyUser)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not set user state", zap.Error(err))
		}
		b.SendMessage(ctx, m, logger)
	} else if err == nil {
		err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateUserLoggedIn)
		if err != nil {
			logger.Error("could not set user state", zap.Error(err))
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		}
		logger.Info("Successfully logged in!")

		personData, err := api.RetryGetPersonData(ctx, userAPI, 5, time.Second, logger)
		m := tgbotapi.NewMessage(msg.Chat.ID, "Успешный вход!")
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not get person data", zap.Error(err))
		} else {
			m.Text += "\nВы вошли как " + personData.FullName + ", " + personData.ClassYear
		}

		b.SendMessage(ctx, m, logger)
		userRepo := b.db.NewUserRepository()
		err = userRepo.InsertUser(ctx, database.RepositoryUser{
			Login:     login,
			Password:  password,
			SessionID: userAPI.GetSessionID(),
			ChatID:    msg.Chat.ID,
			Settings:  users.DefaultSettings,
		})
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not add user to db", zap.Error(err))
		}

		author := msg.Chat.FirstName
		if msg.Chat.LastName != "" {
			author += " " + msg.Chat.LastName
		}
		if msg.Chat.UserName != "" {
			author += " (@" + msg.Chat.UserName + ")"
		}
		adminMsg := tgbotapi.NewMessage(-1, "Новый пользователь: "+author)
		b.SendMessageToAdmins(ctx, adminMsg, logger)

		thisUserScope := tgbotapi.NewBotCommandScopeChat(msg.Chat.ID)
		_, _ = b.Request(ctx, tgbotapi.SetMyCommandsConfig{
			Commands: userCommands,
			Scope:    &thisUserScope,
		})
	}
}

// HandleNewsTextInput handles news text input for /addnews
func (b *Bot) HandleNewsTextInput(ctx context.Context, msg *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleNewsTextInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", msg.From.String()),
		attribute.Int64("author.id", msg.From.ID),
		attribute.String("msg.text", msg.Text),
	)

	newsText := msg.Text
	logger.Info("Handling user")
	err := b.cache.SetUserNewsText(ctx, msg.Chat.ID, newsText)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not add news text to cache", zap.Error(err))
		m := tgbotapi.NewMessage(msg.Chat.ID, "Произошла ошибка. Обратитесь к администратору")
		b.SendMessage(ctx, m, logger)
	}
	logger.Info("Got news text and saved it to cache")
	m1 := tgbotapi.NewMessage(msg.Chat.ID, "Подтвердите новость:")
	m2 := tgbotapi.NewMessage(msg.Chat.ID, newsText)
	m2.ReplyMarkup = GenerateGoodKeyboard([]string{"Подтвердить", "Отмена"})
	b.SendMessage(ctx, m1, logger)
	b.SendMessage(ctx, m2, logger)
	err = b.cache.SetUserState(ctx, msg.Chat.ID, users.StateNewsConfirmWanted)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set user state", zap.Error(err))
	}
}

// HandleNewsConfirm handles news confirmation for /addnews
func (b *Bot) HandleNewsConfirm(ctx context.Context, msg *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleNewsConfirm")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", msg.From.String()),
		attribute.Int64("author.id", msg.From.ID),
		attribute.String("msg.text", msg.Text),
	)

	confirmed := msg.Text == "Подтвердить"
	if confirmed {
		logger.Info("Confirmed news!")
		newsText, err := b.cache.GetUserNewsText(ctx, msg.Chat.ID)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not get news text from cache", zap.Error(err))
		}
		if newsText == "" {
			logger.Info("User confirm time-outed")
			m := tgbotapi.NewMessage(msg.Chat.ID, "Вы слишком долго подтверждали новость. Попробуйте еще раз.")
			m.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
			b.SendMessage(ctx, m, logger)
			err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateUserLoggedIn)
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				logger.Error("could not set user state", zap.Error(err))
			}
		}
		logger.Info("Got user news", zap.String("news", newsText))
		newsMessage := tgbotapi.NewMessage(0, newsText)
		b.SendMessageToEveryone(ctx, newsMessage, logger)
		m := tgbotapi.NewMessage(msg.Chat.ID, "Новость отправлена!")
		m.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		b.SendMessage(ctx, m, logger)
	} else {
		logger.Info("Cancelling news")
		m := tgbotapi.NewMessage(msg.Chat.ID, "Отправка новости отменена")
		m.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		err := b.cache.SetUserState(ctx, msg.Chat.ID, users.StateUserLoggedIn)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not set user state", zap.Error(err))
		}
		b.SendMessage(ctx, m, logger)
	}
}

// HandleReportTextInput handles report text input for /report
func (b Bot) HandleReportTextInput(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleReportTextInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
	)

	logger.Info("handling user")
	reportText := m.Text
	id, err := database.NewReportsRepository(b.db).InsertReport(ctx, database.Report{
		ID:           -1,
		Author:       m.Chat.ID,
		Text:         reportText,
		CreationDate: time.Now(),
		Status:       0,
	})
	if err != nil {
		logger.Error("could not save report to repository", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		msg := tgbotapi.NewMessage(m.Chat.ID, "Не удалось сохранить репорт.\nНапишите мне в личку @fegor04")
		b.SendMessage(ctx, msg, logger)
		return
	}
	err = b.cache.SetUserState(ctx, m.Chat.ID, users.StateUserLoggedIn)
	if err != nil {
		logger.Error("Could not set user state", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	author := ""
	msg := tgbotapi.NewMessage(m.Chat.ID, fmt.Sprintf("Репорт #%04d сохранен!\nСпасибо за отправку!", id))
	b.SendMessage(ctx, msg, logger)
	if m.Chat.LastName != "" {
		author = m.Chat.FirstName + " " + m.Chat.LastName
	} else {
		author = m.Chat.FirstName
	}
	if m.Chat.UserName != "" {
		author += " (@" + m.Chat.UserName + ")"
	}

	text := fmt.Sprintf("Новый репорт\n\n#%04d \nАвтор: %s\nСоздан: %s\nТекст: %s\n\n", id, author, time.Now().Format("2006-02-01 15:01"), reportText)
	msg = tgbotapi.NewMessage(-1, text)
	b.SendMessageToAdmins(ctx, msg, logger)

	logger.Info("Handled user")
}

// HandleReportAnswerInput handles report answer
func (b Bot) HandleReportAnswerInput(ctx context.Context, m *tgbotapi.Message, logger *zap.Logger) {
	span := trace.SpanFromContext(ctx)
	span.SetName("HandleReportAnswerInput")
	defer span.End()
	span.SetAttributes(
		attribute.String("author.name", m.From.String()),
		attribute.Int64("author.id", m.From.ID),
		attribute.String("msg.text", m.Text),
	)

	reportID, err := b.cache.GetAdminChosenReport(ctx, m.Chat.ID)
	if err != nil {
		logger.Error("could not get report from cache", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		b.SendMessage(ctx, UseReportCommand(m.Chat.ID), logger)
		return
	}

	reportAuthor, err := database.NewReportsRepository(b.db).GetReportAuthor(ctx, reportID)
	if err != nil {
		logger.Error("could not get report author")
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		msg := tgbotapi.NewMessage(m.Chat.ID, "Не удалось получить автора репорта из базы данных.")
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		b.SendMessage(ctx, msg, logger)
		return
	}
	msg := tgbotapi.NewMessage(reportAuthor, fmt.Sprintf("Ответ на ваш репорт #%04d\n\n%s", reportID, m.Text))
	b.SendMessage(ctx, msg, logger)
	err = database.NewReportsRepository(b.db).UpdateReportStatus(ctx, reportID, 1)
	if err != nil {
		logger.Error("could not update status", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}

	msg = tgbotapi.NewMessage(m.Chat.ID, "Ответ на репорт отправлен!")
	msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	b.SendMessage(ctx, msg, logger)

	err = b.cache.SetUserState(ctx, m.Chat.ID, users.StateUserLoggedIn)
	if err != nil {
		logger.Error("could not set user state", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
}
