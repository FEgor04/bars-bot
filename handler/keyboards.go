package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"math"
)

// GenerateKeyboard generates keyboard with given choice
func GenerateKeyboard(choice []string, rowCnt int) tgbotapi.ReplyKeyboardMarkup {
	var buttons []tgbotapi.KeyboardButton
	for i := range choice {
		buttons = append(buttons, tgbotapi.NewKeyboardButton(choice[i]))
	}

	var rows [][]tgbotapi.KeyboardButton
	lastI := 0
	n := len(buttons)
	for i := 0; i < n/rowCnt; i++ {
		var rowNow []tgbotapi.KeyboardButton
		for j := 0; j < rowCnt; j++ {
			rowNow = append(rowNow, buttons[rowCnt*i+j])
		}
		rows = append(rows, rowNow)
		lastI = rowCnt*i + rowCnt - 1
	}
	var kek []tgbotapi.KeyboardButton
	for i := lastI + 1; i < n; i++ {
		kek = append(kek, buttons[i])
	}

	if len(kek) > 0 {
		rows = append(rows, kek)
	}

	keyboard := tgbotapi.ReplyKeyboardMarkup{
		Keyboard:        rows,
		ResizeKeyboard:  true,
		Selective:       true,
		OneTimeKeyboard: true,
	}
	return keyboard
}

// GenerateGoodKeyboard generates keyboard which look close to the square
func GenerateGoodKeyboard(choice []string) tgbotapi.ReplyKeyboardMarkup {
	rowCnt := int(math.Round(math.Sqrt(float64(len(choice)))))
	return GenerateKeyboard(choice, rowCnt)
}
