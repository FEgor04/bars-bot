package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/roylee0704/gron"
	"gitlab.com/bars-bot/bars-bot/cache"
	"gitlab.com/bars-bot/bars-bot/config"
	"gitlab.com/bars-bot/bars-bot/database"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	tt "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var timeToHandleUserHistogram = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "handler_time_to_handle",
	Help:    "Time needed to handle user",
	Buckets: []float64{100, 200, 400, 800, 1600, 2500, 3200, 6400},
}, []string{"event"})

var uptimeGauge = promauto.NewGauge(prometheus.GaugeOpts{
	Name: "handler_uptime",
	Help: "Time that updater was up",
})
var startTime time.Time
var uptimeTicker *gron.Cron

var userCommands = []tgbotapi.BotCommand{
	{
		Command:     "/left",
		Description: "Сколько оценок можно/нужно получить для достижения определенных результатов",
	},
	{
		Command:     "/schedule",
		Description: "Получить расписание на DD-MM",
	},
	{
		Command:     "/marks",
		Description: "Получить сводку об оценках",
	},
	{
		Command:     "/allmarks",
		Description: "Получить все оценки по выбранному предмету",
	},
	{
		Command:     "/wreport",
		Description: "Получить отчет об оценках, полученных за неделю",
	},
	{
		Command:     "/cancel",
		Description: "Отмена операции",
	},
	{
		Command:     "/help",
		Description: "Получить сводку о командах бота",
	},
	{
		Command:     "/settings",
		Description: "Настройки",
	},
	{
		Command:     "/report",
		Description: "Отправить сообщение об ошибке",
	},
	{
		Command:     "/addhw",
		Description: "Добавить домашнее задание",
	},
	{
		Command:     "/start",
		Description: "Начало работы",
	},
	{
		Command:     "/logout",
		Description: "Выйти из системы",
	},
}
var adminCommands = []tgbotapi.BotCommand{
	{
		Command:     "/left",
		Description: "Сколько оценок можно/нужно получить для достижения определенных результатов",
	},
	{
		Command:     "/schedule",
		Description: "Получить расписание на DD-MM",
	},
	{
		Command:     "/marks",
		Description: "Получить сводку об оценках",
	},
	{
		Command:     "/allmarks",
		Description: "Получить все оценки по выбранному предмету",
	},
	{
		Command:     "/wreport",
		Description: "Получить отчет об оценках, полученных за неделю",
	},
	{
		Command:     "/cancel",
		Description: "Отмена операции",
	},
	{
		Command:     "/help",
		Description: "Получить сводку о командах бота",
	},
	{
		Command:     "/settings",
		Description: "Настройки",
	},
	{
		Command:     "/report",
		Description: "Отправить сообщение об ошибке",
	},
	{
		Command:     "/addhw",
		Description: "Добавить домашнее задание",
	},
	{
		Command:     "/start",
		Description: "Начало работы",
	},
	{
		Command:     "/admin",
		Description: "Панель админа",
	},
	{
		Command:     "/addnews",
		Description: "Добавить новость",
	},
	{
		Command:     "/logout",
		Description: "Выйти из системы",
	},
}
var defaultCommands = []tgbotapi.BotCommand{
	{
		Command:     "/start",
		Description: "Начало работы",
	},
	{
		Command:     "/help",
		Description: "Получить сводку о командах бота",
	},
	{
		Command:     "/cancel",
		Description: "Отмена операции",
	},
}

func uptime() time.Duration {
	return time.Since(startTime)
}

func updateUptimeCounter() {
	uptimeGauge.Set(float64(uptime().Nanoseconds()))
}

func main() {
	uptimeGauge.Set(0)
	startTime = time.Now()
	uptimeTicker = gron.New()
	uptimeTicker.AddFunc(gron.Every(time.Second*1), updateUptimeCounter)
	uptimeTicker.Start()

	ctx := context.Background()
	config.LoadConfigs()
	botConfig := config.NewBotConfig()

	jaegerExporter, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(botConfig.JaegerEndpoint)))
	if err != nil {
		// cry..
		fmt.Println("Starting with no jaeger")
	}

	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("handler"),
			semconv.ServiceVersionKey.String(botConfig.Version),
			attribute.String("environment", botConfig.Environment)),
	)

	provider := tt.NewTracerProvider(
		tt.WithBatcher(jaegerExporter),
		tt.WithResource(r),
	)
	otel.SetTracerProvider(provider)

	bot := Bot{}
	bot.Init(
		ctx,
		botConfig,
		config.NewDatabaseConfig(),
		config.NewRedisConfig(),
	)
	bot.RegisterCommands(ctx)
	bot.Start()
}

// RegisterCommands registers bot's commands
func (b *Bot) RegisterCommands(ctx context.Context) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "RegisterCommands")
	defer span.End()

	logger := zap.New(b.loggerCore, zap.Fields(zap.String("event", "set_commands")))

	usersScope := tgbotapi.NewBotCommandScopeDefault()
	_, err := b.Request(ctx, tgbotapi.SetMyCommandsConfig{
		Commands: defaultCommands,
		Scope:    &usersScope,
	})
	if err != nil {
		span.RecordError(err)
		logger.Error("could not set default commands", zap.Error(err))
	} else {
		logger.Info("set default commands")
		span.AddEvent("registered default command")
	}

	users, err := b.db.NewUserRepository().GetAllUsersChatID(ctx)
	if err != nil {
		span.RecordError(err)
	} else {
		for _, u := range users {
			scope := tgbotapi.NewBotCommandScopeChat(u)
			_, err = b.Request(ctx, tgbotapi.SetMyCommandsConfig{
				Commands: userCommands,
				Scope:    &scope,
			})
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.Int64("chat.id", u)))
			}
		}
	}
	span.AddEvent("registered user commands", trace.WithAttributes(attribute.Int("users.cnt", len(users))))

	admins, err := b.db.NewUserRepository().GetAdmins(ctx)
	if err != nil {
		span.RecordError(err)
	} else {
		for _, u := range admins {
			scope := tgbotapi.NewBotCommandScopeChat(u)
			_, err = b.Request(ctx, tgbotapi.SetMyCommandsConfig{
				Commands: adminCommands,
				Scope:    &scope,
			})
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.Int64("chat.id", u)))
			}
		}
	}
	span.AddEvent("registered admin commands", trace.WithAttributes(attribute.Int("admins.cnt", len(admins))))
}

// Init inits bot, redis, database
func (b *Bot) Init(ctx context.Context, botConfig config.BotConfig, dbConfig config.DatabaseConfig, redisConfig config.RedisConfig) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "Init")
	defer span.End()

	b.botConfig = botConfig
	b.redisConfig = redisConfig
	b.databaseConfig = dbConfig

	w := zapcore.NewMultiWriteSyncer(
		zapcore.AddSync(os.Stdout),
		zapcore.AddSync(&lumberjack.Logger{
			Filename:   botConfig.LogFolder + "/handler.json",
			MaxSize:    1, // megabytes
			MaxBackups: 3,
			MaxAge:     1, // days
		}),
	)

	var encoder zapcore.Encoder
	if botConfig.Environment == "dev" {
		encoder = zapcore.NewJSONEncoder(zap.NewDevelopmentEncoderConfig())
	} else {
		encoder = zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig())
	}

	b.loggerCore = zapcore.NewCore(
		encoder,
		w,
		zap.DebugLevel,
	)
	hostname, err := os.Hostname()
	if err != nil {
		span.RecordError(err)
	} else {
		b.loggerCore = b.loggerCore.With([]zap.Field{zap.String("hostname", hostname)})
	}

	logger := zap.New(b.loggerCore, zap.Fields(zap.String("event", "init bot")))
	err = tgbotapi.SetLogger(NewBotLogger(zap.New(b.loggerCore, zap.Fields(zap.String("event", "bot_logger"))).Sugar()))
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set bot logger", zap.Error(err))
	}

	logger.Info("Initializing cache system")
	b.cache = cache.RedisCache{}
	err = b.cache.Init(ctx, fmt.Sprintf("%s:%d", b.redisConfig.Host, b.redisConfig.Port), b.redisConfig.Password, b.redisConfig.Database)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Fatal("could not init redis", zap.Error(err))
	} else {
		logger.Info("Redis initialized!")
	}

	logger.Info("Initializing database")
	b.db = &database.Database{}
	err = b.db.Connect(ctx, b.databaseConfig.ConnString())
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Fatal("Could not connect to database", zap.Error(err))
	}
	logger.Info("Connected to database. Running migrations")
	err = b.db.Migrate(ctx, b.databaseConfig.ConnString(), b.databaseConfig.MigrationsFolder, logger)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Fatal("Could not migrate", zap.Error(err))
	}
	logger.Info("Migrated successfully")

	logger.Info("Initializing bot")
	b.bot, err = tgbotapi.NewBotAPI(b.botConfig.Token)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Panic("could not create bot api", zap.Error(err))
		return
	}
	logger.Info("Successfully authed", zap.String("botname", b.bot.Self.String()))
	b.bot.Debug = false

	wantedWebHookURL := fmt.Sprintf("%s/%s", b.botConfig.WebhookURL, b.botConfig.Token)
	webhook, err := b.bot.GetWebhookInfo()
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("Could not get webhook info", zap.Error(err))
	} else {
		logger.Debug("Got webhook info")
		if webhook.URL != wantedWebHookURL {
			logger.Warn("Urls are not equal. Settings new webhook", zap.String("url", wantedWebHookURL))

			webhookConfig, err := tgbotapi.NewWebhook(wantedWebHookURL)
			_, err = b.SetWebhook(ctx, webhookConfig, err)
			if err != nil {
				logger.Error("Could not set webhook", zap.Error(err))
			}
		}
	}
}

// Start starts bot
func (b *Bot) Start() {
	ctx := context.Background()
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "Start")
	defer span.End()

	r := mux.NewRouter()
	logger := zap.New(b.loggerCore, zap.Fields(zap.String("event", "start bot")))
	err := tgbotapi.SetLogger(NewBotLogger(zap.New(b.loggerCore, zap.Fields(zap.String("event", "bot_logger"))).Sugar()))
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not set logger", zap.Error(err))
	}
	logger.Warn("Starting with no TLS mode")
	go func() {
		err := http.ListenAndServe(fmt.Sprintf("%s:%d", "0.0.0.0", 5000), r)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("Server error", zap.Error(err))
		}
	}()
	var updates = make(chan tgbotapi.Update, b.bot.Buffer)
	logger.Debug("Listening for updates")

	r.HandleFunc("/isAlive", b.HandleIsAlive)
	//r.Handle("/debug/vars", expvar.Handler())
	r.Handle("/metrics", promhttp.Handler())
	r.HandleFunc("/"+b.bot.Token, func(w http.ResponseWriter, r *http.Request) {
		bytes, _ := ioutil.ReadAll(r.Body)
		var update tgbotapi.Update
		err := json.Unmarshal(bytes, &update)
		if err != nil {
			logger.Error("could not unmarshal update")
		} else {
			updates <- update
		}
	})

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		<-sigs
		logger.Info("Exiting bot.. Deleting webhook")
		_, err := b.RemoveWebhook(ctx)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not delete webhook", zap.Error(err))
			defer os.Exit(1)
		} else {
			logger.Info("deleted webhook!")
		}
		logger.Info("closing database")
		b.db.Close(ctx)
		logger.Info("closing redis")
		b.cache.Close(ctx)
		defer os.Exit(0)
	}()

	for update := range updates {
		go b.HandleUpdate(update)
	}
}
