package main

import (
	"net/http"

	"go.uber.org/zap"
)

// HandleIsAlive handles isAlive request
func (b *Bot) HandleIsAlive(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte("Yes, I am alive!"))
	if err != nil {
		logger := zap.New(b.loggerCore, zap.Fields(zap.String("event", "handle_is_alive")))
		logger.Error("could not response to request", zap.Error(err))
	}
}
