package main

import (
  "context"
  "fmt"
  "io/ioutil"
  "net/http"
  "strconv"
  "time"

  "go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"

  "github.com/go-redis/redis/v8"
  tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
  "gitlab.com/bars-bot/bars-bot/api"
  "gitlab.com/bars-bot/bars-bot/database"
  "gitlab.com/bars-bot/bars-bot/emoji"
  "gitlab.com/bars-bot/bars-bot/users"
  "go.opentelemetry.io/otel/attribute"
  "go.opentelemetry.io/otel/trace"
  "go.uber.org/zap"
)

// HandleCallback handles user callback
func (b Bot) HandleCallback(ctx context.Context, upd tgbotapi.Update, user users.User, logger *zap.Logger) {
  logger = logger.With(
    zap.String("tg_name", upd.CallbackQuery.From.String()),
    zap.Int64("tg_chat_id", upd.CallbackQuery.From.ID),
  )
  callback := tgbotapi.NewCallback(upd.CallbackQuery.ID, upd.CallbackQuery.Data)
  logger.Info("Got callback from user", zap.String("id", callback.CallbackQueryID), zap.String("data", callback.Text))
  defer logger.Info("Handled callback")
  if callback.Text[:1] == users.CallbackUserSettingsPrefix {
    b.CallbackSettingsChosen(ctx, upd, user, logger.With(zap.String("event", "callback_setting_chosen")))
    return
  }
  if callback.Text[:1] == users.CallbackUserDisciplinesPrefix {
    b.CallbackLeftDisciplineChosen(ctx, upd, user, logger.With(zap.String("event", "callback_discipline_chosen")))
    return
  }
  if callback.Text[:1] == users.CallbackAllMarksPrefix {
    b.CallbackAllMarksDisciplineChosen(ctx, upd, user, logger.With(zap.String("event", "callback_discipline_chosen")))
    return
  }
  switch callback.Text {
  case users.CallbackHomeworkAddFile:
    b.CallbackHomeworkAddFile(ctx, upd, logger)
    return
  case users.CallbackHomeworkSetText:
    b.CallbackHomeworkSetText(ctx, upd, logger)
    return
  case users.CallbackHomeworkReturnToMenu:
    b.CallbackHomeworkReturnToMenu(ctx, upd, logger)
    return
  case users.CallbackHomeworkSetSubject:
    b.CallbackHomeworkSetSubject(ctx, upd, user, logger)
    return
  case users.CallbackHomeworkSetDate:
    b.CallbackHomeworkSetDate(ctx, upd, logger)
    return
  case users.CallbackHomeworkConfirm:
    b.CallbackHomeworkConfirm(ctx, upd, logger)
    return
  }
  if b.db.NewUserRepository().IsUserAdmin(ctx, upd.CallbackQuery.From.ID) {
    if callback.Text[:1] == users.CallbackAdminAnswerReportPrefix {
      b.CallbackReportChosen(ctx, upd, logger.With(zap.String("event", "callback_report_chosen")))
      return
    } else if callback.Text[:1] == users.CallbackAdminChoseInterval {
      b.CallbackIntervalChosen(ctx, upd, logger.With(zap.String("event", "callback_interval_chosen")))
      return
    } else {
      switch callback.Text {
      case users.CallbackAdminAnswerReport:
        b.CallbackAnswerReports(ctx, upd, logger.With(zap.String("event", "callback_answer_reports")))
        return
      case users.CallbackAdminReturnToAdminKeyboard:
        b.CallbackReturnToAdmin(ctx, upd, logger.With(zap.String("event", "callback_return_to_admin")))
        return
      case users.CallbackAdminSetInterval:
        b.CallbackSetInterval(ctx, upd, logger.With(zap.String("event", "callback_set_interval")))
        return
      }
    }
  }

  b.CallbackNotImplemented(ctx, upd, logger.With(zap.String("event", "callback_not_implemented")))
}

// CallbackHomeworkReturnToMenu handles return to homework menu callback
func (b Bot) CallbackHomeworkReturnToMenu(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackReturnToMenu")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  date, err := b.cache.GetUserHomeworkDate(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  subject, err := b.cache.GetUserHomeworkSubject(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  text, err := b.cache.GetUserHomeworkText(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }

  files, err := b.cache.GetUserHomeworkFiles(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }

  confirm := text != "" && subject != "" && date.Year() == time.Now().Year()

  dateStr := date.Format("02-01")
  if date.Year() != time.Now().Year() {
    dateStr = "не указана"
  }
  if subject == "" {
    subject = "не указан"
  }
  if text == "" {
    text = "не указано"
  }

  msgText := fmt.Sprintf("Дата: %s\nПредмет: %s\nДокументы: %d\nИзображения: %d\nДомашнее задание:\n%s", dateStr, subject, len(files.GetDocuments()), len(files.GetImages()), text)
  keyboard := users.GenerateAddHomeworkKeyboard(confirm)

  if _, err = b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, msgText, keyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not edit message", zap.Error(err))
  }
}

// CallbackHomeworkSetText handles set homework text callback
func (b Bot) CallbackHomeworkSetText(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackHomeworkSetText")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  keyboard := tgbotapi.InlineKeyboardMarkup{InlineKeyboard: users.GenerateInlineRows([]tgbotapi.InlineKeyboardButton{tgbotapi.NewInlineKeyboardButtonData("Подтвердить", users.CallbackHomeworkReturnToMenu)}, 1)}
  if err := b.cache.SetUserState(ctx, upd.CallbackQuery.From.ID, users.StateHomeworkTextWanted); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not not set user state", zap.Error(err))

    msg := UseReportCommand(upd.CallbackQuery.From.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Введите текст отдельным сообщением.\nНажмите \"Подтвердить\" по готовности", keyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not edit message", zap.Error(err))
  }

  msg := tgbotapi.NewMessage(upd.CallbackQuery.From.ID, "Отправьте текст отдельным сообщением.\nНажмите \"Подтвердить\" по готовности")
  b.SendMessage(ctx, msg, logger)
}

// CallbackHomeworkAddFile handles homework add file callback
func (b Bot) CallbackHomeworkAddFile(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackHomeworkAddFile")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  keyboard := tgbotapi.InlineKeyboardMarkup{InlineKeyboard: users.GenerateInlineRows([]tgbotapi.InlineKeyboardButton{tgbotapi.NewInlineKeyboardButtonData("Подтвердить", users.CallbackHomeworkReturnToMenu)}, 1)}
  if err := b.cache.SetUserState(ctx, upd.CallbackQuery.From.ID, users.StateHomeworkFileWanted); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not not set user state", zap.Error(err))

    msg := UseReportCommand(upd.CallbackQuery.From.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Отправьте файлы и/или фотографии отдельным сообщением..\nНажмите \"Подтвердить\" по готовности", keyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not edit message", zap.Error(err))
  }

  msg := tgbotapi.NewMessage(upd.CallbackQuery.From.ID, "Отправьте файлы и/или фотографии отдельным сообщением.\nНажмите \"Подтвердить\" по готовности")
  b.SendMessage(ctx, msg, logger)
}

// CallbackHomeworkConfirm handles homework confirm callback
func (b Bot) CallbackHomeworkConfirm(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackHomeworkConfirm")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  date, err := b.cache.GetUserHomeworkDate(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  subject, err := b.cache.GetUserHomeworkSubject(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  text, err := b.cache.GetUserHomeworkText(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }

  files, err := b.cache.GetUserHomeworkFiles(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }

  confirm := text != "" && subject != "" && date.Year() == time.Now().Year()

  dateStr := date.Format("02-01")
  if date.Year() != time.Now().Year() {
    dateStr = "не указана"
  }
  if subject == "" {
    subject = "не указан"
  }
  if text == "" {
    text = "не указано"
  }

  msgText := fmt.Sprintf("Дата: %s\nПредмет: %s\n\nДомашнее задание: %s", dateStr, subject, text)

  if !confirm {
    _, err = b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Не удалось. Попробуйте еще."))
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not send alert", zap.Error(err))
    }

    _, err = b.Request(ctx, tgbotapi.NewEditMessageText(upd.CallbackQuery.Message.Chat.ID, upd.CallbackQuery.Message.MessageID, msgText))
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not edit message", zap.Error(err))
    }
    return
  }

  err = b.db.NewHomeworksRepository().InsertHomework(ctx, database.Homework{
    Author:  upd.CallbackQuery.From.ID,
    Text:    text,
    Date:    date,
    Subject: subject,
    Files:   files,
  })

  if err != nil {
    logger.Error("could not insert homework", zap.Error(err))
    _, err = b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Не удалось. Попробуйте еще."))
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not send alert", zap.Error(err))
    }

    _, err = b.Request(ctx, tgbotapi.NewEditMessageText(upd.CallbackQuery.Message.Chat.ID, upd.CallbackQuery.Message.MessageID, msgText))
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not edit message", zap.Error(err))
    }
    return
  }

  err = b.cache.CleanUserHomework(ctx, upd.CallbackQuery.Message.Chat.ID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }

  _, err = b.Request(ctx, tgbotapi.NewCallback(upd.CallbackQuery.ID, "Успех!"))
  if err != nil {
    logger.Error("could not send callback", zap.Error(err))
  }

  _, err = b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Успех!", tgbotapi.InlineKeyboardMarkup{}))
  if err != nil {
    logger.Error("could not edit message text", zap.Error(err))
  }
}

// CallbackHomeworkSetSubject handles homework set subject callback
func (b Bot) CallbackHomeworkSetSubject(ctx context.Context, upd tgbotapi.Update, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackHomeworkSetSubject")
  defer span.End()
  span.SetAttributes(user.GetTraceAttributes()...)

  msg := tgbotapi.NewMessage(upd.CallbackQuery.From.ID, "")

  date, err := b.cache.GetUserHomeworkDate(ctx, upd.CallbackQuery.From.ID)
  if err != nil && err != redis.Nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get date from database")
  }
  hasErrors := false
  if err == nil {
    API, err := user.GetUserAPI(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger, false)
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not get user api", zap.Error(err))
      hasErrors = true
    } else {
      schedule, err := API.GetScheduleWithHomework(ctx, date)
      if err != nil {
        span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
        logger.Error("could not get user api", zap.Error(err))
        hasErrors = true
      } else if len(schedule.GetDay(date).GetSubjects(true)) != 0 {
        msg.Text = "Выберите предмет из предложенных или введите самостоятельно.\nНажмите \"Подтвердить\" по готовности."
        msg.ReplyMarkup = GenerateGoodKeyboard(schedule.GetDay(date).GetSubjects(true))
      }
    }
  }

  if hasErrors {
    msg.Text = "Введите предмет отдельным сообщением\nНажмите \"Подтвердить\" по готовности."
  }

  keyboard := tgbotapi.InlineKeyboardMarkup{InlineKeyboard: users.GenerateInlineRows([]tgbotapi.InlineKeyboardButton{tgbotapi.NewInlineKeyboardButtonData("Подтвердить", users.CallbackHomeworkReturnToMenu)}, 1)}
  if err := b.cache.SetUserState(ctx, upd.CallbackQuery.From.ID, users.StateHomeworkSubjectWanted); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not not set user state", zap.Error(err))

    msg := UseReportCommand(upd.CallbackQuery.From.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Введите предмет отдельным сообщением.\nНажмите \"Подтвердить\" по готовности", keyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not edit message", zap.Error(err))
  }

  b.SendMessage(ctx, msg, logger)
}

// CallbackHomeworkSetDate handles homework set date callback
func (b Bot) CallbackHomeworkSetDate(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackHomeworkSetDate")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  keyboard := tgbotapi.InlineKeyboardMarkup{InlineKeyboard: users.GenerateInlineRows([]tgbotapi.InlineKeyboardButton{tgbotapi.NewInlineKeyboardButtonData("Подтвердить", users.CallbackHomeworkReturnToMenu)}, 1)}
  if err := b.cache.SetUserState(ctx, upd.CallbackQuery.From.ID, users.StateHomeworkDateWanted); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not not set user state", zap.Error(err))

    msg := UseReportCommand(upd.CallbackQuery.From.ID)
    b.SendMessage(ctx, msg, logger)
    return
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Введите дату отдельным сообщением в формате DD-MM.\nНажмите \"Подтвердить\" по готовности", keyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not edit message", zap.Error(err))
  }
}

// CallbackSetInterval handles set interval menu callback
func (b Bot) CallbackSetInterval(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) { // Нужно показать клавиатуру с выборов интервала
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackSetInterval")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  updaterURL := b.botConfig.UpdaterURL
  path := "/interval"

  currentInterval := "\nТекущий интервал: "
  //client := gentleman.New()
  //client.UseContext(ctx)
  //client.Use(transport.Set(otelhttp.NewTransport(gentleman.DefaultTransport)))
  //req := client.Request()
  //req.Method("GET")
  //req.BaseURL(updaterURL + path)
  //req.AddHeader("Auth", b.botConfig.SetIntervalToken)

  client := http.Client{}
  client.Transport = otelhttp.NewTransport(client.Transport)

  request, _ := http.NewRequestWithContext(ctx, "GET", updaterURL+path, nil)
  request.Header.Set("Auth", b.botConfig.SetIntervalToken)

  response, err := client.Do(request)
  defer func() {
    err := response.Body.Close()
    if err != nil {
      logger.Error("could not close response", zap.Error(err))
    }
  }()
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not get interval", zap.Error(err))
    currentInterval = ""
  }
  if response.StatusCode < 300 {
    responseBytes, err := ioutil.ReadAll(response.Body)
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not read body", zap.Error(err))
      currentInterval = ""
    }
    intervalDur, err := time.ParseDuration(string(responseBytes))
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not parse duration", zap.Error(err))
      currentInterval = ""
    } else {
      currentInterval += fmt.Sprintf("%.0f минут", intervalDur.Minutes())
    }
  }
  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Интервал обновления"+currentInterval, users.SetIntervalKeyboard)); err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not change callback message", zap.Error(err))
  }
}

// CallbackIntervalChosen handles set interval button
func (b Bot) CallbackIntervalChosen(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackIntervalChosen")
  defer span.End()

  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  updaterURL := b.botConfig.UpdaterURL
  // Возможно стоит добавить передачу сообщений о смене интервала с помощью Redis
  // Таким образом можно будет менять интервал сразу на всех updater-ах
  // А не только на том, к которому придет запрос от load balancer
  path := "/interval/set/" + upd.CallbackQuery.Data[1:]

  client := http.Client{
    Transport: otelhttp.NewTransport(http.DefaultTransport),
  }

  request, _ := http.NewRequestWithContext(ctx, "PUT", updaterURL+path, nil)
  request.Header.Set("Auth", b.botConfig.SetIntervalToken)

  response, err := client.Do(request)
  defer func() {
    err := response.Body.Close()
    if err != nil {
      logger.Error("could not close response", zap.Error(err))
    }
  }()

  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Error("could not do request", zap.Error(err), zap.String("base_url", updaterURL), zap.String("path", path))
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Операция не")); err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }
  if response.StatusCode == http.StatusOK {
    //text := "Успех! Интервал установлен на" + upd.CallbackQuery.Data[1:] + " минут"
    logger.Warn("Changed interval", zap.String("new_interval", upd.CallbackQuery.Data[1:]+"m"))
    //if _, err := b.bot.Request(tgbotapi.NewCallback(upd.CallbackQuery.ID, text)); err != nil {
    //  span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    //  logger.Error("could not send callback message", zap.Error(err))
    //}
    currentInterval := "\nТекущий интервал: " + upd.CallbackQuery.Data[1:] + " минут"
    if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, "Интервал обновления"+currentInterval, users.SetIntervalKeyboard)); err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      logger.Error("could not change callback message", zap.Error(err))
    }
  } else {
    responseBytes, err := ioutil.ReadAll(response.Body)
    if err != nil {
      span.RecordError(err)
    }
    text := callbackReportText
    logger.Error("could not change interval", zap.String("response", string(responseBytes)), zap.String("base_url", updaterURL), zap.String("path", path))
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, text)); err != nil {
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }
}

// CallbackSettingsChosen handles change setting callback
func (b Bot) CallbackSettingsChosen(ctx context.Context, upd tgbotapi.Update, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackSettingsChosen")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()

  span.SetAttributes(
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  bitID, err := strconv.Atoi(upd.CallbackQuery.Data[1:])
  if err != nil {
    logger.Error("could not get bit id", zap.Error(err))
    text := callbackReportText
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, text)); err != nil {
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }
  logger.Info("got bit id", zap.Int("bit_id", bitID))
  settings, err := b.db.NewUserRepository().UpdateSettings(ctx, upd.CallbackQuery.From.ID, bitID)
  if err != nil {
    logger.Error("could not get settings", zap.Error(err))
    text := callbackReportText
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, text)); err != nil {
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }

  text := "Настройки\n\n"
  for i := range users.SettingsNames {
    if users.GetMaskBit(settings, users.SettingsUseEmoji) {
      if users.GetMaskBit(settings, i) {
        text += emoji.CheckMark
      } else {
        text += emoji.CrossMark
      }
      text += users.SettingsNames[i]
    } else {
      text += users.SettingsNames[i] + " - "
      if users.GetMaskBit(settings, i) {
        text += "ВКЛЮЧЕНО"
      } else {
        text += "ВЫКЛЮЧЕНО"
      }
    }
    text += "\n"
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, text, users.GenerateSettingsKeyboard())); err != nil {
    logger.Error("could not edit callback message", zap.Error(err))
  }
}

// CallbackReportChosen handles report choice
func (b Bot) CallbackReportChosen(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackReportChosen")
  defer span.End()
  span.SetAttributes(
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )
  //callback := tgbotapi.NewCallback(upd.CallbackQuery.ID, upd.CallbackQuery.Data)
  reportID, err := strconv.Atoi(upd.CallbackQuery.Data[1:])
  chatID := upd.CallbackQuery.Message.Chat.ID
  msg := UseReportCommand(chatID)
  if err != nil {
    logger.Error("could not parse id", zap.Error(err))
    b.SendMessage(ctx, msg, logger)
    return
  }
  logger.Info("got report id", zap.Int("id", reportID))

  err = b.cache.SetUserState(ctx, upd.CallbackQuery.From.ID, users.StateAdminReportAnswerWanted)
  msg = UseReportCommand(chatID)
  if err != nil {
    logger.Error("could not set user state", zap.Error(err))
    b.SendMessage(ctx, msg, logger)
    return
  }

  err = b.cache.SetAdminChosenReport(ctx, chatID, reportID)
  if err != nil {
    logger.Error("could not set chosen report", zap.Error(err))
    b.SendMessage(ctx, msg, logger)
    return
  }

  msg.Text = "Напишите ответ на репорт в чат"
  b.SendMessage(ctx, msg, logger)
}

// CallbackAnswerReports handles answer report menu
func (b Bot) CallbackAnswerReports(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackAnswerReports")
  defer span.End()
  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.String("callback.data", upd.CallbackQuery.Data),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
  )

  reports, err := database.NewReportsRepository(b.db).GetReports(ctx, 0)
  if err != nil {
    logger.Error("could not get reports", zap.Error(err))
    _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.Message.Chat.ID, upd.CallbackQuery.Message.MessageID, UseReportCommand(upd.CallbackQuery.Message.Chat.ID).Text, users.ReturnToAdminKeyboard))
    if err != nil {
      logger.Error("could not update message", zap.Error(err))
    }
  } else {
    var text string
    if len(reports) != 0 {
      text = "Репорты:\n"
    } else {
      text = "Нет репортов!"
    }
    for _, r := range reports {
      chat, err := b.bot.GetChat(tgbotapi.ChatInfoConfig{ChatConfig: tgbotapi.ChatConfig{ChatID: r.Author}})
      var author string
      if err != nil {
        logger.Error("could not get user chat", zap.Error(err))
        author = fmt.Sprintf("%d", r.Author)
      } else {
        if chat.LastName != "" {
          author = chat.FirstName + " " + chat.LastName
        } else {
          author = chat.FirstName
        }
        if chat.UserName != "" {
          author += " (@" + chat.UserName + ")"
        }
      }

      text += fmt.Sprintf("#%04d \nАвтор: %s\nСоздан: %s\nТекст: %s\n\n", r.ID, author, r.CreationDate.Format("2006-02-01 15:01"), r.Text)
    }
    _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.Message.Chat.ID, upd.CallbackQuery.Message.MessageID, text, users.GetReportsKeyboard(reports)))
    if err != nil {
      logger.Error("could not update message", zap.Error(err))
    }
  }
}

// CallbackNotImplemented handles not implemented callback
func (b Bot) CallbackNotImplemented(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackNotImplemented")
  defer span.End()
  span.SetAttributes(
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )
  logger.Warn("user tried something strange")

  _, _ = b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Еще не реализовано!"))
}

// CallbackReturnToAdmin handles return to admin menu callback
func (b Bot) CallbackReturnToAdmin(ctx context.Context, upd tgbotapi.Update, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackReturnToAdmin")
  defer span.End()

  span.SetAttributes(
    attribute.String("author.name", upd.CallbackQuery.From.String()),
    attribute.Int64("author.id", upd.CallbackQuery.From.ID),
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(
    upd.CallbackQuery.Message.Chat.ID,
    upd.CallbackQuery.Message.MessageID,
    fmt.Sprintf(adminMenuText, b.botConfig.Version, b.botConfig.Environment),
    users.AdminKeyboard,
  ))
  if err != nil {
    logger.Error("could not update message", zap.Error(err))
  }
}

// CallbackLeftDisciplineChosen handles discipline choice in /left
func (b Bot) CallbackLeftDisciplineChosen(ctx context.Context, upd tgbotapi.Update, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackLeftDisciplineChosen")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()
  span.SetAttributes(
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  dID, err := strconv.Atoi(upd.CallbackQuery.Data[1:])
  if err != nil {
    logger.Error("could not get discipline id", zap.Error(err))
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, callbackReportText)); err != nil {
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }
  logger.Debug("got discipline id", zap.Int("bit_id", dID))

  var marks api.SummaryMarks
  if len(user.Marks.DisciplineMarks) == 0 {
    marks, err = b.cache.GetUserMarks(ctx, upd.CallbackQuery.From.ID)
    if err != nil {
      if err != redis.Nil {
        span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
        logger.Error("could not get marks from cache", zap.Error(err))
      } else {
        logger.Warn("no marks in cache. getting them for database")
      }
      marks, err = user.GetMarks(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger)
      if err != nil {
        logger.Error("could not get marks", zap.Error(err))
        span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
        if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, callbackReportText)); err != nil {
          logger.Error("could not change callback message", zap.Error(err))
        }
        return
      }
    }
  } else {
    marks = user.Marks
  }
  if dID >= len(marks.DisciplineMarks) {
    span.AddEvent("dId is strange. leaving now", trace.WithAttributes(
      attribute.Int("dId", dID),
      attribute.Int("len(marks)", len(marks.DisciplineMarks)),
    ))
    _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Что-то пошлно не так..."))
    if err != nil {
      logger.Error("could not send alert", zap.Error(err))
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    }
    return
  }

  marks.Sort()
  discipline := marks.DisciplineMarks[dID]
  span.SetAttributes(attribute.String("discipline.name", discipline.Discipline))
  markNow := discipline.Round()
  msgText := ""
  settings, err := b.db.NewUserRepository().GetUserSettings(ctx, upd.CallbackQuery.From.ID)
  if err != nil {
    logger.Error("could not get user settings", zap.Error(err))
  }
  useEmoji := users.GetMaskBit(settings, users.SettingsUseEmoji)
  msgText += discipline.ShortenDisciplineName() + "\n"
  msgText += "Текущая оценка: " + emoji.GetEmoji(strconv.Itoa(markNow), useEmoji)
  msgText += "\nСредний балл: " + discipline.AverageMark
  if users.GetMaskBit(settings, users.SettingsMarksShowGoodAverageMark) {
    msgText += fmt.Sprintf("\nСредний арифметический балл: %.2f", discipline.CalculateAverage())
  }

  msgText += "\n\n"

  for wantedMark := 5; wantedMark > markNow; wantedMark-- {
    //logger.Info("wanted mark cycle", zap.Int("wanted_mark", wantedMark))
    wantedM := api.Mark{Mark: strconv.Itoa(wantedMark)}
    for markToGet := 5; markToGet >= wantedMark; markToGet-- {
      mToGet := api.Mark{Mark: strconv.Itoa(markToGet)}
      count := discipline.CalculateNeeded(wantedMark, markToGet)
      msgText += fmt.Sprintf("Для получения оценки %s за семестр нужно получить еще %d оценок %s", wantedM.Emoji(useEmoji), count, mToGet.Emoji(useEmoji))
      msgText += "\n"
    }
    msgText += "\n"
  }

  for wantedMark := markNow; wantedMark >= 3; wantedMark-- {
    wantedM := api.Mark{Mark: strconv.Itoa(wantedMark)}
    for markToGet := wantedMark - 1; markToGet >= 1; markToGet-- {
      mToGet := api.Mark{Mark: strconv.Itoa(markToGet)}
      count := discipline.CalculateLeft(wantedMark, markToGet)
      if count == 0 {
        msgText += fmt.Sprintf("Нельзя получать оценки %s, если вы хотите получить оценку %s за семестр", mToGet.Emoji(useEmoji), wantedM.Emoji(useEmoji))
        msgText += "\n"
        break
      } else {
        msgText += fmt.Sprintf("Можно получить %d оценок %s, если вы хотите получить оценку %s за семестр", count, mToGet.Emoji(useEmoji), wantedM.Emoji(useEmoji))
      }
      msgText += "\n"
    }
    msgText += "\n"
  }

  if _, err := b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(upd.CallbackQuery.From.ID, upd.CallbackQuery.Message.MessageID, msgText, users.GenerateDisciplinesKeyboard(marks, dID, users.CallbackUserDisciplinesPrefix))); err != nil {
    logger.Error("could not edit callback message", zap.Error(err))
  }
}

// CallbackAllMarksDisciplineChosen handles discipline choice in /allmarks
func (b Bot) CallbackAllMarksDisciplineChosen(ctx context.Context, upd tgbotapi.Update, user users.User, logger *zap.Logger) {
  span := trace.SpanFromContext(ctx)
  span.SetName("CallbackAllMarksDisciplineChosen")
  span.SetAttributes(user.GetTraceAttributes()...)
  defer span.End()
  span.SetAttributes(
    attribute.String("callback.data", upd.CallbackQuery.Data),
  )

  dID, err := strconv.Atoi(upd.CallbackQuery.Data[1:])
  if err != nil {
    logger.Error("could not get discipline id", zap.Error(err))
    if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, callbackReportText)); err != nil {
      logger.Error("could not change callback message", zap.Error(err))
    }
    return
  }
  logger.Debug("got discipline id", zap.Int("bit_id", dID))

  var marks api.SummaryMarks
  if len(user.Marks.DisciplineMarks) == 0 {
    marks, err = b.cache.GetUserMarks(ctx, upd.CallbackQuery.From.ID)
    if err != nil {
      if err != redis.Nil {
        span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
        logger.Error("could not get marks from cache", zap.Error(err))
      } else {
        logger.Warn("no marks in cache. getting them for database")
      }
      marks, err = user.GetMarks(ctx, b.db.NewUserRepository(), baseURL, 3, time.Second, logger)
      if err != nil {
        logger.Error("could not get marks", zap.Error(err))
        span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
        if _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, callbackReportText)); err != nil {
          logger.Error("could not change callback message", zap.Error(err))
        }
        return
      }
    }
  } else {
    marks = user.Marks
  }
  if dID >= len(marks.DisciplineMarks) {
    span.AddEvent("dId is strange. leaving now", trace.WithAttributes(
      attribute.Int("dId", dID),
      attribute.Int("len(marks)", len(marks.DisciplineMarks)),
    ))
    _, err := b.Request(ctx, tgbotapi.NewCallbackWithAlert(upd.CallbackQuery.ID, "Что-то пошлно не так..."))
    if err != nil {
      logger.Error("could not send alert", zap.Error(err))
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    }
    return
  }

  marks.Sort()

  msgText := ""
  msgText += marks.DisciplineMarks[dID].ShortenDisciplineName()
  msgText += fmt.Sprintf("\nСредний балл: %s", marks.DisciplineMarks[dID].AverageMark)
  if user.GetUserSetting(users.SettingsMarksShowGoodAverageMark) {
    msgText += fmt.Sprintf("\nСредний арифметический балл: %.2f", marks.DisciplineMarks[dID].CalculateAverage())
  }

  for _, mark := range marks.DisciplineMarks[dID].Marks {
    msgText += fmt.Sprintf(
      "\n%s: %s. %s",
      mark.GetOutputDate(),
      mark.Emoji(user.GetUserSetting(users.SettingsUseEmoji)),
      mark.Description,
    )
  }

  _, _ = b.Request(ctx, tgbotapi.NewEditMessageTextAndMarkup(
    upd.CallbackQuery.From.ID,
    upd.CallbackQuery.Message.MessageID,
    msgText,
    users.GenerateDisciplinesKeyboard(marks, dID, users.CallbackAllMarksPrefix),
  ))
}
