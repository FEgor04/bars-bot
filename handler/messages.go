package main

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

const adminMenuText = "Панель админа\nТекущая версия: %s\nОкружение: %s"
const callbackReportText = "Операция не удалась. Обратитесь в /report"

func UseReportCommand(chatID int64) tgbotapi.MessageConfig {
	msg := tgbotapi.NewMessage(chatID, "Операция не удалась. Обратитесь в /report или откройте [issue](https://gitlab.com/FEgor04/bars-bot/-/issues).")
	msg.ParseMode = tgbotapi.ModeMarkdown
	return msg
}
