package main

import (
  "context"
  _ "expvar"
  "fmt"
  "go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
  "net/http"
  "os"
  "os/signal"
  "sync"
  "syscall"
  "time"

  tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
  "github.com/gorilla/mux"
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
  "github.com/prometheus/client_golang/prometheus/promhttp"
  "github.com/roylee0704/gron"
  "gitlab.com/bars-bot/bars-bot/config"
  "gitlab.com/bars-bot/bars-bot/database"
  "go.opentelemetry.io/otel"
  "go.opentelemetry.io/otel/attribute"
  "go.opentelemetry.io/otel/trace"
  "go.uber.org/zap"
  "go.uber.org/zap/zapcore"
  "gopkg.in/natefinch/lumberjack.v2"
)

const baseURL = "https://school.karelia.ru"
const maxTries = 1

// Updater stores all updater data and needed structures
type Updater struct {
  loggerCore     zapcore.Core
  bot            *tgbotapi.BotAPI
  db             *database.Database
  UpdateInterval time.Duration
  UpdatesTicker  *gron.Cron
  ReportsTicker  *gron.Cron
  config         config.BotConfig
}

var updateTimeHistogram = promauto.NewHistogram(prometheus.HistogramOpts{
  Name:    "updater_time_to_update_user",
  Help:    "Time used to update user",
  Buckets: []float64{200, 400, 600, 800, 1200, 1600, 2000, 3000, 4000, 5000},
})

var newMarksCounter = promauto.NewCounter(prometheus.CounterOpts{
  Name: "updater_new_marks",
  Help: "New marks found",
})

var currentIntervalGauge = promauto.NewGauge(prometheus.GaugeOpts{
  Name: "updater_current_interval",
  Help: "Updater's current interval",
})

var uptimeGauge = promauto.NewGauge(prometheus.GaugeOpts{
  Name: "updater_uptime",
  Help: "Time that updater was up",
})

// Init inits updater
func (u *Updater) Init(ctx context.Context, botConfig config.BotConfig, dbConfig config.DatabaseConfig, logFolder string) {
  var span trace.Span
  ctx, span = otel.Tracer(tracerName).Start(ctx, "Init")
  defer span.End()
  span.AddEvent("TEST")

  w := zapcore.NewMultiWriteSyncer(
    zapcore.AddSync(os.Stdout),
    zapcore.AddSync(&lumberjack.Logger{
      Filename:   logFolder + "/updater.json",
      MaxSize:    1, // megabytes
      MaxBackups: 3,
      MaxAge:     1, // days
    }),
  )

  var encoder zapcore.Encoder
  if botConfig.Environment == "dev" {
    encoder = zapcore.NewJSONEncoder(zap.NewDevelopmentEncoderConfig())
  } else {
    encoder = zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig())
  }
  u.loggerCore = zapcore.NewCore(
    encoder,
    w,
    zap.DebugLevel,
  ).With([]zap.Field{zap.String("version", botConfig.Version), zap.String("environment", botConfig.Environment)})
  hostname, err := os.Hostname()
  if err != nil {
    span.RecordError(err)
  } else {
    u.loggerCore = u.loggerCore.With([]zap.Field{zap.String("hostname", hostname)})
  }
  logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "init")))
  logger.Info("Connecting to database")
  u.db = &database.Database{}
  err = u.db.Connect(ctx, dbConfig.ConnString())
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Fatal("Could not connect to database", zap.Error(err))
  }
  logger.Info("Connected to database!")

  u.UpdateInterval, err = u.db.NewConfigRepository().GetInterval(ctx)
  if err != nil {
    logger.Error("could not get interval from database", zap.Error(err))
  } else {
    logger.Info("loaded interval from db", zap.Duration("interval", u.UpdateInterval))
  }

  //client := http.DefaultClient
  //client.Transport = otelhttp.NewTransport(http.DefaultTransport)
  //u.bot, err = tgbotapi.NewBotAPIWithClient(botConfig.Token, tgbotapi.APIEndpoint, client)
  u.bot, err = tgbotapi.NewBotAPI(botConfig.Token)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    logger.Fatal("Could not connect to bot", zap.Error(err))
  }
  logger.Info("Connected to bot!", zap.String("bot_name", u.bot.Self.String()))
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGTERM, syscall.SIGINT)
  go func() {
    <-sigs
    logger.Info("exiting updater")
    u.db.Close(ctx)
    defer os.Exit(0)
  }()
}

// StartUpdatesTicker starts updater's ticker
func (u *Updater) StartUpdatesTicker() {
  currentIntervalGauge.Set(u.UpdateInterval.Minutes())
  //var span trace.Span
  //ctx, span = otel.Tracer(tracerName).Start(ctx, "StartUpdatesTicker")
  //defer span.End()
  logger := zap.New(u.loggerCore).With(zap.String("event", "start_ticker"))
  logger.Info("starting ticker", zap.Duration("dur", u.UpdateInterval))
  if u.UpdatesTicker != nil {
    u.UpdatesTicker.Stop()
  }
  u.UpdatesTicker = gron.New()
  logger.Info("starting ticker", zap.String("interval", u.UpdateInterval.String()))
  var mutex sync.Mutex
  u.UpdatesTicker.AddFunc(gron.Every(u.UpdateInterval), func() {
    mutex.Lock()
    ctx := context.Background()
    var span trace.Span
    ctx, span = otel.Tracer(tracerName).Start(ctx, "UpdateUsers")
    defer span.End()
    u.UpdateUsersMarks(ctx)
    u.UpdateUsersPersonData(ctx)
    mutex.Unlock()
  })
  u.UpdatesTicker.Start()
}

// StartServer starts http server
func (u *Updater) StartServer(port int) {
  logger := zap.New(u.loggerCore)

  r := mux.NewRouter()
  // r.Use(otelmux.Middleware(tracerName))
  r.HandleFunc("/isAlive", u.HandleIsAlive).Methods("GET")
  r.Handle("/metrics", promhttp.Handler())
  r.Handle("/interval", otelhttp.NewHandler(http.HandlerFunc(u.HandleGetInterval), "GetInterval")).Methods("GET")
  r.Handle("/interval/set/{val}", otelhttp.NewHandler(http.HandlerFunc(u.HandleSetInterval), "SetInterval")).Methods("PUT")
  r.Handle("/sendWeekReport", otelhttp.NewHandler(http.HandlerFunc(u.HandleSendWeekReport), "SendWeekReport")).Methods("POST")
  //r.Handle("/debug/vars", expvar.Handler()).Methods("GET")
  //expvar.Publish("update_interval", u.UpdateInterval)
  //expvar.Publish("cmdline", expvar.Func(expvar.cmd))
  logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r).Error())
}
