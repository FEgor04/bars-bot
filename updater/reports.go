package main

import (
	"context"
	"database/sql"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/roylee0704/gron"
	"github.com/roylee0704/gron/xtime"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/users"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"time"
)

const showDeletedMarks = true
const shouldSendWeekReports = true

func (u Updater) SendReports() {
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "send_reports")))
	if time.Now().Weekday() == time.Saturday {
		ctx := context.Background()
		var span trace.Span
		ctx, span = otel.Tracer(tracerName).Start(ctx, "SendReports")
		defer span.End()
		logger.Info("Sending reports to usersToUpdate")
		usersToUpdate, err := u.db.NewUserRepository().GetAllUsersChatID(ctx)
		if err != nil {
			logger.Error("could not get usersToUpdate", zap.Error(err))
			span.RecordError(err)
			return
		}
		logger.Info("got all usersToUpdate", zap.Int("len", len(usersToUpdate)))
		for i, user := range usersToUpdate {
			logger.Info("Updating user", zap.Int("id", i), zap.Int64("tg.id", user))
			u.SendReportToUser(ctx, user, true)
		}
	} else {
		logger.Info("Today is not saturday, leaving")
	}
}

func (u Updater) SendReportToUser(ctx context.Context, userID int64, invokedByTimer bool) {
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "send_report_to_user"), zap.Int64("tg.id", userID)))
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "SendReports")
	defer span.End()

	oldMarks, err := u.db.NewWeekMarksRepository().GetCurrentWeekMarks(ctx, userID)
	if err != nil && err != sql.ErrNoRows {
		logger.Error("could not get user's week_marks", zap.Error(err))
		span.RecordError(err)
		return
	}

	user, userErr := users.GetUserByChatID(ctx, u.db.NewUserRepository(), userID)
	if userErr != nil {
		logger.Error("could not get user from repository", zap.Error(err))
		span.RecordError(err)
		return
	}
	if err == sql.ErrNoRows {
		oldMarks = user.Marks
	}

	span.SetAttributes(user.GetTraceAttributes()...)
	logger = logger.With(user.GetLoggerFields()...)

	logger.Info("got users oldMarks", zap.Int("ln", oldMarks.MarksCount()))

	useEmoji := user.GetUserSetting(users.SettingsUseEmoji)
	showDate := user.GetUserSetting(users.SettingsUpdatesShowMarkDate)
	showDescription := user.GetUserSetting(users.SettingsUpdatesShowMarkDescription)
	text, totalMarksCount := api.GetDifferenceMessage(oldMarks, user.Marks, useEmoji, showDate, showDescription, logger)
	message := tgbotapi.NewMessage(user.ChatID, text)
	message.ParseMode = tgbotapi.ModeMarkdown
	if totalMarksCount > 0 || !invokedByTimer {
		func() {
			var span trace.Span
			ctx, span = otel.Tracer(tracerName).Start(ctx, "SendMessage", trace.WithAttributes(attribute.String("text", message.Text)))
			defer span.End()

			if message.Text == "" {
				message.Text = "За эту неделю вы не получили ни одной оценки"
			}

			if _, err := u.bot.Send(message); err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				logger.Error("could not send message", zap.Error(err))
			}
		}()
	} else {
		logger.Info("no new oldMarks. wont send anything")
	}
}

func (u Updater) StartReportsTicker() {
	u.ReportsTicker = gron.New()
	u.ReportsTicker.AddFunc(gron.Every(xtime.Day).At("15:00"), u.SendReports)
	u.ReportsTicker.Start()
}
