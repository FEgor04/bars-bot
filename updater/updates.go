package main

import (
	"context"
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/bars-bot/bars-bot/api"
	"gitlab.com/bars-bot/bars-bot/users"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"sync"
	"time"
)

// UpdateUser updates user marks
func (u Updater) UpdateUser(ctx context.Context, wg *sync.WaitGroup, user users.User, logger *zap.Logger) {
	timeStart := time.Now()
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "UpdateUser")
	defer span.End()
	defer wg.Done()
	span.SetAttributes(user.GetTraceAttributes()...)
	logger = logger.With(user.GetLoggerFields()...)

	userAPI, err := user.GetUserAPI(ctx, u.db.NewUserRepository(), baseURL, maxTries, time.Second, logger, false)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		if errors.Is(err, api.ErrUnauthorized) {
			logger.Error("user is not authorized!")
		}
		logger.Error("could not get user api. skipping update", zap.Error(err))
		return
	}
	logger.Info("Got user api")

	marks, err := api.RetryGetSummaryMarks(ctx, userAPI, time.Now(), maxTries, time.Second, logger)
	if err != nil {
		if errors.Is(err, api.ErrUnauthorized) {
			logger.Warn("bad api in database, trying to update it")
			userAPI, err := user.GetUserAPI(ctx, u.db.NewUserRepository(), baseURL, maxTries, time.Second, logger, true)
			if err != nil {
				logger.Error("could not get user api", zap.Error(err))
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				return
			}
			marks, err = userAPI.GetPersonMarks(ctx, time.Now())
			if err != nil {
				logger.Error("could not get user api", zap.Error(err))
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				return
			}
		} else {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not get users marks. skipping update", zap.Error(err))
			return
		}
	}

	difference := api.FindDifference(user.Marks, marks)
	oldMarks := user.Marks
	newMarks := marks
	marksWereEmpty := len(user.Marks.DisciplineMarks) == 0
	user.Marks = marks
	if len(difference) == 0 {
		logger.Debug("No difference found")
	} else if marksWereEmpty {
		logger.Debug("Old marks were empty")
	} else {
		newMarksCounter.Add(float64(len(difference)))
		logger.Debug("Found difference!")
		span.AddEvent("Found difference")
		u.SendDifferenceMessage(ctx, oldMarks, newMarks, user, logger)
	}
	err = u.db.NewUserRepository().UpdateUserMarks(ctx, user.ChatID, user.Marks) // Need to update time anyway
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("Could not update database", zap.Error(err))
	}
	err = u.db.NewWeekMarksRepository().UpdateUserMarks(ctx, user.ChatID, user.Marks)
	if err != nil {
		logger.Error("could not update user marks", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}

	timeDifference := time.Since(timeStart)
	updateTimeHistogram.Observe(float64(timeDifference.Milliseconds()))
}

// UpdateUsersMarks updates all users' marks
func (u Updater) UpdateUsersMarks(ctx context.Context) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "UpdateUsersMarks")
	defer span.End()
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "update_users")))
	usersToUpdate, err := users.GetUsersThatNeedMarksUpdate(ctx, u.db.NewUserRepository(), int(u.UpdateInterval.Minutes()))
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("Could not get users than need update from db", zap.Error(err))
		return
	}
	span.SetAttributes(attribute.Int("users.count", len(usersToUpdate)))
	logger.Info("Got users to update", zap.Int("len", len(usersToUpdate)))
	var wg sync.WaitGroup
	for _, user := range usersToUpdate {
		wg.Add(1)
		go u.UpdateUser(ctx, &wg, user, logger)
	}
	logger.Info("Waiting for worker to finish")
	wg.Wait()
	logger.Info("Updated everyone")
}

// UpdateUsersPersonData updates all users person data
func (u Updater) UpdateUsersPersonData(ctx context.Context) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "UpdateUsersPersonData")
	defer span.End()
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "get_users_person_data")))

	usersToUpdate, err := users.GetUsersThatHaveNoPersonData(ctx, u.db.NewUserRepository())
	if err != nil {
		logger.Error("could not get usersToUpdate that have no person data", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	span.SetAttributes(attribute.Int("users.n", len(usersToUpdate)))
	var wg sync.WaitGroup
	for _, user := range usersToUpdate {
		wg.Add(1)
		go u.UpdateUserPersonData(ctx, &wg, user)
	}
	wg.Wait()
}

// UpdateUserPersonData gets user's person data
func (u Updater) UpdateUserPersonData(ctx context.Context, wg *sync.WaitGroup, user users.User) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "UpdateUserPersonData")
	defer span.End()
	defer wg.Done()

	span.SetAttributes(user.GetTraceAttributes()...)
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "get_users_person_data")))
	logger = logger.With(user.GetLoggerFields()...)

	uAPI, err := user.GetUserAPI(ctx, u.db.NewUserRepository(), baseURL, maxTries, time.Second, logger, false)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not get user api", zap.Error(err))
		return
	}
	personData, err := api.RetryGetPersonData(ctx, uAPI, 3, time.Second, logger)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not get person data", zap.Error(err))
		return
	}

	user.Class = personData.ClassYear
	user.School = personData.School
	user.FullName = personData.FullName

	err = (u.db.NewUserRepository()).UpdateUserPersonData(ctx, user.RepositoryUser())
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not update user person data")
	}
}

// SendDifferenceMessage sends difference message
func (u Updater) SendDifferenceMessage(ctx context.Context, old, new api.SummaryMarks, user users.User, logger *zap.Logger) {
	var span trace.Span
	ctx, span = otel.Tracer(tracerName).Start(ctx, "SendDifferenceMessage", trace.WithAttributes(user.GetTraceAttributes()...))
	defer span.End()

	useEmoji := user.GetUserSetting(users.SettingsUseEmoji)
	showDate := user.GetUserSetting(users.SettingsUpdatesShowMarkDate)
	showDescription := user.GetUserSetting(users.SettingsScheduleShowDescription)
	msgText, _ := api.GetDifferenceMessage(old, new, useEmoji, showDate, showDescription, logger)

	message := tgbotapi.NewMessage(user.ChatID, msgText)
	func() {
		var span trace.Span
		ctx, span = otel.Tracer(tracerName).Start(ctx, "SendMessage", trace.WithAttributes(attribute.String("text", message.Text)))
		defer span.End()

		if _, err := u.bot.Send(message); err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Error("could not send message", zap.Error(err))
		}
	}()
}
