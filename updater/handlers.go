package main

import (
	"encoding/json"
	"go.opentelemetry.io/otel"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

const tracerName = "updater"

// HandleIsAlive handles /isAlive request
func (u *Updater) HandleIsAlive(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("Yes, I am alive!"))
}

// HandleGetInterval handles getInterval request
func (u *Updater) HandleGetInterval(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "GetInterval")
	defer span.End()
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "get_interval")))

	wantedToken := u.config.SetIntervalToken
	token := r.Header.Get("Auth")
	if token != wantedToken {
		logger.Warn("Unauthorized request on get interval", zap.String("wanted_token", wantedToken), zap.String("actual_token", token))
		w.WriteHeader(http.StatusForbidden)
		_, _ = w.Write([]byte("Hey buddy, I think you got the wrong door. The leather-club is two blocks down."))
		return
	}

	logger.Info("everything is fine", zap.Duration("interval", u.UpdateInterval))
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(u.UpdateInterval.String()))
}

// HandleSetInterval handles set interval request
func (u *Updater) HandleSetInterval(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var span trace.Span
	_, span = otel.Tracer(tracerName).Start(ctx, "SetInterval")
	defer span.End()
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "set_interval")))

	wantedToken := u.config.SetIntervalToken
	token := r.Header.Get("Auth")
	if token != wantedToken {
		logger.Warn("Unauthorized request on get interval", zap.String("wanted_token", wantedToken), zap.String("actual_token", token))
		w.WriteHeader(http.StatusForbidden)
		_, _ = w.Write([]byte("Hey buddy, I think you got the wrong door. The leather-club is two blocks down."))
		return
	}

	vars := mux.Vars(r)
	newIntervalString := vars["val"] + "m"
	span.SetAttributes(attribute.String("interval.length", newIntervalString))
	interval, err := time.ParseDuration(newIntervalString)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not parse duration", zap.Error(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	u.UpdateInterval = interval
	err = u.db.NewConfigRepository().SetInterval(ctx, interval)
	if err != nil {
		logger.Error("could not update interval in DB", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	u.StartUpdatesTicker()
	logger.Info("Updated interval", zap.Duration("new_interval", interval))
	_, err = w.Write([]byte("Success!"))
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not write response", zap.Error(err))
	}
}

func (u Updater) HandleSendWeekReport(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	span := trace.SpanFromContext(ctx)
	span.SetName("SendWeekReport")
	defer span.End()
	logger := zap.New(u.loggerCore, zap.Fields(zap.String("event", "send_report")))
	logger.Info("got new request")

	token := r.Header.Get("Auth")
	var chatID int64
	err := json.NewDecoder(r.Body).Decode(&chatID)

	if token != u.config.SetIntervalToken {
		logger.Warn("bad token", zap.String("actual", token), zap.String("wanted", u.config.SetIntervalToken))
		http.Error(w, "bad token", http.StatusUnauthorized)
		return
	}

	if err != nil {
		logger.Error("bad chatID", zap.Error(err))
		http.Error(w, "bad chatID", http.StatusBadRequest)
		return
	}

	u.SendReportToUser(ctx, chatID, false)
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte("everything is good"))
}
