package main

import (
	"context"
	"fmt"
	"github.com/roylee0704/gron"
	"gitlab.com/bars-bot/bars-bot/config"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"time"
)

var startTime time.Time

func uptime() time.Duration {
	return time.Since(startTime)
}

func updateUptimeCounter() {
	uptimeGauge.Set(float64(uptime().Nanoseconds()))
}

func main() {
	uptimeGauge.Set(0)
	startTime = time.Now()
	uptimeTicker := gron.New()
	uptimeTicker.AddFunc(gron.Every(time.Second*1), updateUptimeCounter)
	uptimeTicker.Start()

	config.LoadConfigs()
	botConfig := config.NewBotConfig()
	ctx := context.Background()

	jaegerExporter, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(botConfig.JaegerEndpoint)))
	if err != nil {
		fmt.Println("Starting with no jaeger")
	}

	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("updater"),
			semconv.ServiceVersionKey.String(botConfig.Version),
			attribute.String("environment", botConfig.Environment),
		),
	)

	provider := trace.NewTracerProvider(
		trace.WithBatcher(jaegerExporter),
		trace.WithResource(r),
	)
	otel.SetTracerProvider(provider)

	updater := Updater{}
	updater.Init(ctx, botConfig, config.NewDatabaseConfig(), botConfig.LogFolder)
	updater.StartUpdatesTicker()
	if shouldSendWeekReports {
		updater.StartReportsTicker()
	}
	updater.StartServer(9000)
}
