package database

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/bars-bot/bars-bot/api"
	"time"
)

// Database is a wrapper class for database
type Database struct {
	db *sqlx.DB
}

// RepositoryUser is a class to store user data
type RepositoryUser struct {
	Login               string `db:"login"`
	Password            string `db:"password"`
	SessionID           string `db:"sessionid"`
	ChatID              int64  `db:"chatid"`
	Settings            int    `db:"settings"`
	SessionIDUpdateTime time.Time
	Marks               api.SummaryMarks
	MarksUpdateTime     time.Time
	School              string
	Class               string
	FullName            string
}

// RepositoryNews is a class to store news data
type RepositoryNews struct {
	Author int64
	Text   string
	Date   time.Time
}
