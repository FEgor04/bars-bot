package database

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"time"
)

// HomeworksRepository is a db wrapper for Homework methods
type HomeworksRepository struct {
	db *sqlx.DB
}

// Homework is a class to store homework data
type Homework struct {
	ID      int
	Author  int64
	Text    string
	Date    time.Time
	Subject string
	Files   HomeworkFiles
}

// HomeworkFile is a class to store homework files
type HomeworkFile struct {
	FileID     string
	IsDocument bool
}

// HomeworkFiles is an array of homework files
type HomeworkFiles []HomeworkFile

// GetImages gets all images from
func (f HomeworkFiles) GetImages() HomeworkFiles {
	var answ HomeworkFiles
	for i := range f {
		if !f[i].IsDocument {
			answ = append(answ, f[i])
		}
	}
	return answ
}

// GetDocuments gets all documents
func (f HomeworkFiles) GetDocuments() HomeworkFiles {
	var answ HomeworkFiles
	for i := range f {
		if f[i].IsDocument {
			answ = append(answ, f[i])
		}
	}
	return answ
}

// NewHomeworksRepository creates new homework repository from Database object
func (d *Database) NewHomeworksRepository() HomeworksRepository {
	return HomeworksRepository{db: d.db}
}

// InsertHomework inserts homework and its files into database
func (h HomeworksRepository) InsertHomework(ctx context.Context, hw Homework) error {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "InsertHomework")
	defer span.End()
	span.SetAttributes(
		attribute.Int64("author.id", hw.Author),
		attribute.String("hw.text", hw.Text),
		attribute.String("hw.date", hw.Date.String()),
		attribute.String("hw.subject", hw.Subject),
	)

	tx, err := h.db.BeginTxx(ctx, nil)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}

	rows, err := tx.QueryxContext(ctx, "INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id", hw.Author, hw.Text, hw.Date, hw.Subject)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		errN := tx.Rollback()
		if errN != nil {
			span.RecordError(errN)
		}
		return err
	}
	defer rows.Close()

	var id int
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		}
	}

	for _, file := range hw.Files {
		span.AddEvent("inserting file", trace.WithAttributes(attribute.Int("hw.id", id), attribute.String("file.id", file.FileID)))
		if file.IsDocument {
			_, err := tx.ExecContext(ctx, "INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)", id, file.FileID)
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				rollbackErr := tx.Rollback()
				if rollbackErr != nil {
					span.RecordError(rollbackErr)
				}
				return err
			}
		} else {
			_, err := tx.ExecContext(ctx, "INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)", id, file.FileID)
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				rollbackErr := tx.Rollback()
				if rollbackErr != nil {
					span.RecordError(rollbackErr)
				}
				return err
			}
		}
	}
	return tx.Commit()
}

// GetHomeworks gets homework for desired school, class and date
func (h HomeworksRepository) GetHomeworks(ctx context.Context, date time.Time, school, class string) ([]Homework, error) {
	date = time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, date.Location())

	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "GetHomeworks")
	defer span.End()
	span.SetAttributes(
		attribute.String("school", school),
		attribute.String("class", class),
		attribute.String("date", date.Format("2006-02-01")),
	)

	var answ []Homework

	rows, err := h.db.QueryContext(
		ctx,
		"SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3",
		date,
		school,
		class,
	)

	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}
	var now Homework
	var last Homework

	last.ID = -1

	for rows.Next() {
		now.Date = date

		var imgFileID sql.NullString
		var docFileID sql.NullString

		rErr := rows.Scan(&now.ID, &now.Subject, &now.Text, &now.Author, &imgFileID, &docFileID)
		if rErr != nil {
			span.RecordError(rErr)
			continue
		}

		var file HomeworkFile

		if imgFileID.Valid {
			span.AddEvent("got image", trace.WithAttributes(attribute.String("fileid", imgFileID.String)))
			file.FileID = imgFileID.String
			file.IsDocument = false
			now.Files = append(now.Files, file)
		}
		if docFileID.Valid {
			span.AddEvent("got file", trace.WithAttributes(attribute.String("fileid", imgFileID.String)))
			file.FileID = docFileID.String
			file.IsDocument = true
			now.Files = append(now.Files, file)
		}

		if now.ID == last.ID {
			last.Files = now.Files
		} else {
			if last.ID != -1 {
				span.AddEvent("added new", trace.WithAttributes(
					attribute.Int("id", last.ID),
					attribute.Int64("author", last.Author),
					attribute.String("subject", last.Subject),
					attribute.String("text", last.Text),
					attribute.Int("files.cnt", len(last.Files)),
				))
				answ = append(answ, last)
			}
			if imgFileID.Valid || docFileID.Valid {
				now.Files = []HomeworkFile{file}
			}
			last = now
		}
	}
	if last.ID != -1 {
		answ = append(answ, last)
	} else {
		span.AddEvent("no homework")
	}

	return answ, nil
}
