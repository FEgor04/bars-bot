package database

import (
  "context"

  "github.com/jmoiron/sqlx"
  "gitlab.com/bars-bot/bars-bot/api"
  "go.opentelemetry.io/otel"
  "go.opentelemetry.io/otel/attribute"
  "go.opentelemetry.io/otel/trace"
)

// UserRepository is a wrapper class for User methods
type UserRepository struct {
  db *sqlx.DB
}

// InsertUser inserts user in the database
func (u UserRepository) InsertUser(ctx context.Context, user RepositoryUser) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "InsertUser")
  defer span.End()

  span.SetAttributes(
    attribute.Int64("user.id", user.ChatID),
    attribute.String("user.login", user.Login),
    attribute.String("user.school", user.School),
    attribute.String("user.class", user.Class),
  )

  user.Marks.Sort()
  _, err := u.db.ExecContext(ctx,
    "INSERT INTO users (chatid, login, password, sessionid, settings, sessionid_update_time, marks, marks_update_time, school, class) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
    user.ChatID,
    user.Login,
    user.Password,
    user.SessionID,
    user.Settings,
    user.SessionIDUpdateTime,
    user.Marks,
    user.MarksUpdateTime,
    user.School,
    user.Class,
  )
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// NewUserRepository creates new UserRepository from Database repository
func (d *Database) NewUserRepository() *UserRepository {
  return &UserRepository{db: d.db}
}

// GetUserByChatID gets user by ChatID
func (u UserRepository) GetUserByChatID(ctx context.Context, chatID int64) (RepositoryUser, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserByChatID")
  span.SetAttributes(attribute.Int64("user.id", chatID))
  defer span.End()

  var val RepositoryUser
  rows, err := u.db.QueryContext(ctx, "SELECT * FROM users WHERE chatid = $1", chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return val, err
  }
  for rows.Next() {
    errNow := rows.Scan(
      &val.ChatID,
      &val.Login,
      &val.Password,
      &val.SessionID,
      &val.Settings,
      &val.SessionIDUpdateTime,
      &val.Marks,
      &val.MarksUpdateTime,
      &val.School,
      &val.Class,
      &val.FullName,
    )
    if errNow != nil {
      span.RecordError(errNow)
    }
    if errNow == nil {
      val.Marks.Sort()
    }
  }
  return val, nil
}

// GetUsersThanNeedMarksUpdate gets user that need marks update
func (u UserRepository) GetUsersThanNeedMarksUpdate(ctx context.Context, interval int) ([]RepositoryUser, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUsersThatNeedUpdate")
  defer span.End()
  var val []RepositoryUser
  rows, err := u.db.QueryContext(ctx, "SELECT * FROM users where marks_update_time < now() - make_interval(mins => $1) + make_interval(secs => 10) or marks_update_time is null or marks is null;", interval)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return nil, err
  }
  for rows.Next() {
    uNow := RepositoryUser{}
    err := rows.Scan(
      &uNow.ChatID,
      &uNow.Login,
      &uNow.Password,
      &uNow.SessionID,
      &uNow.Settings,
      &uNow.SessionIDUpdateTime,
      &uNow.Marks,
      &uNow.MarksUpdateTime,
      &uNow.School,
      &uNow.Class,
      &uNow.FullName,
    )
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    } else {
      uNow.Marks.Sort()
    }
    val = append(val, uNow)
  }
  span.SetAttributes(attribute.Int("users.count", len(val)))
  return val, err
}

// UpdateUserMarks update user's marks
func (u UserRepository) UpdateUserMarks(ctx context.Context, chatID int64, marks api.SummaryMarks) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateUserMarks")
  defer span.End()

  marks.Sort()

  span.SetAttributes(
    attribute.Int64("user.id", chatID),
  )

  _, err := u.db.ExecContext(ctx, "UPDATE users SET marks = $1, marks_update_time = now() WHERE chatid = $2", marks, chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUsersNumber get total number of users in database
func (u UserRepository) GetUsersNumber(ctx context.Context) (int, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUsersNumber")
  defer span.End()

  row := u.db.QueryRowContext(ctx, "SELECT COUNT(chatid) FROM users")
  var answer int
  err := row.Scan(&answer)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return 1, err
  }
  return answer, nil
}

// IsUserAdmin checks is user admin
func (u UserRepository) IsUserAdmin(ctx context.Context, chatID int64) bool {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "IsUserAdmin")
  defer span.End()

  span.SetAttributes(attribute.Int64("user.id", chatID))

  row := u.db.QueryRowContext(ctx, "SELECT COUNT(chatid) FROM admins WHERE chatid = $1", chatID)
  var cnt int
  err := row.Scan(&cnt)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return false
  }
  return cnt >= 1
}

// GetAllUsersChatID getes all users' ChatID
func (u UserRepository) GetAllUsersChatID(ctx context.Context) ([]int64, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAllUsersChatID")
  defer span.End()

  var answ []int64
  rows, err := u.db.QueryContext(ctx, "SELECT chatid FROM users")
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return nil, err
  }
  for rows.Next() {
    var valNow int64
    err = rows.Scan(&valNow)
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    } else {
      answ = append(answ, valNow)
    }
  }
  return answ, nil
}

// UpdateSettings updates user's settings
func (u UserRepository) UpdateSettings(ctx context.Context, chatID int64, bitID int) (int, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateSettings")
  defer span.End()

  span.SetAttributes(attribute.Int64("user.id", chatID))
  span.SetAttributes(attribute.Int("user.bitid", bitID))

  rows, err := u.db.QueryContext(ctx, "UPDATE users SET settings = settings # (1 << $1) WHERE chatid = $2 RETURNING settings", bitID, chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return 0, err
  }
  var answer int
  for rows.Next() {
    err = rows.Scan(&answer)
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
      return 0, err
    }
  }
  return answer, nil
}

// UpdateSessionID updates user's SessionID
func (u UserRepository) UpdateSessionID(ctx context.Context, chatID int64, sessionID string) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateSessionID")
  defer span.End()

  span.SetAttributes(attribute.Int64("user.id", chatID))

  _, err := u.db.ExecContext(ctx, "UPDATE users SET sessionid = $1, sessionid_update_time = now() WHERE chatid = $2", sessionID, chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// GetUserMarks gets user's marks
func (u UserRepository) GetUserMarks(ctx context.Context, chatID int64) (api.SummaryMarks, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserMarksFromDatabase")
  defer span.End()

  span.SetAttributes(attribute.Int64("user.id", chatID))

  var answ api.SummaryMarks
  err := u.db.GetContext(ctx, &answ, "SELECT marks FROM users WHERE chatid = $1", chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return api.SummaryMarks{}, err
  } else {
    answ.Sort()
  }
  return answ, err
}

// GetUserSettings get user's settings
func (u UserRepository) GetUserSettings(ctx context.Context, chatID int64) (int, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUserSettings")
  defer span.End()
  span.SetAttributes(attribute.Int64("user.id", chatID))
  var answ int
  err := u.db.GetContext(ctx, &answ, "SELECT settings FROM users WHERE chatid = $1", chatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return answ, err
}

// GetAdmins get admins' ChatID
func (u UserRepository) GetAdmins(ctx context.Context) ([]int64, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetAdmins")
  defer span.End()

  var answ []int64
  rows, err := u.db.QueryContext(ctx, "SELECT chatid FROM admins")
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  for rows.Next() {
    var now int64
    err = rows.Scan(&now)
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    } else {
      answ = append(answ, now)
    }
  }
  span.SetAttributes(attribute.Int("users.count", len(answ)))
  return answ, err
}

// GetUsersThatHaveNoPersonData get users with no person data
func (u UserRepository) GetUsersThatHaveNoPersonData(ctx context.Context) ([]RepositoryUser, error) {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "GetUsersThatHaveNoPersonData")
  defer span.End()
  var val []RepositoryUser
  rows, err := u.db.QueryContext(ctx, "SELECT * FROM users WHERE (school = '') IS NOT FALSE OR (class = '') IS NOT FALSE OR (fullname = '') IS NOT FALSE")
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    return nil, err
  }
  for rows.Next() {
    uNow := RepositoryUser{}
    err := rows.Scan(
      &uNow.ChatID,
      &uNow.Login,
      &uNow.Password,
      &uNow.SessionID,
      &uNow.Settings,
      &uNow.SessionIDUpdateTime,
      &uNow.Marks,
      &uNow.MarksUpdateTime,
      &uNow.School,
      &uNow.Class,
      &uNow.FullName,
    )
    if err != nil {
      span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
    } else {
      uNow.Marks.Sort()
    }
    val = append(val, uNow)
  }
  span.SetAttributes(attribute.Int("users.count", len(val)))
  return val, err
}

// UpdateUserPersonData updates user's person data
func (u UserRepository) UpdateUserPersonData(ctx context.Context, user RepositoryUser) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateUserPersonData")
  defer span.End()

  span.SetAttributes(
    attribute.Int64("user.id", user.ChatID),
    attribute.String("user.login", user.Login),
    attribute.String("user.school", user.School),
    attribute.String("user.class", user.Class),
  )

  _, err := u.db.ExecContext(ctx, "UPDATE users SET school = $1, class = $2, fullname = $3 WHERE chatid = $4", user.School, user.Class, user.FullName, user.ChatID)
  if err != nil {
    span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
  }
  return err
}

// DeleteUser deletes everything connected with this user
func (u UserRepository) DeleteUser(ctx context.Context, chatID int64) error {
  var span trace.Span
  ctx, span = otel.Tracer(TracerName).Start(ctx, "DeleteUser")
  defer span.End()

  span.SetAttributes(
    attribute.Int64("user.id", chatID),
  )

  tx, err := u.db.BeginTx(ctx, nil)
  if err != nil {
    span.RecordError(err)
    return err
  }
  _, err = tx.ExecContext(ctx, "DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)", chatID)
  if err != nil {
    span.RecordError(err)
    span.RecordError(tx.Rollback())
    return err
  }
  _, err = tx.ExecContext(ctx, "DELETE FROM homeworks_documents WHERE hwid in (select id from homeworks where author = $1)", chatID)
  if err != nil {
    span.RecordError(err)
    span.RecordError(tx.Rollback())
    return err
  }
  _, err = tx.ExecContext(ctx, "DELETE FROM homeworks WHERE author = $1", chatID)
  if err != nil {
    span.RecordError(err)
    span.RecordError(tx.Rollback())
    return err
  }
  _, err = tx.ExecContext(ctx, "DELETE FROM users WHERE chatid = $1", chatID)
  if err != nil {
    span.RecordError(err)
    span.RecordError(tx.Rollback())
    return err
  }
  err = tx.Commit()
  span.RecordError(err)
  return err
}
