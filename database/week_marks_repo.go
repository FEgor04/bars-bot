package database

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/bars-bot/bars-bot/api"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type WeekMarksRepository struct {
	db *sqlx.DB
}

type Mark struct {
	Mark       api.Mark  `db:"mark"`
	StudentID  int64     `db:"student"`
	Discipline string    `db:"discipline"`
	TimeSet    time.Time `db:"time_set"`
}

type DisciplineMark struct {
	Discipline string
}

func (d Database) NewWeekMarksRepository() WeekMarksRepository {
	return WeekMarksRepository(d)
}

func (w WeekMarksRepository) UpdateUserMarks(ctx context.Context, userID int64, userMarks api.SummaryMarks) error {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateUserWeekMarks", trace.WithAttributes(attribute.Int64("id", userID)))
	defer span.End()

	_, err := w.db.ExecContext(ctx, "INSERT INTO day_marks(day_number, student_id, marks) VALUES(DATE_PART('doy', now()), $1, $2) ON CONFLICT (student_id, day_number) DO UPDATE SET marks = $2", userID, userMarks)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}
	return nil
}

func (w WeekMarksRepository) GetCurrentWeekMarks(ctx context.Context, studentID int64) (api.SummaryMarks, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "GetCurrentWeekMarks", trace.WithAttributes(attribute.Int64("id", studentID)))
	defer span.End()

	var marks api.SummaryMarks
	err := w.db.QueryRowxContext(ctx, `SELECT marks FROM day_marks WHERE student_id = $1 AND day_number = (SELECT max(day_number) FROM day_marks WHERE student_id = $1 AND day_number <= DATE_PART('doy', now()) - (DATE_PART('dow', now() - '1 days'::interval)::int + 1))`, studentID).Scan(&marks)
	if err != nil {
		span.RecordError(err)
	}
	return marks, err
}
