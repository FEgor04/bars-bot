//go:build unit || all
// +build unit all

package database

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sqlmock "github.com/zhashkevych/go-sqlxmock"
	"gitlab.com/bars-bot/bars-bot/api"
	"testing"
)

var ctx = context.Background()

func TestWeekMarksRepository_GetCurrentWeekMarks(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewWeekMarksRepository()
	studentID := int64(1)
	expected := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates:           []string{"test"},
		DisciplineMarks: nil,
	}
	expected.Push("Test", api.Mark{Mark: "5"})
	expected.Push("Test 2", api.Mark{Mark: "3"})
	expected.Push("Test 3", api.Mark{Mark: "1"})

	mock.ExpectQuery("SELECT marks FROM day_marks WHERE student_id = $1 AND day_number = (SELECT max(day_number) FROM day_marks WHERE student_id = $1 AND day_number <= DATE_PART('doy', now()) - (DATE_PART('dow', now() - '1 days'::interval)::int + 1))").
		WithArgs(studentID).
		WillReturnRows(sqlmock.NewRows([]string{"marks"}).AddRow(expected))

	actual, err := repo.GetCurrentWeekMarks(ctx, studentID)
	require.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestWeekMarksRepository_GetCurrentWeekMarks_Error(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewWeekMarksRepository()

	studentID := int64(1)
	wantedErr := errors.New("test")

	mock.ExpectQuery("SELECT marks FROM day_marks WHERE student_id = $1 AND day_number = (SELECT max(day_number) FROM day_marks WHERE student_id = $1 AND day_number <= DATE_PART('doy', now()) - (DATE_PART('dow', now() - '1 days'::interval)::int + 1))").
		WithArgs(studentID).
		WillReturnError(wantedErr)

	actual, err := repo.GetCurrentWeekMarks(ctx, studentID)
	assert.Equal(t, wantedErr, err)
	assert.Equal(t, api.SummaryMarks{}, actual)
}

func TestWeekMarksRepository_UpdateUserMarks(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewWeekMarksRepository()

	studentID := int64(1)
	marks := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates:           []string{"test"},
		DisciplineMarks: nil,
	}
	marks.Push("Test", api.Mark{Mark: "5"})
	marks.Push("Test 2", api.Mark{Mark: "3"})
	marks.Push("Test 3", api.Mark{Mark: "1"})

	mock.ExpectExec("INSERT INTO day_marks(day_number, student_id, marks) VALUES(DATE_PART('doy', now()), $1, $2) ON CONFLICT (student_id, day_number) DO UPDATE SET marks = $2").
		WithArgs(studentID, marks).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.UpdateUserMarks(ctx, studentID, marks)
	assert.NoError(t, err)
}

func TestWeekMarksRepository_UpdateUserMarks_Error(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewWeekMarksRepository()

	studentID := int64(1)
	marks := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "test",
		},
		Dates:           []string{"test"},
		DisciplineMarks: nil,
	}
	wantedErr := errors.New("err")
	marks.Push("Test", api.Mark{Mark: "5"})
	marks.Push("Test 2", api.Mark{Mark: "3"})
	marks.Push("Test 3", api.Mark{Mark: "1"})

	mock.ExpectExec("INSERT INTO day_marks(day_number, student_id, marks) VALUES(DATE_PART('doy', now()), $1, $2) ON CONFLICT (student_id, day_number) DO UPDATE SET marks = $2").
		WithArgs(studentID, marks).
		WillReturnError(wantedErr)

	err = repo.UpdateUserMarks(ctx, studentID, marks)
	assert.Equal(t, wantedErr, err)
}
