package database

import (
	"context"

	"github.com/golang-migrate/migrate/v4"
	// Postgres database for migrations
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	// File source for migrations
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// WantedDatabaseVersion is a database that current version wants
const WantedDatabaseVersion = 20220303055131

// TracerName is a name for jaeger tracer
const TracerName = "database"

// New creates new database
func New(db *sqlx.DB) *Database {
	return &Database{db: db}
}

// Connect connects to database and pings it
func (d *Database) Connect(ctx context.Context, psqlconn string) error {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "Connect")
	defer span.End()
	db, err := sqlx.ConnectContext(ctx, "postgres", psqlconn)
	if err != nil {
		return err
	}
	d.db = db
	stats := d.db.Stats()
	span.SetAttributes(attribute.Int("database.open_connections", stats.OpenConnections))
	return nil
}

// Close closes database connection
func (d *Database) Close(ctx context.Context) {
	var span trace.Span
	_, span = otel.Tracer(TracerName).Start(ctx, "Close")
	defer span.End()

	err := d.db.Close()
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
}

// Migrate migrates database to WantedDatabaseVersion
func (d *Database) Migrate(ctx context.Context, psqlconn string, migrationsFolder string, logger *zap.Logger) error {
	var span trace.Span
	_, span = otel.Tracer(TracerName).Start(ctx, "Migrate")
	defer span.End()

	logger.Info("Connecting to database")
	m, err := migrate.New("file://"+migrationsFolder, psqlconn)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		logger.Error("could not connect to database", zap.Error(err))
		return err
	}
	logger.Info("connected to database. getting version")

	version, isDirty, err := m.Version()
	if err != nil {
		logger.Error("could not get version", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	logger.Info("got database version", zap.Int64("version", int64(version)))
	if err == migrate.ErrNilVersion {
		err := m.Up()
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
			logger.Info("could not update database", zap.Error(err))
			return err
		}
		return nil
	} else if err != nil {
		logger.Error("cgot error while getting database version", zap.Error(err))
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return err
	}
	if version != WantedDatabaseVersion {
		logger.Warn("Bad database version. Updating")
		if isDirty {
			logger.Warn("FORCE MIGRATING! DATABASE IS DIRTY")
			err := m.Force(WantedDatabaseVersion)
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				logger.Error("could not migrate", zap.Error(err))
			}
		} else {
			err := m.Migrate(WantedDatabaseVersion)
			if err != nil {
				span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
				logger.Info("could not update database", zap.Error(err))
				return err
			}
		}
	}
	return nil
}
