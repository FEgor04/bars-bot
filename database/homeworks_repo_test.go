//go:build all || unit
// +build all unit

package database

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sqlmock "github.com/zhashkevych/go-sqlxmock"
	"testing"
	"time"
)

func TestHomeworksRepository_InsertHomeworkTextOnly(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files:   nil,
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkTextOnlyError(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	errorToReturn := fmt.Errorf("not today")

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files:   nil,
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnError(errorToReturn)
	mock.ExpectRollback()

	err = repo.InsertHomework(ctx, hw)
	assert.Equal(t, errorToReturn, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkOneImage(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{{
			FileID:     "fileId",
			IsDocument: false,
		}},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkThreeImages(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{
			{
				FileID:     "fileId1",
				IsDocument: false,
			},
			{
				FileID:     "fileId2",
				IsDocument: false,
			},
			{
				FileID:     "fileId3",
				IsDocument: false,
			},
		},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[1].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[2].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkOneDoc(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{{
			FileID:     "fileId",
			IsDocument: true,
		}},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkThreeDocs(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{
			{
				FileID:     "fileId1",
				IsDocument: true,
			},
			{
				FileID:     "fileId2",
				IsDocument: true,
			},
			{
				FileID:     "fileId3",
				IsDocument: true,
			},
		},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[1].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[2].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkTwoImagesTwoDocs(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{
			{
				FileID:     "fileId1",
				IsDocument: false,
			},
			{
				FileID:     "fileId2",
				IsDocument: false,
			},
			{
				FileID:     "fileId3",
				IsDocument: true,
			},
			{
				FileID:     "fileId4",
				IsDocument: true,
			},
		},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[1].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[2].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[3].FileID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.InsertHomework(ctx, hw)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkOneDocError(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	errToReturn := fmt.Errorf("not today")

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{{
			FileID:     "fileId",
			IsDocument: true,
		}},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_documents(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnError(errToReturn)
	mock.ExpectRollback()

	err = repo.InsertHomework(ctx, hw)
	assert.Equal(t, errToReturn, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_InsertHomeworkOneImageError(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	errToReturn := fmt.Errorf("not today")

	hw := Homework{
		Author:  123456,
		Text:    "test",
		Date:    time.Now(),
		Subject: "test subject",
		Files: []HomeworkFile{{
			FileID:     "fileId",
			IsDocument: false,
		}},
	}

	mock.ExpectBegin()
	mock.ExpectQuery("INSERT INTO homeworks(author, text, date, subject) VALUES ($1, $2, $3, $4) RETURNING id").
		WithArgs(hw.Author, hw.Text, hw.Date, hw.Subject).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	mock.ExpectExec("INSERT INTO homeworks_images(hwid, fileid) VALUES ($1, $2)").
		WithArgs(1, hw.Files[0].FileID).
		WillReturnError(errToReturn)
	mock.ExpectRollback()

	err = repo.InsertHomework(ctx, hw)
	assert.Equal(t, errToReturn, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_OneImage(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{{
				FileID:     "fileId",
				IsDocument: false,
			}},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[0].FileID,
					nil,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_ThreeImages(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{
				{
					FileID:     "fileId",
					IsDocument: false,
				},
				{
					FileID:     "fileId1",
					IsDocument: false,
				},
				{
					FileID:     "fileId2",
					IsDocument: false,
				},
			},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[0].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[1].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[2].FileID,
					nil,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_OneDoc(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{{
				FileID:     "fileId",
				IsDocument: true,
			}},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[0].FileID,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_ThreeDocs(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{
				{
					FileID:     "fileId",
					IsDocument: true,
				},
				{
					FileID:     "fileId1",
					IsDocument: true,
				},
				{
					FileID:     "fileId2",
					IsDocument: true,
				},
			},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[0].FileID,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[1].FileID,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[2].FileID,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_TwoImages_TwoDocs(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{
				{
					FileID:     "fileId",
					IsDocument: false,
				},
				{
					FileID:     "fileId1",
					IsDocument: false,
				},
				{
					FileID:     "fileId",
					IsDocument: true,
				},
				{
					FileID:     "fileId1",
					IsDocument: true,
				},
			},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[0].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[1].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[2].FileID,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[3].FileID,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_TwoHomeworks_TwoImages_TwoDocs(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{
				{
					FileID:     "fileId",
					IsDocument: false,
				},
				{
					FileID:     "fileId1",
					IsDocument: false,
				},
				{
					FileID:     "fileId",
					IsDocument: true,
				},
				{
					FileID:     "fileId1",
					IsDocument: true,
				},
			},
		},
		{
			ID:      2,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files: []HomeworkFile{
				{
					FileID:     "fileId",
					IsDocument: false,
				},
				{
					FileID:     "fileId1",
					IsDocument: false,
				},
				{
					FileID:     "fileId",
					IsDocument: true,
				},
				{
					FileID:     "fileId1",
					IsDocument: true,
				},
			},
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[0].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					expected[0].Files[1].FileID,
					nil,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[2].FileID,
				).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					expected[0].Files[3].FileID,
				).
				AddRow(
					expected[1].ID,
					expected[1].Subject,
					expected[1].Text,
					expected[1].Author,
					expected[1].Files[0].FileID,
					nil,
				).
				AddRow(
					expected[1].ID,
					expected[1].Subject,
					expected[1].Text,
					expected[1].Author,
					expected[1].Files[1].FileID,
					nil,
				).
				AddRow(
					expected[1].ID,
					expected[1].Subject,
					expected[1].Text,
					expected[1].Author,
					nil,
					expected[1].Files[2].FileID,
				).
				AddRow(
					expected[1].ID,
					expected[1].Subject,
					expected[1].Text,
					expected[1].Author,
					nil,
					expected[1].Files[3].FileID,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_NoHomework(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					nil,
					nil,
					nil,
					nil,
					nil,
					nil,
				),
		)

	actual, err := repo.GetHomeworks(ctx, date, school, class)
	assert.NoError(t, err)
	assert.Nil(t, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestHomeworksRepository_GetHomeworks_OneHomework_NoFiles(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()
	ctx := context.Background()
	repo := HomeworksRepository{db: db}
	date := time.Date(2021, time.August, 5, 0, 0, 0, 0, time.Local)

	expected := []Homework{
		{
			ID:      1,
			Author:  123456,
			Text:    "test",
			Date:    date,
			Subject: "test subject",
			Files:   nil,
		},
	}
	school := "Test School"
	class := "Test Class"

	mock.ExpectQuery("SELECT id, subject, text, author, i.fileid, d.fileid FROM homeworks LEFT JOIN homeworks_documents as d ON d.hwid = id LEFT JOIN homeworks_images as i on i.hwid = id LEFT JOIN users as u on u.chatid = author WHERE date = $1 and u.school = $2 and u.class = $3").
		WithArgs(expected[0].Date, school, class).
		WillReturnRows(
			sqlmock.NewRows([]string{"id", "subject", "text", "author", "i.fileid", "d.fileid"}).
				AddRow(
					expected[0].ID,
					expected[0].Subject,
					expected[0].Text,
					expected[0].Author,
					nil,
					nil,
				),
		)

	actual, err := repo.GetHomeworks(ctx, expected[0].Date, school, class)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}
