//go:build unit || all
// +build unit all

package database

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/zhashkevych/go-sqlxmock"
	"gitlab.com/bars-bot/bars-bot/api"
	"testing"
	"time"
)

func TestUserRepository_InsertUser(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	newUser := RepositoryUser{
		Login:               "Login",
		Password:            "Password",
		SessionID:           "SessionID",
		ChatID:              4,
		Settings:            5,
		SessionIDUpdateTime: time.Now(),
		MarksUpdateTime:     time.Now(),
		Marks:               api.SummaryMarks{},
		School:              "МБОУ \"КЕК ЛОЛ\"",
		Class:               "11 Мемный",
	}
	mock.ExpectExec("INSERT INTO users (chatid, login, password, sessionid, settings, sessionid_update_time, marks, marks_update_time, school, class) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)").
		WithArgs(newUser.ChatID, newUser.Login, newUser.Password, newUser.SessionID, newUser.Settings, newUser.SessionIDUpdateTime, newUser.Marks, newUser.MarksUpdateTime, newUser.School, newUser.Class).
		WillReturnResult(sqlmock.NewResult(1, 1))

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	err = repo.InsertUser(context.Background(), newUser)
	assert.NoError(t, err)

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUserByChatId(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	newUser := RepositoryUser{
		Login:               "Login",
		Password:            "Password",
		SessionID:           "SessionID",
		ChatID:              4,
		Settings:            5,
		SessionIDUpdateTime: time.Now(),
		MarksUpdateTime:     time.Now(),
		Marks: api.SummaryMarks{
			Subperiod: api.MarksSubperiod{
				Code: "Test",
				Name: "Test",
			},
			Dates: []string{"Test", "Test"},
			DisciplineMarks: []api.DisciplineMark{{
				Discipline: "Test",
				Marks: []api.Mark{{
					Date:        "Test",
					Description: "Test",
					Mark:        "5",
				}},
				AverageMark: "5",
			}},
		},
		School:   "Test school",
		Class:    "Test calss",
		FullName: "Test name",
	}

	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time", "school", "class", "fullname"}
	expectedRows := sqlmock.NewRows(columns).
		AddRow(
			newUser.ChatID,
			newUser.Login,
			newUser.Password,
			newUser.SessionID,
			newUser.Settings,
			newUser.SessionIDUpdateTime,
			newUser.Marks,
			newUser.MarksUpdateTime,
			newUser.School,
			newUser.Class,
			newUser.FullName,
		)

	mock.ExpectQuery("SELECT * FROM users WHERE chatid = $1").
		WithArgs(newUser.ChatID).
		WillReturnRows(expectedRows)

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	_, err = repo.GetUserByChatID(context.Background(), newUser.ChatID)
	assert.NoError(t, err)
	//assert.Equal(t, newUser, actualUser)
	//
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUsersThanNeedMarksUpdate(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	newUser := RepositoryUser{
		Login:               "Login",
		Password:            "Password",
		SessionID:           "SessionID",
		ChatID:              4,
		Settings:            5,
		SessionIDUpdateTime: time.Now(),
		MarksUpdateTime:     time.Now(),
		Marks: api.SummaryMarks{
			Subperiod: api.MarksSubperiod{
				Code: "Test",
				Name: "Test",
			},
			Dates: []string{"Test", "Test"},
			DisciplineMarks: []api.DisciplineMark{{
				Discipline: "Test",
				Marks: []api.Mark{{
					Date:        "Test",
					Description: "Test",
					Mark:        "5",
				}},
				AverageMark: "5",
			}},
		},
	}

	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time"}
	expectedRows := sqlmock.NewRows(columns).
		AddRow(
			newUser.ChatID,
			newUser.Login,
			newUser.Password,
			newUser.SessionID,
			newUser.Settings,
			newUser.SessionIDUpdateTime,
			newUser.Marks,
			newUser.MarksUpdateTime,
		).
		AddRow(
			newUser.ChatID,
			newUser.Login,
			newUser.Password,
			newUser.SessionID,
			newUser.Settings,
			newUser.SessionIDUpdateTime,
			newUser.Marks,
			newUser.MarksUpdateTime,
		)
	interval := 5
	mock.ExpectQuery("SELECT * FROM users where marks_update_time < now() - make_interval(mins => $1) + make_interval(secs => 10) or marks_update_time is null or marks is null;").
		WithArgs(interval).
		WillReturnRows(expectedRows)

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	kek, err := repo.GetUsersThanNeedMarksUpdate(context.Background(), interval)
	assert.NoError(t, err)
	//assert.Equal(t, newUser, actualUser)
	//
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, 2, len(kek))
}

func TestUserRepository_UpdateUserMarks(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	var updatedID int64 = 42
	updatedMarks := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "Test",
		},
		Dates: []string{"Test", "Test"},
		DisciplineMarks: []api.DisciplineMark{{
			Discipline: "Test",
			Marks: []api.Mark{{
				Date:        "Test",
				Description: "Test",
				Mark:        "5",
			}},
			AverageMark: "5",
		}},
	}

	mock.ExpectExec("UPDATE users SET marks = $1, marks_update_time = now() WHERE chatid = $2").
		WithArgs(updatedMarks, updatedID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	err = userRepo.UpdateUserMarks(context.Background(), updatedID, updatedMarks)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUsersCnt(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	expected := 10
	mock.ExpectQuery("SELECT COUNT(chatid) FROM users").
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(expected))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	actual, err := userRepo.GetUsersNumber(context.Background())
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestUserRepository_IsUserAdmin(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	expected := true
	var wantedUser int64 = 1234
	mock.ExpectQuery("SELECT COUNT(chatid) FROM admins WHERE chatid = $1").
		WithArgs(wantedUser).
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(1))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	actual := userRepo.IsUserAdmin(context.Background(), wantedUser)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestUserRepository_GetAllUsersChatId(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	mock.ExpectQuery("SELECT chatid FROM users").
		WillReturnRows(sqlmock.NewRows([]string{"chatid"}).AddRow(1).AddRow(2).AddRow(3))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	actual, err := userRepo.GetAllUsersChatID(context.Background())
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, []int64{1, 2, 3}, actual)
}

func TestUserRepository_UpdateSettings(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	mock.ExpectQuery("UPDATE users SET settings = settings # (1 << $1) WHERE chatid = $2 RETURNING settings").
		WithArgs(1, 1).
		WillReturnRows(sqlmock.NewRows([]string{"settings"}).AddRow(2))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	actual, err := userRepo.UpdateSettings(context.Background(), 1, 1)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, 2, actual)
}

func TestUserRepository_UpdateSessionId(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	mock.ExpectExec("UPDATE users SET sessionid = $1, sessionid_update_time = now() WHERE chatid = $2").
		WithArgs("hellothere", 2).
		WillReturnResult(sqlmock.NewResult(1, 1))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	err = userRepo.UpdateSessionID(context.Background(), 2, "hellothere")
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUserMarks(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	expected := api.SummaryMarks{
		Subperiod: api.MarksSubperiod{
			Code: "Test",
			Name: "Test",
		},
		Dates: []string{"Test", "Test"},
		DisciplineMarks: []api.DisciplineMark{{
			Discipline: "Test",
			Marks: []api.Mark{{
				Date:        "Test",
				Description: "Test",
				Mark:        "5",
			}},
			AverageMark: "5",
		}},
	}
	mock.ExpectQuery("SELECT marks FROM users WHERE chatid = $1").
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"marks"}).AddRow(expected))

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	marks, err := repo.GetUserMarks(context.Background(), 1)
	assert.NoError(t, err)
	assert.Equal(t, expected, marks)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUserSettings(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	expected := 2
	mock.ExpectQuery("SELECT settings FROM users WHERE chatid = $1").
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"settings"}).AddRow(expected))

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	settings, err := repo.GetUserSettings(context.Background(), 1)
	assert.NoError(t, err)
	assert.Equal(t, expected, settings)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetAdmins(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	expected := []int64{2, 3, 4}
	mock.ExpectQuery("SELECT chatid FROM admins").
		WillReturnRows(sqlmock.NewRows([]string{"chatid"}).AddRow(2).AddRow(3).AddRow(4))

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	admins, err := repo.GetAdmins(context.Background())
	assert.NoError(t, err)
	assert.Equal(t, expected, admins)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_GetUsersThatHaveNoPersonData(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	newUser := RepositoryUser{
		Login:               "Login",
		Password:            "Password",
		SessionID:           "SessionID",
		ChatID:              4,
		Settings:            5,
		SessionIDUpdateTime: time.Now(),
		MarksUpdateTime:     time.Now(),
		Marks: api.SummaryMarks{
			Subperiod: api.MarksSubperiod{
				Code: "Test",
				Name: "Test",
			},
			Dates: []string{"Test", "Test"},
			DisciplineMarks: []api.DisciplineMark{{
				Discipline: "Test",
				Marks: []api.Mark{{
					Date:        "Test",
					Description: "Test",
					Mark:        "5",
				}},
				AverageMark: "5",
			}},
		},
		School:   "МБОУ КЕК ЛОЛ",
		Class:    "11-й Мемный",
		FullName: "Пупкин Вася Иванович",
	}

	columns := []string{"chatid", "login", "password", "sessionid", "settings", "sessionid_last_update", "marks", "marks_update_time", "school", "class", "fullname"}
	expectedRows := sqlmock.NewRows(columns).
		AddRow(
			newUser.ChatID,
			newUser.Login,
			newUser.Password,
			newUser.SessionID,
			newUser.Settings,
			newUser.SessionIDUpdateTime,
			newUser.Marks,
			newUser.MarksUpdateTime,
			newUser.School,
			newUser.Class,
			newUser.FullName,
		).
		AddRow(
			newUser.ChatID,
			newUser.Login,
			newUser.Password,
			newUser.SessionID,
			newUser.Settings,
			newUser.SessionIDUpdateTime,
			newUser.Marks,
			newUser.MarksUpdateTime,
			newUser.School,
			newUser.Class,
			newUser.FullName,
		)
	mock.ExpectQuery("SELECT * FROM users WHERE (school = '') IS NOT FALSE OR (class = '') IS NOT FALSE OR (fullname = '') IS NOT FALSE").
		WillReturnRows(expectedRows)

	myDb := &Database{db: db}
	repo := myDb.NewUserRepository()
	kek, err := repo.GetUsersThatHaveNoPersonData(context.Background())
	assert.NoError(t, err)
	//assert.Equal(t, newUser, actualUser)
	//
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
	assert.Equal(t, 2, len(kek))
}

func TestUserRepository_UpdateUserPersonData(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	var updatedID int64 = 42
	updatedSchool := "Lol kek"
	updatedFullName := "test"
	updatedClass := "Test kek"

	mock.ExpectExec("UPDATE users SET school = $1, class = $2, fullname = $3 WHERE chatid = $4").
		WithArgs(updatedSchool, updatedClass, updatedFullName, updatedID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	database := &Database{db: db}
	userRepo := database.NewUserRepository()
	err = userRepo.UpdateUserPersonData(context.Background(), RepositoryUser{ChatID: updatedID, School: updatedSchool, Class: updatedClass, FullName: updatedFullName})
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestUserRepository_DeleteUser_NoError(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewUserRepository()
	userID := int64(1)

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks_documents WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks WHERE author = $1").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM users WHERE chatid = $1").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = repo.DeleteUser(context.Background(), userID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestUserRepository_DeleteUser_ErrorOnUsers(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewUserRepository()
	userID := int64(1)
	wantedErr := fmt.Errorf("not today =)")

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks_documents WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks WHERE author = $1").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM users WHERE chatid = $1").
		WithArgs(userID).WillReturnError(wantedErr)
	mock.ExpectRollback()

	err = repo.DeleteUser(context.Background(), userID)
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestUserRepository_DeleteUser_ErrorOnHomeworks(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewUserRepository()
	userID := int64(1)
	wantedErr := fmt.Errorf("not today =)")

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks_documents WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks WHERE author = $1").
		WithArgs(userID).WillReturnError(wantedErr)
	mock.ExpectRollback()

	err = repo.DeleteUser(context.Background(), userID)
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestUserRepository_DeleteUser_ErrorOnDocuments(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewUserRepository()
	userID := int64(1)
	wantedErr := fmt.Errorf("not today =)")

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("DELETE FROM homeworks_documents WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnError(wantedErr)
	mock.ExpectRollback()

	err = repo.DeleteUser(context.Background(), userID)
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestUserRepository_DeleteUser_ErrorOnImages(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewUserRepository()
	userID := int64(1)
	wantedErr := fmt.Errorf("not today =)")

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM homeworks_images WHERE hwid in (select id from homeworks where author = $1)").
		WithArgs(userID).WillReturnError(wantedErr)
	mock.ExpectRollback()

	err = repo.DeleteUser(context.Background(), userID)
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}
