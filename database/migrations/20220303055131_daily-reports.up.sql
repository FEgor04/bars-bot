CREATE TABLE IF NOT EXISTS day_marks(
  day_number integer,
  student_id bigint references users(chatid),
  marks json,
  UNIQUE (student_id, day_number)
  );

INSERT INTO day_marks (day_number, student_id, marks)
SELECT (week_number * 7 + 6 - date_part('dow', now())) as day_number, student as student_id, marks
FROM week_marks;
-- DROP TABLE IF EXISTS week_marks;
  
