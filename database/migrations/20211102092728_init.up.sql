create table if not exists users(
  chatid bigint not null primary key,
  login varchar,
  password varchar,
  sessionid varchar,
  settings int
);
