CREATE TABLE IF NOT EXISTS week_marks(
  week_number integer,
  marks json,
  student bigint references users(chatid),
  UNIQUE (week_number, student)
  );
INSERT INTO week_marks(week_number, marks, student)
SELECT div(day_number, 7) as week_number, marks, student_id as student
FROM day_marks;
-- DROP TABLE IF EXISTS day_marks;

