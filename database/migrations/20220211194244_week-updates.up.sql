create table if not exists week_marks(
  student bigint references users(chatid),
  mark json not null,
  discipline varchar(30) not null,
  date_set date not null
)
