alter table users add column school varchar;
alter table users add column class varchar;

create table if not exists homeworks(
  id serial primary key,
  author bigint references users(chatid),
  text text,
  date timestamp,
  subject varchar
);

create table if not exists homeworks_documents(
  hwid integer references homeworks(id),
  fileid varchar
);

create table if not exists homeworks_images(
  hwid integer references homeworks(id),
  fileid varchar
);
