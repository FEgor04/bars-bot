create table if not exists reports(
    id serial primary key,
    author bigint references users(chatid),
    text text,
    creation_date timestamp not null default now(),
    status int not null default 0
)