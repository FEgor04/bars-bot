drop table if exists week_marks;
create table if not exists week_marks
(
  week_number integer,
  marks json,
  student bigint references users (chatid),
  UNIQUE (week_number, student)
);
