package database

import (
	"context"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// ReportsRepository is a db wrapper for Report methods
type ReportsRepository struct {
	db *sqlx.DB
}

// Report is a class to store report data
type Report struct {
	ID           int
	Author       int64
	Text         string
	CreationDate time.Time
	Status       int
}

// Value returns report data compatible with database
func (s Report) Value() (driver.Value, error) {
	kek, err := json.Marshal(s)
	if err != nil {
		return "", err
	}
	return string(kek), nil
}

// Scan creates report from database data
func (s *Report) Scan(kek interface{}) error {
	switch kek := kek.(type) {
	case string:
		return json.Unmarshal([]byte(kek), s)
	case []uint8:
		return json.Unmarshal(kek, s)
	case nil:
		return nil
	}
	return errors.New("wtf lol")
}

// NewReportsRepository creates new report repository from Database
func NewReportsRepository(database *Database) *ReportsRepository {
	return &ReportsRepository{db: database.db}
}

// InsertReport вставляет репорт в базу данных
// Возвращает ID созданного репорта и ошибку при создании
// Если ошибки не возникло, возвращает nil
func (r ReportsRepository) InsertReport(ctx context.Context, rep Report) (int, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "InsertReport")
	defer span.End()

	span.SetAttributes(attribute.Int64("report.author", rep.Author))
	span.SetAttributes(attribute.String("report.text", rep.Text))

	var val int
	err := r.db.QueryRowContext(ctx, "INSERT INTO reports(author, text, status) VALUES ($1, $2, 0) RETURNING id", rep.Author, rep.Text).Scan(&val)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	return val, err
}

// GetReports получает репорты с заданным статусом
// Возвращает непосредственно массив репортов и ошибку
// Если ошибки не было, возвращает nil
func (r ReportsRepository) GetReports(ctx context.Context, status int) ([]Report, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "GetReports")
	span.SetAttributes(attribute.Int("reports.status", status))
	defer span.End()

	var answ []Report
	rows, err := r.db.QueryContext(ctx, "SELECT * FROM reports WHERE status = $1 order by id", status)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		return nil, err
	}
	for rows.Next() {
		var valNow Report
		err = rows.Scan(&valNow.ID, &valNow.Author, &valNow.Text, &valNow.CreationDate, &valNow.Status)
		if err != nil {
			span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
		} else {
			answ = append(answ, valNow)
		}
	}
	span.SetAttributes(attribute.Int("reports.n", len(answ)))
	return answ, err
}

// GetReportAuthor gets report author ChatID
func (r ReportsRepository) GetReportAuthor(ctx context.Context, reportID int) (int64, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "GetReportAuthor")
	defer span.End()

	span.SetAttributes(attribute.Int("report.id", reportID))

	var answ int64
	err := r.db.QueryRowContext(ctx, "SELECT author FROM reports WHERE id = $1", reportID).Scan(&answ)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	return answ, err
}

// UpdateReportStatus updates report status
func (r ReportsRepository) UpdateReportStatus(ctx context.Context, reportID int, status int) error {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "UpdateReportStatus")
	defer span.End()

	span.SetAttributes(attribute.Int("report.id", reportID))
	span.SetAttributes(attribute.Int("report.status", status))

	_, err := r.db.ExecContext(ctx, "UPDATE reports SET status = $1 WHERE id = $2", status, reportID)
	if err != nil {
		span.RecordError(err, trace.WithAttributes(attribute.String("error.text", err.Error())))
	}
	return err
}
