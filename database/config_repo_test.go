//go:build unit || all
// +build unit all

package database

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sqlmock "github.com/zhashkevych/go-sqlxmock"
	"testing"
	"time"
)

func TestConfigRepository_GetInterval_NoError(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewConfigRepository()
	interval := 10 * time.Minute

	mock.ExpectQuery("SELECT interval FROM config LIMIT 1").
		WillReturnRows(sqlmock.NewRows([]string{"interval"}).AddRow(interval.Microseconds()))

	actual, err := repo.GetInterval(ctx)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, interval, actual)
}

func TestConfigRepository_GetInterval_Error(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewConfigRepository()
	wantedErr := errors.New("not today")

	mock.ExpectQuery("SELECT interval FROM config LIMIT 1").
		WillReturnError(wantedErr)

	_, err = repo.GetInterval(ctx)
	assert.ErrorIs(t, wantedErr, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	//assert.Equal(t, interval, actual)
}

func TestConfigRepository_SetInterval(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewConfigRepository()
	interval := 10 * time.Minute

	mock.ExpectExec("UPDATE config SET interval = make_interval(secs => $1)").
		WithArgs(interval.Seconds()).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.SetInterval(ctx, interval)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestConfigRepository_SetInterval_Error(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	database := &Database{db: db}
	repo := database.NewConfigRepository()
	interval := 10 * time.Minute
	wantedError := errors.New("not today")

	mock.ExpectExec("UPDATE config SET interval = make_interval(secs => $1)").
		WithArgs(interval.Seconds()).
		WillReturnError(wantedError)

	err = repo.SetInterval(ctx, interval)
	assert.ErrorIs(t, wantedError, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}
