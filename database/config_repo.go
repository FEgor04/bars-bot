package database

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const defaultInterval = 1 * time.Minute

type ConfigRepository struct {
	db *sqlx.DB
}

func (d Database) NewConfigRepository() ConfigRepository {
	return ConfigRepository(d)
}

func (c ConfigRepository) SetInterval(ctx context.Context, interval time.Duration) error {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "SetInterval")
	defer span.End()
	span.SetAttributes(attribute.String("interval", interval.String()))
	_, err := c.db.ExecContext(ctx, "UPDATE config SET interval = make_interval(secs => $1)", interval.Seconds())
	if err != nil {
		span.RecordError(err)
	}
	return err
}

func (c ConfigRepository) GetInterval(ctx context.Context) (time.Duration, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "GetInterval")
	defer span.End()

	var resInt int
	err := c.db.QueryRowContext(ctx, "SELECT interval FROM config LIMIT 1").Scan(&resInt)
	if err != nil {
		span.RecordError(err)
		return defaultInterval, err
	}
	var resDur time.Duration
	resDur = time.Duration(int64(resInt) * time.Microsecond.Nanoseconds())
	if resInt == 0 {
		resDur = defaultInterval
	}
	return resDur, nil
}

func (c ConfigRepository) CheckIfScheduleDisabled(ctx context.Context) (bool, error) {
	var span trace.Span
	ctx, span = otel.Tracer(TracerName).Start(ctx, "CheckSchedule")
	defer span.End()

	var res bool
	err := c.db.QueryRowxContext(ctx, "SELECT disable_schedule FROM config LIMIT 1").Scan(&res)
	if err != nil {
		span.RecordError(err)
		return true, err
	}
	return res, nil
}
