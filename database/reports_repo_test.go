//go:build unit || all
// +build unit all

package database

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	sqlmock "github.com/zhashkevych/go-sqlxmock"
	"testing"
	"time"
)

func TestReportsRepository_InsertReport(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	report := Report{
		ID:           1,
		Author:       12345,
		Text:         "Test test",
		CreationDate: time.Now(),
		Status:       1,
	}
	mock.ExpectQuery("INSERT INTO reports(author, text, status) VALUES ($1, $2, 0) RETURNING id").
		WithArgs(report.Author, report.Text).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(report.ID))

	myDb := &Database{db: db}
	repo := NewReportsRepository(myDb)
	id, err := repo.InsertReport(context.Background(), report)
	assert.NoError(t, err)
	assert.Equal(t, report.ID, id)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestReportsRepository_GetReports(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	wantedStatus := 0

	mock.ExpectQuery("SELECT * FROM reports WHERE status = $1 order by id").
		WithArgs(wantedStatus).
		WillReturnRows(sqlmock.NewRows([]string{"id", "author", "text", "creation_date", "status"}).AddRow(
			1,
			123,
			"kek",
			time.Now(),
			0,
		).AddRow(
			2,
			234,
			"lol",
			time.Now(),
			0,
		),
		)

	myDb := &Database{db: db}
	repo := NewReportsRepository(myDb)
	reports, err := repo.GetReports(context.Background(), wantedStatus)
	assert.NoError(t, err)
	assert.NotEqual(t, nil, reports)
	assert.Equal(t, 2, len(reports))
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestReportsRepository_GetReportAuthor(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	wantedID := 1
	var wantedAuthor int64 = 42

	mock.ExpectQuery("SELECT author FROM reports WHERE id = $1").
		WithArgs(wantedID).WillReturnRows(sqlmock.NewRows([]string{"author"}).AddRow(wantedAuthor))

	myDb := &Database{db: db}
	repo := NewReportsRepository(myDb)
	id, err := repo.GetReportAuthor(context.Background(), wantedID)
	assert.NoError(t, err)
	assert.Equal(t, wantedAuthor, id)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestReportsRepository_UpdateReportStatus(t *testing.T) {
	t.Parallel()
	db, mock, err := sqlmock.Newx(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	require.NoError(t, err)
	defer db.Close()

	wantedID := 1
	newStatus := 42

	mock.ExpectExec("UPDATE reports SET status = $1 WHERE id = $2").
		WithArgs(newStatus, wantedID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	myDb := &Database{db: db}
	repo := NewReportsRepository(myDb)
	err = repo.UpdateReportStatus(context.Background(), wantedID, newStatus)
	assert.NoError(t, err)
	err = mock.ExpectationsWereMet()
	assert.NoError(t, err)
}

func TestReportsRepository_ValueAndScan(t *testing.T) {
	report := Report{
		ID:           4,
		Author:       int64(2121121),
		Text:         "Test Report",
		CreationDate: time.Now(),
		Status:       1,
	}

	reportValue, err := report.Value()
	require.NoError(t, err)

	reportScanned := Report{}
	err = reportScanned.Scan(reportValue)
	require.NoError(t, err)
	assert.EqualValues(t, report.ID, reportScanned.ID)
	assert.EqualValues(t, report.Text, reportScanned.Text)
	assert.EqualValues(t, report.Author, reportScanned.Author)
	assert.True(t, report.CreationDate.Equal(reportScanned.CreationDate), "Creation dates are not equal")
	assert.EqualValues(t, report.Status, reportScanned.Status)
}
